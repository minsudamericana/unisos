	1.0.1
	^ Updated jquery to v 2.2.4
	^ Updated UIKit to v 2.26.3
	^ Updated FullCalendar to v 2.7.2
	! Changed files/folders
		js/jquery/jquery.min.js
		vendor/


	1.0.0
	+ Initial Release



	* -> Security Fix
	# -> Bug Fix
	$ -> Language fix or change
	+ -> Addition
	^ -> Change
	- -> Removed
	! -> Note
