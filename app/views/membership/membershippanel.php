<?php

$backURL = 'http://www.unisos.com/membership/responseMP';
$sandboxTesting = true;
if ($sandboxTesting == true) {
  $initPoint = 'sandbox_init_point';
  $cli_id = '7541711196164994';
  $cli_sec = 'LgwtWNlLK1f21IwQo7DT2orxCRxwf27D';
}else{
  $initPoint = 'init_point';
  $cli_id = getMPConf()['client_id'];
  $cli_sec = getMPConf()['client_secret'];
}

$usermail = getUserNfo($user_id)['email'];
$usermail = 'maildetesteo@mail.com.ar';

$mp = new MP($cli_id, $cli_sec);
$onemonth = array(
  'payer_email' => $usermail,
  'back_url' => $backURL,
  'reason' => 'Unisos Premium | Subscripcion con pago una vez por mes',
  'external_reference' => 'OP-1234',
  'auto_recurring' => array(
    'frequency' => 1,
    'frequency_type' => 'months',
    'transaction_amount' => 40,
    'currency_id' => 'ARS'
  )
);
$onemonthAprov = $mp->create_preapproval_payment($onemonth);

$threemonth = array(
  'payer_email' => $usermail,
  'back_url' => $backURL,
  'reason' => 'Unisos Premium | Subscripcion con pago cada 3 meses',
  'external_reference' => 'OP-1234',
  'auto_recurring' => array(
    'frequency' => 3,
    'frequency_type' => 'months',
    'transaction_amount' => 35,
    'currency_id' => 'ARS'
  )
);
$threemonthAprov = $mp->create_preapproval_payment($threemonth);

$sixmonth = array(
  'payer_email' => $usermail,
  'back_url' => $backURL,
  'reason' => 'Unisos Premium | Subscripcion con pago cada 6 meses',
  'external_reference' => 'OP-1234',
  'auto_recurring' => array(
    'frequency' => 6,
    'frequency_type' => 'months',
    'transaction_amount' => 30,
    'currency_id' => 'ARS'
  )
);
$sixmonthAprov = $mp->create_preapproval_payment($sixmonth);

$twelvemonth = array(
  'payer_email' => $usermail,
  'back_url' => $backURL,
  'reason' => 'Unisos Premium | Subscripcion con pago cada 12 meses',
  'external_reference' => 'OP-1234',
  'auto_recurring' => array(
    'frequency' => 12,
    'frequency_type' => 'months',
    'transaction_amount' => 25,
    'currency_id' => 'ARS'
  )
);
$twelvemonthAprov = $mp->create_preapproval_payment($twelvemonth);

$preapprovalID = '0d3afd538e9f46e5842e530e6c8fc244';
checkMPRecurrent($cli_id, $cli_sec, $preapprovalID);
?>

<!DOCTYPE html>
<html lang="<?php echo $language;?>">
<head>
  <?php $this->load->view('assets/headnfo');?>
  <?php $this->load->view('assets/style');?>
</head>
<body id="tm-container">
  <div class="tm-container">
    <?php $this->load->view('elements/component');?>
    <?php $this->load->view('elements/navbar');?>
    <?php $this->load->view('elements/spotlight-internal');?>
    <?php $this->load->view('elements/breadcrum-gen');?>
    <div id="tm-main" class="tm-block-main uk-block uk-block-default">
      <div class="uk-container uk-container-center">
        <div class="tm-middle uk-grid" data-uk-grid-match data-uk-grid-margin>
          <div class="tm-main uk-width-medium-7-10">
            <main id="tm-content" class="tm-content">
              <article class="uk-article tm-article">
                <div class="tm-article-wrapper">
                  <div class="tm-article-content uk-margin-large-bottom uk-margin-top-remove">
                    <div class="tm-article">
                      <h1 class="uk-article-title">Estado de Membresia</h1>
                      
                      <h4>Estado de Membresia:</h4>
                      <?php foreach ($memStatus->result() as $mst): ?>
                        <?php if($mst->status == 1): ?>
                          <p>Activa</p>
                          <small><a href="#">Quiero dar de baja mi membresia.</a></small>
                        <?php elseif($mst->status == 0): ?>
                          <p>Inactiva</p>
                          <p>Adquirir Membresia</p>
                          <a class="uk-button-success uk-button" href="<?php echo $onemonthAprov['response'][$initPoint]; ?>" name="MP-Checkout">1 Mes</a>
                          <a class="uk-button-success uk-button" href="<?php echo $threemonthAprov['response'][$initPoint]; ?>" name="MP-Checkout">3 Meses</a>
                          <a class="uk-button-success uk-button" href="<?php echo $sixmonthAprov['response'][$initPoint]; ?>" name="MP-Checkout">6 Meses</a>
                          <a class="uk-button-success uk-button" href="<?php echo $twelvemonthAprov['response'][$initPoint]; ?>" name="MP-Checkout">12 Meses</a>
                        <?php endif ?>
                      <?php endforeach ?>

                    </div>
                  </div>
                </div>
              </article>
            </main>
          </div>
        </div>
      </div>
    </div>
    <?php $this->load->view('elements/footer');?>
  </div>
  <?php $this->load->view('assets/scripts');?>
  <script type="text/javascript" src="https://www.mercadopago.com/org-img/jsapi/mptools/buttons/render.js"></script>
  <style>
  #MP-Checkout-dialog {
    z-index: 8000!important;
  }
  </style>
</body>
</html>