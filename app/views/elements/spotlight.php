<!-- spotlight -->
<div class="tm-block-spotlight uk-block uk-block-primary tm-float-box tm-overlay-5" id="tm-spotlight">
  <div class="uk-container uk-container-center">
    <section class="tm-spotlight uk-grid" data-uk-grid-match="{target:'> div > .uk-panel'}">
      <div class="uk-width-1-1">
        <div class="uk-panel">
          <div class="uk-grid" data-uk-grid-margin="">
            <div class="uk-width-medium-8-10">
              <blockquote class="uk-clearfix">
                <p>Ofrecemos Servicios Integrales al Alcance de su Presupuesto. Conocenos.</p><span class="author"></span>
              </blockquote>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
</div>