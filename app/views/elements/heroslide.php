<div class="tm-block-hero uk-block uk-block-default tm-block-fullwidth tm-grid-collapse" id="tm-hero">
  <div class="uk-container uk-container-center">
    <section class="tm-hero uk-grid" data-uk-grid-match="{target:'> div > .uk-panel'}">
      <div class="uk-width-1-1">
        <div class="uk-panel">
          <div class="tm-slideshow-sandal uk-slidenav-position" data-uk-slideshow="{autoplay:true, animation: 'random-fx', pauseOnHover: true, duration: 300, autoplayInterval: 15000, kenburns: false, slices: 25}">
            <ul class="uk-slideshow uk-overlay-active">
              <li>
                <video width="320" height="240" loop>
                  <source src="<?php echo base_url();?>/assets/videos/unisos-hd.mp4" type="video/mp4">
                  Tu navegador no soporta videos
                </video>
              </li>
            </ul>
            <div class="uk-margin">
              <ul class="uk-dotnav uk-hidden-touch">
                <li data-uk-slideshow-item="0"><a href=""></a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
</div>