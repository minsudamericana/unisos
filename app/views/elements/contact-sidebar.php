<aside class="tm-sidebar-a uk-width-medium-1-2">
  <div class="uk-panel uk-panel-box">
    <div class="uk-grid uk-grid-width-medium-1-2 uk-grid-width-large-1-2" data-uk-grid-margin="">
      <div class="uk-panel uk-panel-space">
        <h3 class="uk-module-title">Consultas</h3>
        <ul class="uk-list list-icons">
          <li><i class="uk-icon-phone"></i> +54 11 1234 1234</li>
          <li><i class="uk-icon-envelope"></i> info@unisos.com.ar</li>
          <li><i class="uk-icon-map-marker"></i> Buenos Aires, Argentina</li>
        </ul>
      </div>
    </div>
  </div>
</aside>