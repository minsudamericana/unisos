<div class="tm-block-top-b uk-block uk-block-secondary tm-block-fullwidth tm-grid-collapse" id="tm-top-b">
  <div class="uk-container uk-container-center">
    <section class="tm-top-b uk-grid" data-uk-grid-margin="" data-uk-grid-match="{target:'> div > .uk-panel'}">
      <div class="uk-width-1-1">
        <div class="uk-panel">

          <div class="tm-slideshow-panel-sandal" id="wk-1">
            <div class="uk-panel uk-panel uk-padding-remove uk-overflow-hidden">
              <div class="uk-grid uk-grid-collapse uk-flex-middle" data-uk-grid-margin="">
                
                <div class="uk-width-medium-1-2 uk-text-center uk-row-first">
                  <div class="uk-slidenav-position" data-uk-slideshow="{animation: 'swipe'}">
                    <ul class="uk-slideshow">
                      <li><img alt="demo" src="<?php echo base_url();?>assets/img/demo/default/content/content-1.jpg" width="650" height="420"></li>
                      <li><img alt="demo" src="<?php echo base_url();?>assets/img/demo/default/content/content-2.jpg" width="650" height="420"></li>
                    </ul>

                    <a class="uk-slidenav uk-slidenav-previous uk-hidden-touch" data-uk-slideshow-item="previous" href="#"></a>
                    <a class="uk-slidenav uk-slidenav-next uk-hidden-touch" data-uk-slideshow-item="next" href="#"></a>

                    <div class="uk-position-bottom uk-margin-large-bottom uk-margin-large-left uk-margin-large-right">
                      <ul class="uk-dotnav uk-dotnav-contrast uk-flex-center uk-margin-bottom-remove">
                        <li data-uk-slideshow-item="0"><a href=""></a></li>
                        <li data-uk-slideshow-item="1"><a href=""></a></li>
                      </ul>
                    </div>
                  </div>
                </div>

                <div class="uk-width-medium-1-2">
                  <div class="uk-panel-body uk-margin-left uk-text-left" data-uk-slideshow="">
                    <ul class="uk-slideshow">


                      <li>
                        <div class="uk-width-xlarge-9-10">
                          <h3 class="uk-module-title uk-h2 uk-margin-top-remove"><a class="uk-link-reset" href="#">We provide timely support</a></h3>
                          <div class=" uk-margin-top">
                            Do not dwell in the past, do not dream of the future. Let reality be reality. Let things flow naturally. Do not dwell in the past, do not dream of the future.
                            <div class="uk-grid" data-uk-grid-margin="">
                              <div class="uk-width-medium-1-2">
                                <ul class="uk-list list-icons">
                                  <li><i class="uk-icon-check"></i>Soft loans</li>
                                  <li><i class="uk-icon-check"></i>As needed support</li>
                                  <li><i class="uk-icon-check"></i>Time unlimited consultancy</li>
                                </ul>
                              </div>
                              <div class="uk-width-medium-1-2">
                                <ul class="uk-list list-icons">
                                  <li><i class="uk-icon-check"></i>Therapy and consulting</li>
                                  <li><i class="uk-icon-check"></i>Auditing and reports</li>
                                  <li><i class="uk-icon-check"></i>Board members support</li>
                                </ul>
                              </div>
                            </div>
                          </div>
                          <p class="uk-margin-bottom-remove"><a class="uk-margin-top uk-button uk-button-primary" href="#">Read more <i class="uk-icon-chevron-right"></i></a></p>
                        </div>
                      </li>

                      <li>
                        <div class="uk-width-xlarge-9-10">
                          <h3 class="uk-module-title uk-h2 uk-margin-top-remove"><a class="uk-link-reset" href="#">We keep our word</a></h3>
                          <div class=" uk-margin-top">
                            Let reality be reality. Let things flow naturally. Do not dwell in the past, do not dream of the future. Do not dwell in the past, do not dream of the future.
                            <div class="uk-grid" data-uk-grid-margin="">
                              <div class="uk-width-medium-1-2">
                                <ul class="uk-list list-icons">
                                  <li><i class="uk-icon-check"></i>After sales services</li>
                                  <li><i class="uk-icon-check"></i>One on one customer care</li>
                                  <li><i class="uk-icon-check"></i>24/7 online help</li>
                                </ul>
                              </div>
                              <div class="uk-width-medium-1-2">
                                <ul class="uk-list list-icons">
                                  <li><i class="uk-icon-check"></i>On call consultants</li>
                                  <li><i class="uk-icon-check"></i>Affordable services</li>
                                  <li><i class="uk-icon-check"></i>Financial help</li>
                                </ul>
                              </div>
                            </div>
                          </div>
                          <p class="uk-margin-bottom-remove"><a class="uk-margin-top uk-button uk-button-primary" href="#">Read more <i class="uk-icon-chevron-right"></i></a></p>
                        </div>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
</div>