<!-- top-a -->
<div class="tm-block-top-a uk-block uk-block-default" id="tm-top-a">
  <div class="uk-container uk-container-center">
    <section class="tm-top-a uk-grid" data-uk-grid-margin="" data-uk-grid-match="{target:'> div > .uk-panel'}">
      <div class="uk-width-1-1">
        <div class="uk-panel">
          <div class="uk-grid uk-grid-width-small-1-2 uk-grid-width-medium-1-2 uk-grid-width-large-1-4" data-uk-grid-margin="">

            <div class="uk-block">
              <h3 class="uk-module-title"><span class="tm-primary-color">Plataforma</span> E-Learning</h3>
              <img alt="Asset finance" class="uk-border-rounded tm-border-bottom" height="183" src="<?php echo base_url();?>assets/img/images/stock/elearning.jpg" width="275">
              <p>Ingresa hoy mismo en la plataforma de estudio del Futuro, la educacion certificada  distancia esta dando pasos agigantados. Capacitate y hace la diferencia.</p>
              <a class="uk-button-link uk-button" href="<?php echo base_url() . 'elearning/';?>" target="_self">Saber Más<i class="uk-icon-angle-right"></i></a>
            </div>

            <div class="uk-block">
              <h3 class="uk-module-title"><span class="tm-primary-color">Directorio</span> Empresarial</h3>
              <img alt="Asset finance" class="uk-border-rounded tm-border-bottom" height="183" src="<?php echo base_url();?>assets/img/images/stock/directorio.jpg" width="275">
              <p>Conoce nuestros amplios directorios de Profesionales y Empresas. Empresas e Industrias de todos los Rubros. Profesionales calificados y seleccionados meticulosamente.</p>
              <a class="uk-button-link uk-button" href="<?php echo base_url() . 'industrial/';?>" target="_self">Empresas <i class="uk-icon-angle-right"></i></a>
              <a class="uk-button-link uk-button" href="<?php echo base_url() . 'rrhh/';?>" target="_self">Profesionales <i class="uk-icon-angle-right"></i></a>
            </div>

            <div class="uk-block">
              <h3 class="uk-module-title"><span class="tm-primary-color">Foro</span> Corporativo</h3>
              <img alt="Business consulting" class="uk-border-rounded tm-border-bottom" height="183" src="<?php echo base_url();?>assets/img/images/stock/foro.jpg" width="275">
              <p>Conoce y comparti inquietudes, ideas y crea vinculos profesionales con Empresarios, Emprendedores y Oficiales de todos los rubros.</p>
              <a class="uk-button-link uk-button" href="<?php echo base_url() . 'foro/';?>" target="_self">Saber Más <i class="uk-icon-angle-right"></i></a>
            </div>

            <div class="uk-block">
              <h3 class="uk-module-title"><span class="tm-primary-color">Consultoria</span> de Software</h3>
              <img alt="Morgage funding" class="uk-border-rounded tm-border-bottom" height="183" src="<?php echo base_url();?>assets/img/images/stock/erp.jpg" width="275">
              <p>Actualiza tus procesos y productos. Ofrecemos la tecnologia mas vanguardista del Mercado, a medida y con asistencia post-venta.</p>
              <a class="uk-button-link uk-button" href="<?php echo base_url() . 'page/software/';?>" target="_self">Informarme <i class="uk-icon-angle-right"></i></a>
            </div>

          </div>
        </div>
      </div>
    </section>
  </div>
</div>