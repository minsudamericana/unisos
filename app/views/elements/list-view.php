<div class="modal modal-big fade" id="listModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Lista</h4>
            </div>
            <div class="modal-body">  
                <div class="row">
                    <div class="col-md-12">
                      <div class="table-responsive">
                        <table class="table">
                          <thead>
                              <tr>
                                <th class="text-center">
                                </th>
                                <th>
                                  Producto
                                </th>
                                <th class="th-description">
                                  Descripcion
                                </th>
                                <th class="text-right">
                                  Precio
                                </th>
                                <th class="text-right">
                                  Cant.
                                </th>
                                <th class="text-right">
                                  Valor Final
                                </th>
                                <th>
                                </th>
                              </tr>
                             </thead>
                             <tbody class="cart-list">                         
                             </tbody>
                          </table>
                      </div>                       
                    </div>                    
               </div>
            </div>
        </div>
    </div>
</div>

<div class="modal modal-small fade" id="createListModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Crear nueva lista</h4>
            </div>
            <div class="modal-body">
                <form id="newListForm" action="<?php echo base_url();?>Cart/newList" method="post" accept-charset="utf-8">
                    <div class="form-group">
                        <input type="text" name="listName" id="listName" value="" placeholder="" class="form-control">
                    </div>                   
                    <button type="submit" class="btn btn-info btn-fill btn-block">Crear</button>
                </form>
                <div id="loadingDiv" style="width:100%;float:left;display:none;">
                    <img src="<?php echo base_url();?>assets/img/AjaxLoader.gif"> Creando....
                </div>
                <span class="alert alert-success alert-mail" role="alert" id="contactOk" style="display:none;">Lista creada</span>
                <span class="alert alert-danger alert-mail" role="alert" id="contactError" style="display:none;">Se produjo un error</span>
            </div>
        </div>
    </div>
</div>

<div class="modal modal-small fade" id="listFeedback" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Deja tu comentario</h4>
            </div>
            <div class="modal-body">
                <form id="feedbackForm" action="<?php echo base_url();?>Cart/listFeedback" method="post" accept-charset="utf-8">
                    <div class="form-group">
                        <textarea name="feedback" id="feedback" class="form-control"> </textarea>
                        <input type="hidden" id="listIdHidden" name="listIdHidden" />
                    </div>                   
                    <button type="submit" class="btn btn-info btn-fill btn-block">Enviar</button>
                </form>        
                
            </div>
        </div>
    </div>
</div>