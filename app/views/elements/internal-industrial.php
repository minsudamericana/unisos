<aside class="tm-sidebar-b uk-width-medium-3-10">
  
  <div class="uk-panel uk-panel-box tm-panel-box-primary-light uk-border-rounded industrial-list">
    <h3 class="uk-panel-title list-title">Directorio de Empresas</h3>
      
      <?php foreach ($industrialCatList->result() as $indcat): ?>
        <?php if (checkIFSubcatInCat($indcat->indcat_id) == true): ?>
        <div class="tm-toggle uk-accordion" data-uk-accordion="{showfirst:false,duration:300}">
          <h3 class="uk-accordion-title"><i class="tm-toggle-button uk-float-right"></i> <?php echo $indcat->name; ?></h3>
          <div class="uk-accordion-content">
            <?php getIndSubCatByCatID($indcat->indcat_id);?>
          </div>
        </div>
        <?php endif ?>
      <?php endforeach ?>

  </div>
  
  <div class="uk-panel">
    <a class="uk-button-default uk-width-1-1 uk-margin-small-bottom uk-button" href="#" target="_self">
      <i class="uk-icon-file-pdf-o"></i> Descargar Instructivo
    </a>
  </div>
  
  <div class="uk-panel uk-panel-box uk-border-rounded tm-background-icon" style="border: 1px solid #258bce;overflow: hidden;">
    <h3 class="uk-panel-title"><i class="uk-icon-comments uk-margin-small-right"></i> Nuestras Redes Sociales</h3>
    <div class="fb-like" data-href="https://www.facebook.com/unisosoficial/" data-layout="standard" data-action="like" data-size="small" data-show-faces="true" data-share="true"></div>
    <a class="twitter-follow-button"
      href="https://twitter.com/unisos_sa">
    Seguir a @Unisos_sa
    </a>
    <script src="//platform.linkedin.com/in.js" type="text/javascript"> lang: es_ES</script>
    <script type="IN/FollowCompany" data-id="15243923" data-counter="right"></script>
    <br>
    <a class="github-button" href="https://github.com/unisos" data-style="mega" data-count-href="/unisos/followers" data-count-api="/users/unisos#followers" data-count-aria-label="# seguidores e  n GitHub" aria-label="Seguir @unisos en GitHub">Seguir @unisos</a>
  </div>

</aside>

<style>
  .industrial-list {
    padding: 10px 10px;
  }
  .industrial-list .list-title {
    margin-bottom: 5px;
    padding: 0px 10px 0 10px;
    font-weight: bold;
  }
  .industrial-list .uk-accordion {
    margin: 0;
    padding: 0;
  }
  .industrial-list .uk-accordion .uk-accordion-title {
    margin: 0;
    border: none;
    padding: 5px 10px;
  }
  .industrial-list .uk-accordion .uk-accordion-content {
    padding: 5px 15px;
    border: none;
  }
  .industrial-list .uk-accordion .uk-accordion-content a {
    float: left;
    width: 100%;
  }
</style>