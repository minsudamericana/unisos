<div class="tm-breadcrumbs">
  <div class="uk-container uk-container-center">
    <div class="uk-hidden-small">
      <ul class="uk-breadcrumb">
    	<li><a href="<?php echo base_url();?>">Home</a></li>
    	<?php if ($this->uri->segment(1) == 'Software'): ?>
    		<li><span>Software</span></li>
		<?php elseif ($this->uri->segment(1) == 'elearning'): ?>
			<?php if ($this->uri->segment(2) == 'list'): ?>
    			<li><span><a href="<?php echo base_url();?>elearning">E-Learning</a></span></li>
    			<li><span>Lista</span></li>
    		<?php else: ?>
				<li><span>E-Learning</span></li>
    		<?php endif ?>
		<?php elseif ($this->uri->segment(1) == 'User'): ?>
			<?php if ($this->uri->segment(2) == 'mustLog' OR $this->uri->segment(2) == 'logUser'): ?>
				<li><span>Iniciar Sesion</span></li>

			<?php else: ?>
			<?php endif ?>
		<?php elseif ($this->uri->segment(1) == 'rrhh'): ?>
			<?php if ($this->uri->segment(2) == 'candidate'): ?>
				<li><a href="<?php echo base_url();?>rrhh"><span>Mercado RRHH</span></a></li>
				<li><span>Mi Perfil</span></li>
			<?php elseif ($this->uri->segment(2) == 'directory'): ?>
				<li><a href="<?php echo base_url();?>rrhh"><span>Mercado RRHH</span></a></li>
				
			<?php elseif ($this->uri->segment(2) == 'mycandidates'): ?>
				<li><a href="<?php echo base_url();?>rrhh"><span>Mercado RRHH</span></a></li>
				<li><span>Mis Candidatos</span></li>
			<?php else: ?>
				<li><span>Mercado RRHH</span></li>
			<?php endif ?>
		<?php endif ?>
      </ul>
    </div>
  </div>
</div>