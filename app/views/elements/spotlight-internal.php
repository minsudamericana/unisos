<div id="tm-spotlight" class="tm-block-spotlight uk-block uk-block-default tm-block-fullwidth tm-grid-collapse" >
  <div class="uk-container uk-container-center">
    <section class="tm-spotlight uk-grid" data-uk-grid-match="{target:'> div > .uk-panel'}">
      <div class="uk-width-1-1">
        <div class="uk-panel uk-contrast tm-overlay-primary">
          <div class="tm-background-cover uk-cover-background uk-flex uk-flex-center uk-flex-middle" style="background-position: 50% 0px; background-image:url(images/background/bg-image-1.jpg)" data-uk-parallax="{bg: '-200'}">
            <div class="uk-position-relative uk-container tm-inner-container">
              <h2 class="uk-module-title-alt uk-margin-remove"><?php echo $titleSpot; ?></h2>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
</div>