<!-- top-e -->
<div class="tm-block-top-e uk-block uk-block-muted tm-overlay-5" id="tm-top-e">
  <div class="uk-container uk-container-center">
    <section class="tm-top-e uk-grid" data-uk-grid-margin="" data-uk-grid-match="{target:'> div > .uk-panel'}">
      <div class="uk-width-1-1">
        <div class="uk-panel">
          <div class="uk-text-center">
            <h2 class="uk-module-title-alt">Meet our team</h2>
          </div>
          <br>
          <div class="uk-grid uk-grid-width-small-1-2 uk-grid-width-medium-1-2 uk-grid-width-large-1-4" data-uk-grid-margin="">
            
          <!-- block -->
            <div class="uk-block">
              <div data-uk-scrollspy="{cls:'uk-animation-fade', delay:100}">
                <div class="uk-panel uk-panel-box tm-panel-card">
                  <div class="uk-panel-teaser">
                    <img alt="Diane Moreno - Finance specialist" class="tm-card-avatar" src="<?php echo base_url();?>assets/img/demo/avatar/team-1.jpg">
                    <div class="tm-social-icons tm-icons-visible">
                      <a class="uk-icon-button uk-icon-facebook" href="#" target="_blank"></a> <a class="uk-icon-button uk-icon-twitter" href="#" target="_blank"></a> <a class="uk-icon-button uk-icon-linkedin" href="#" target="_blank"></a>
                    </div>
                  </div><a class="tm-card-link" data-uk-modal="{center:true}" href="#team1"></a>
                  <div class="tm-card-content">
                    <h3 class="uk-panel-title">Diane Moreno</h3>
                    <h4 class="tm-card-title">Finance specialist</h4>
                  </div>
                </div>
              </div>
            </div>
            
            <!-- block -->
            <div class="uk-block">
              <div data-uk-scrollspy="{cls:'uk-animation-slide-right', delay:200}">
                <h3 class="uk-module-title"><span class="tm-primary-color">Hi.</span> I'm Diane.</h3>Do not dwell in the past, do not dream of the future, do it now
                <ul class="uk-list list-icons">
                  <li><i class="uk-icon-check"></i>After sales services</li>
                  <li><i class="uk-icon-check"></i>Direct consultancy</li>
                  <li><i class="uk-icon-check"></i>24/7 online help</li>
                </ul><a class="uk-button-primary uk-button-small uk-button" href="#" target="_self">Get in touch</a>
              </div>
            </div>
            
            <!-- block -->
            <div class="uk-block">
              <div data-uk-scrollspy="{cls:'uk-animation-fade', delay:300}">
                <div class="uk-panel uk-panel-box tm-panel-card">
                  <div class="uk-panel-teaser">
                    <img alt="Kerry Clayton - Asset Advisor" class="tm-card-avatar" src="<?php echo base_url();?>assets/img/demo/avatar/team-4.jpg">
                    <div class="tm-social-icons tm-icons-visible">
                      <a class="uk-icon-button uk-icon-facebook" href="#" target="_blank"></a> <a class="uk-icon-button uk-icon-twitter" href="#" target="_blank"></a> <a class="uk-icon-button uk-icon-linkedin" href="#" target="_blank"></a>
                    </div>
                  </div><a class="tm-card-link" data-uk-modal="{center:true}" href="#team1"></a>
                  <div class="tm-card-content">
                    <h3 class="uk-panel-title">Kerry Clayton</h3>
                    <h4 class="tm-card-title">Asset Advisor</h4>
                  </div>
                </div>
              </div>
            </div>
            
            <!-- block -->
            <div class="uk-block">
              <div data-uk-scrollspy="{cls:'uk-animation-slide-right', delay:400}">
                <h3 class="uk-module-title"><span class="tm-primary-color">Hi.</span> I'm Kerry.</h3>Do not dwell in the past, do not dream of the future, do it now
                <ul class="uk-list list-icons">
                  <li><i class="uk-icon-check"></i>After sales services</li>
                  <li><i class="uk-icon-check"></i>Direct consultancy</li>
                  <li><i class="uk-icon-check"></i>24/7 online help</li>
                </ul><a class="uk-button-primary uk-button-small uk-button" href="#" target="_self">Get in touch</a>
              </div>
            </div>
            
            <!-- block -->
            <div class="uk-block uk-text-right">
              <div data-uk-scrollspy="{cls:'uk-animation-slide-left', delay:600}">
                <h3 class="uk-module-title uk-text-right"><span class="tm-primary-color">Hi.</span> I'm Miguel.</h3>Do not dwell in the past, do not dream of the future, do it now
                <ul class="uk-list list-icons uk-text-right">
                  <li><i class="uk-icon-check"></i>After sales services</li>
                  <li><i class="uk-icon-check"></i>Direct consultancy</li>
                  <li><i class="uk-icon-check"></i>24/7 online help</li>
                </ul><a class="uk-button-primary uk-button-small uk-button" href="#" target="_self">Get in touch</a>
              </div>
            </div>
            
            <!-- block -->
            <div class="uk-block">
              <div data-uk-scrollspy="{cls:'uk-animation-fade', delay:500}">
                <div class="uk-panel uk-panel-box tm-panel-card">
                  <div class="uk-panel-teaser">
                    <img alt="Miguel Welch - Finance specialist" class="tm-card-avatar" src="<?php echo base_url();?>assets/img/demo/avatar/team-5.jpg">
                    <div class="tm-social-icons tm-icons-visible">
                      <a class="uk-icon-button uk-icon-facebook" href="#" target="_blank"></a> <a class="uk-icon-button uk-icon-twitter" href="#" target="_blank"></a> <a class="uk-icon-button uk-icon-linkedin" href="#" target="_blank"></a>
                    </div>
                  </div><a class="tm-card-link" data-uk-modal="{center:true}" href="#team1"></a>
                  <div class="tm-card-content">
                    <h3 class="uk-panel-title">Miguel Welch</h3>
                    <h4 class="tm-card-title">Finance specialist</h4>
                  </div>
                </div>
              </div>
            </div>
            
            <!-- block -->
            <div class="uk-block uk-text-right">
              <div data-uk-scrollspy="{cls:'uk-animation-slide-left', delay:800}">
                <h3 class="uk-module-title uk-text-right"><span class="tm-primary-color">Hi.</span> I'm Yvonne.</h3>Do not dwell in the past, do not dream of the future, do it now
                <ul class="uk-list list-icons uk-text-right">
                  <li><i class="uk-icon-check"></i>After sales services</li>
                  <li><i class="uk-icon-check"></i>Direct consultancy</li>
                  <li><i class="uk-icon-check"></i>24/7 online help</li>
                </ul><a class="uk-button-primary uk-button-small uk-button" href="#" target="_self">Get in touch</a>
              </div>
            </div>
            
            <!-- block -->
            <div class="uk-block">
              <div data-uk-scrollspy="{cls:'uk-animation-fade', delay:700}">
                <div class="uk-panel uk-panel-box tm-panel-card">
                  <div class="uk-panel-teaser">
                    <img alt="Yvonne Mason - Asset Advisor" class="tm-card-avatar" src="<?php echo base_url();?>assets/img/demo/avatar/team-6.jpg">
                    <div class="tm-social-icons tm-icons-visible">
                      <a class="uk-icon-button uk-icon-facebook" href="#" target="_blank"></a> <a class="uk-icon-button uk-icon-twitter" href="#" target="_blank"></a> <a class="uk-icon-button uk-icon-linkedin" href="#" target="_blank"></a>
                    </div>
                  </div><a class="tm-card-link" data-uk-modal="{center:true}" href="#team1"></a>
                  <div class="tm-card-content">
                    <h3 class="uk-panel-title">Yvonne Mason</h3>
                    <h4 class="tm-card-title">Asset Advisor</h4>
                  </div>
                </div>
              </div>
            </div>
          </div><br>
          <div class="uk-text-center">
            <a class="uk-button-primary uk-button-large uk-button" href="our-team-basic.html" target="_self">See all team members</a>
          </div>

          <!-- modal team box -->
          <div class="uk-modal" id="team1">
            <div class="uk-modal-dialog uk-modal-dialog-large uk-modal-dialog-lightbox tm-modal-dialog">
              <a class="uk-modal-close uk-close uk-close-alt" href=""></a>
              <div class="uk-grid uk-grid-collapse" data-uk-grid-margin="">
                <div class="uk-width-medium-1-2">
                  <div class="tm-modal-image"><img alt="Diane Moreno" class="uk-width-1-1" height="478" src="<?php echo base_url();?>assets/img/demo/avatar/team-1-big.jpg" width="420"></div>
                </div>
                <div class="uk-width-medium-1-2">
                  <div class="uk-panel uk-panel-space">
                    <h3 class="uk-module-title">Diane Moreno</h3>
                    <h4 class="uk-margin-remove">Finance Specialist</h4><br>
                    <p>Diane Moreno is an British Board Certified (BBE) and London Board Certified Periodontist (LBCP) who joined Sandal from The Endomics Dental Team.</p>
                    <p>She received her Doctorate in Medical Dentistry from the Boston University School of Dental. She later completed an extensive training in Conscious Sedation from Cambridge.</p><a class="uk-button-primary uk-button" href="#" target="_self">Get in touch</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
</div>