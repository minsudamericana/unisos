<div class="tm-preload">
  <div class="spinner"></div>
</div>

<div class="uk-sticky-placeholder">
  <div data-uk-smooth-scroll data-uk-sticky="{top:-500}">
    <a class="tm-totop-scroller uk-animation-slide-bottom" href="#" ></a>
  </div>
</div>