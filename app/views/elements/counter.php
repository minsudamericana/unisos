<!-- top c -->
<div class="tm-block-top-c uk-block uk-block-secondary tm-darker" id="tm-top-c">
  <div class="uk-container uk-container-center">
    <section class="tm-top-c uk-grid" data-uk-grid-margin="" data-uk-grid-match="{target:'> div > .uk-panel'}">
      <div class="uk-width-1-1">
        <div class="uk-panel uk-contrast">
          <div class="uk-grid uk-grid-width-small-1-2 uk-grid-width-medium-1-2 uk-grid-width-large-1-4" data-uk-grid-margin="">
            <div class="tm-counter uk-flex" data-uk-scrollspy="{cls:'uk-animation-fade', delay:100}">
              
              <!-- counter 1 -->
              <div class="tm-counter-icon uk-flex-item-none">
                <i class="uk-icon-7s-plugin"></i>
              </div>
              <div class="tm-counter-content uk-flex-item-1">
                <h2 class="tm-counter-number" data-duration="4000" data-end="12" id="tm1">12</h2>
                <h3 class="tm-counter-title">Projects done</h3>
              </div>
            </div>
            <div class="tm-counter uk-flex" data-uk-scrollspy="{cls:'uk-animation-fade', delay:100}">
              
              <!-- counter 2 -->
              <div class="tm-counter-icon uk-flex-item-none">
                <i class="uk-icon-7s-coffee"></i>
              </div>
              <div class="tm-counter-content uk-flex-item-1">
                <h2 class="tm-counter-number" data-duration="3000" data-end="65" id="tm2">65</h2>
                <h3 class="tm-counter-title">Cups of coffee</h3>
              </div>
            </div>
            <div class="tm-counter uk-flex" data-uk-scrollspy="{cls:'uk-animation-fade', delay:100}">
              
              <!-- counter 3 -->
              <div class="tm-counter-icon uk-flex-item-none">
                <i class="uk-icon-7s-note"></i>
              </div>
              <div class="tm-counter-content uk-flex-item-1">
                <h2 class="tm-counter-number" data-duration="2000" data-end="9300" id="tm3">9300</h2>
                <h3 class="tm-counter-title">Lines of code</h3>
              </div>
            </div>
            <div class="tm-counter uk-flex" data-uk-scrollspy="{cls:'uk-animation-fade', delay:100}">
              
              <!-- counter 4 -->
              <div class="tm-counter-icon uk-flex-item-none">
                <i class="uk-icon-7s-graph2"></i>
              </div>
              <div class="tm-counter-content uk-flex-item-1">
                <h2 class="tm-counter-number" data-duration="2500" data-end="140" id="tm4">140</h2>
                <h3 class="tm-counter-title">Web pages</h3>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
</div>