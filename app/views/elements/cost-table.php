<!-- bottom-a -->
<div class="tm-block-bottom-a uk-block uk-block-default" id="tm-bottom-a">
  <div class="uk-container uk-container-center">
    <section class="tm-bottom-a uk-grid" data-uk-grid-margin="" data-uk-grid-match="{target:'> div > .uk-panel'}">
      <div class="uk-width-1-1">
        <div class="uk-panel">
          <div class="uk-text-center">
            <h2 class="uk-module-title-alt">Choose a plan</h2>
          </div><br>
          <div class="tm-pricing uk-grid-width-medium-1-4 uk-clearfix uk-margin-top">
            
          <!-- pricing column -->
            <div class="tm-price-column uk-float-left" data-uk-scrollspy="{cls:'uk-animation-slide-right', delay:300}">
              <div class="tm-price-title">
                Basic
              </div>
              <div class="tm-price-tag">
                <h3>from £9 a month</h3>
              </div>
              <div class="tm-price-content">
                <ul class="tm-price-list uk-float-left uk-width-1-1">
                  <li><i class="uk-icon-check"></i>24/5 Support</li>
                  <li><i class="uk-icon-check"></i>3 months Renewal</li>
                  <li><i class="uk-icon-check"></i>1 User Licence</li>
                  <li><i class="uk-icon-check"></i>No Contracts</li>
                  <li><i class="uk-icon-check"></i>1 Domain</li>
                </ul>
                <div class="tm-price-bottom uk-text-center">
                  <a class="uk-button uk-button-line" href="#">Get quote</a>
                </div>
              </div>
            </div>
            
            <!-- pricing column -->
            <div class="tm-price-column uk-float-left" data-uk-scrollspy="{cls:'uk-animation-slide-right', delay:100}">
              <div class="tm-price-title">
                Premium
              </div>
              <div class="tm-price-tag">
                <h3>from £19 a month</h3>
              </div>
              <div class="tm-price-content">
                <ul class="tm-price-list uk-float-left uk-width-1-1">
                  <li><i class="uk-icon-check"></i>24/6 Support</li>
                  <li><i class="uk-icon-check"></i>5 months Renewal</li>
                  <li><i class="uk-icon-check"></i>3 User Licences</li>
                  <li><i class="uk-icon-check"></i>No Contracts</li>
                  <li><i class="uk-icon-check"></i>3 Domain</li>
                </ul>
                <div class="tm-price-bottom uk-text-center">
                  <a class="uk-button uk-button-line" href="#">Get quote</a>
                </div>
              </div>
            </div>
            
            <!-- pricing column -->
            <div class="tm-price-column uk-float-left tm-table-focus tm-shift-top" data-uk-scrollspy="{cls:'uk-animation-fade', delay:50}">
              <div class="tm-price-title">
                Standard
              </div>
              <div class="tm-price-tag">
                <h3>from £29 a month</h3>
              </div>
              <div class="tm-price-content">
                <ul class="tm-price-list uk-float-left uk-width-1-1">
                  <li><i class="uk-icon-check"></i>24/6 Support</li>
                  <li><i class="uk-icon-check"></i>5 months Renewal</li>
                  <li><i class="uk-icon-check"></i>3 User Licences</li>
                  <li><i class="uk-icon-check"></i>No Contracts</li>
                  <li><i class="uk-icon-check"></i>Best option</li>
                  <li><i class="uk-icon-check"></i>3 Domain</li>
                </ul>
                <div class="tm-price-bottom uk-text-center">
                  <a class="uk-button uk-button-line" href="#">Get quote</a>
                </div>
              </div>
            </div>
            
            <!-- pricing column -->
            <div class="tm-price-column uk-float-left" data-uk-scrollspy="{cls:'uk-animation-slide-left', delay:400}">
              <div class="tm-price-title">
                Developer
              </div>
              <div class="tm-price-tag">
                <h3>from £199 a month</h3>
              </div>
              <div class="tm-price-content">
                <ul class="tm-price-list uk-float-left uk-width-1-1">
                  <li><i class="uk-icon-check"></i>24/7 Support</li>
                  <li><i class="uk-icon-check"></i>7 months Renewal</li>
                  <li><i class="uk-icon-check"></i>5 User Licences</li>
                  <li><i class="uk-icon-check"></i>No Contracts</li>
                  <li><i class="uk-icon-check"></i>5 Domain</li>
                </ul>
                <div class="tm-price-bottom uk-text-center">
                  <a class="uk-button uk-button-line" href="#">Get quote</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
</div> 