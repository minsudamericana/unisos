<!-- bottom-b -->
<div class="tm-block-bottom-b uk-block uk-block-default tm-block-fullwidth tm-grid-collapse" id="tm-bottom-b">
  <div class="uk-container uk-container-center">
    <section class="tm-bottom-b uk-grid" data-uk-grid-margin="" data-uk-grid-match="{target:'> div > .uk-panel'}">
      <div class="uk-width-1-1">
        <div class="uk-panel uk-contrast tm-inner-padding-large tm-overlay-secondary">

          <!-- background cover -->
          <div class="tm-background-cover uk-cover-background uk-flex uk-flex-center uk-flex-middle" data-uk-parallax="{bg: '-200'}" style="background-position: 50% 0px; background-image:url(assets/images/background/bg-image-1.jpg)">
            <div class="uk-position-relative uk-container tm-inner-container">
              <div class="uk-grid" data-uk-grid-margin="">

                <div class="uk-width-medium-1-2">
                  <div class="tm-rotate-text uk-flex">
                    <h2 class="uk-h1 uk-module-title tm-thin-font">Nuestra Filosofia: </h2>
                    <h2 class="uk-h1">
                      <span class="tm-word">Profesionalidad.</span>
                      <span class="tm-word">Creatividad.</span>
                      <span class="tm-word">Innovacion.</span>
                      <span class="tm-word">Seriedad.</span>
                    </h2>
                  </div>
                  <p>Nuestra Filosofia es apoyar a las PYME y brindarles el apoyo que necesitan. Desde nuestro equipo de profesionales en todas las areas, brindamos servicios de capacitacion, acompañamiento y gestion de sus proyectos.</p>
                  <a class="uk-button-line uk-button" href="<?php echo base_url() . 'internal/faq';?>" target="_self">Quiero Saber Mas</a>
                </div>

                <div class="uk-width-medium-1-2 uk-flex uk-flex-center uk-flex-middle">
                  <div data-uk-scrollspy="{cls:'uk-animation-slide-left', delay:100}"><img alt="demo" height="355" src="<?php echo base_url();?>assets/img/demo/default/content/your-idea.png" width="512"></div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
</div>