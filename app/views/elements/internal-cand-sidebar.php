<aside class="tm-sidebar-b uk-width-medium-3-10">
  
  <div class="uk-panel uk-panel-box tm-panel-box-primary-light uk-border-rounded industrial-list">
    <h3 class="uk-panel-title list-title">Directorio de Empresas</h3>
      <?php foreach ($categories->result() as $candCat): ?>
        <a href="<?php echo base_url() . 'rrhh/directory/' . $candCat->slug;?>">
          <h3 class="uk-accordion-title">
            <?php echo $candCat->title; ?>
          </h3>
        </a>
      <?php endforeach ?>
  </div>

</aside>

<style>
  .industrial-list {
    padding: 10px 10px;
  }
  .industrial-list .list-title {
    margin-bottom: 5px;
    padding: 0px 10px 0 10px;
    font-weight: bold;
  }
  .industrial-list .uk-accordion {
    margin: 0;
    padding: 0;
  }
  .industrial-list .uk-accordion .uk-accordion-title {
    margin: 0;
    border: none;
    padding: 5px 10px;
  }
  .industrial-list .uk-accordion .uk-accordion-content {
    padding: 5px 15px;
    border: none;
  }
  .industrial-list .uk-accordion .uk-accordion-content a {
    float: left;
    width: 100%;
  }
</style>