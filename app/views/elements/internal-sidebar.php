<aside class="tm-sidebar-b uk-width-medium-3-10">
  
  <div class="uk-panel uk-panel-box tm-panel-box-primary-light uk-border-rounded">
    <ul class="uk-nav uk-nav-parent-icon uk-nav-side" data-uk-nav="{}">

      <?php if ($this->uri->segment(1) == 'software'): ?>
      <li class="uk-active">
      <?php else: ?>
      <li>  
      <?php endif ?>
        <a href="<?php echo base_url();?>Software/home">
          Software
        </a>
      </li>
      <?php if ($this->uri->segment(1) == 'elearning'): ?>
      <li class="uk-active">
      <?php else: ?>
      <li>  
      <?php endif ?>
        <a href="<?php echo base_url();?>elearning">
          E-Learning
        </a>
      </li>
      <?php if ($this->uri->segment(1) == 'rrhh'): ?>
      <li class="uk-active">
      <?php else: ?>
      <li>  
      <?php endif ?>
        <a href="<?php echo base_url();?>rrhh">
          RRHH
        </a>
      </li>
      <?php if ($this->uri->segment(1) == 'industrial'): ?>
      <li class="uk-active">
      <?php else: ?>
      <li>  
      <?php endif ?>
        <a href="<?php echo base_url();?>industrial">
          Empresas
        </a>
      </li>
      <?php if ($this->uri->segment(1) == 'foro'): ?>
      <li class="uk-active">
      <?php else: ?>
      <li>  
      <?php endif ?>
        <a href="<?php echo base_url();?>foro">
          Foro
        </a>
      </li>
      <?php if ($this->uri->segment(2) == 'faq'): ?>
      <li class="uk-active">
      <?php else: ?>
      <li>  
      <?php endif ?>
        <a href="<?php echo base_url();?>internal/faq">
          Preguntas Frecuentes
        </a>
      </li>
      <?php if ($this->uri->segment(2) == 'blog'): ?>
      <li class="uk-active">
      <?php else: ?>
      <li>  
      <?php endif ?>
        <a href="<?php echo base_url();?>blog">
          Gazeta Corporativa
        </a>
      </li>
    </ul>
  </div>

  <div class="uk-panel">
    <a class="uk-button-default uk-width-1-1 uk-margin-small-bottom uk-button" href="#" target="_self">
      <i class="uk-icon-file-pdf-o"></i> Descargar Instructivo
    </a>
  </div>
  
  <div class="uk-panel uk-panel-box uk-border-rounded tm-background-icon" style="border: 1px solid #258bce;overflow: hidden;">
    <h3 class="uk-panel-title"><i class="uk-icon-comments uk-margin-small-right"></i> Nuestras Redes Sociales</h3>
    <div class="fb-like" data-href="https://www.facebook.com/unisosoficial/" data-layout="standard" data-action="like" data-size="small" data-show-faces="true" data-share="true"></div>
    <a class="twitter-follow-button"
      href="https://twitter.com/unisos_sa">
    Seguir a @Unisos_sa
    </a>
    <script src="//platform.linkedin.com/in.js" type="text/javascript"> lang: es_ES</script>
    <script type="IN/FollowCompany" data-id="15243923" data-counter="right"></script>
    <br>
    <a class="github-button" href="https://github.com/unisos" data-style="mega" data-count-href="/unisos/followers" data-count-api="/users/unisos#followers" data-count-aria-label="# seguidores e  n GitHub" aria-label="Seguir @unisos en GitHub">Seguir @unisos</a>
  </div>

  <div class="uk-panel uk-panel-box uk-panel-box-primary uk-border-rounded tm-background-icon">
    <h3 class="uk-panel-title"><i class="uk-icon-at uk-margin-small-right"></i> ¿Dudas o Consultas?</h3>
    <p> Escribinos hoy y despeja todas tus dudas con nuestro equipo altamente capacitado.</p>
    <a class="uk-button-default uk-button" href="<?php echo base_url() ?>contact" target="_blank">Contactanos</a>
  </div>

</aside>