<!-- top-d -->
<div class="tm-block-top-d uk-block uk-block-default" id="tm-top-d">
  <div class="uk-container uk-container-center">
    <section class="tm-top-d uk-grid" data-uk-grid-margin="" data-uk-grid-match="{target:'> div > .uk-panel'}">
      <div class="uk-width-1-1">
        <div class="uk-panel">
          <div class="uk-text-center">
            <h2 class="uk-module-title-alt">Why choose us?</h2>
          </div>
          <div class="uk-grid uk-grid-width-small-1-2 uk-grid-width-medium-1-2 uk-grid-width-large-1-4" data-uk-grid-margin="">

            <!-- block -->
            <div class="uk-block">
              <div data-uk-scrollspy="{cls:'uk-animation-slide-top', delay:100}">
                <h3 class="uk-module-title"><span class="tm-primary-color">We</span> Design</h3>
                <div class="uk-flex">
                  <div class="tm-block-icon uk-icon-7s-alarm"></div>
                  <div class="tm-block-content">
                    Do not dwell in the past, do not dream of the future, do it now
                  </div>
                </div>
              </div>
            </div>

            <!-- block -->
            <div class="uk-block">
              <div data-uk-scrollspy="{cls:'uk-animation-slide-top', delay:300}">
                <h3 class="uk-module-title"><span class="tm-primary-color">We</span> Build</h3>
                <div class="uk-flex">
                  <div class="tm-block-icon uk-icon-7s-config"></div>
                  <div class="tm-block-content">
                    Do not dwell in the past, do not dream of the future, do it now
                  </div>
                </div>
              </div>
            </div>

            <!-- block -->
            <div class="uk-block">
              <div data-uk-scrollspy="{cls:'uk-animation-slide-top', delay:500}">
                <h3 class="uk-module-title"><span class="tm-primary-color">We</span> are You</h3>
                <div class="uk-flex">
                  <div class="tm-block-icon uk-icon-7s-graph2"></div>
                  <div class="tm-block-content">
                    Do not dwell in the past, do not dream of the future, do it now
                  </div>
                </div>
              </div>
            </div>

            <!-- block -->
            <div class="uk-block">
              <div data-uk-scrollspy="{cls:'uk-animation-slide-top', delay:650}">
                <h3 class="uk-module-title"><span class="tm-primary-color">We</span> Create</h3>
                <div class="uk-flex">
                  <div class="tm-block-icon uk-icon-7s-ribbon"></div>
                  <div class="tm-block-content">
                    Do not dwell in the past, do not dream of the future, do it now
                  </div>
                </div>
              </div>
            </div>

            <!-- block -->
            <div class="uk-block">
              <div data-uk-scrollspy="{cls:'uk-animation-slide-top', delay:750}">
                <h3 class="uk-module-title"><span class="tm-primary-color">We</span> Inspire</h3>
                <div class="uk-flex">
                  <div class="tm-block-icon uk-icon-7s-paint"></div>
                  <div class="tm-block-content">
                    Do not dwell in the past, do not dream of the future, do it now
                  </div>
                </div>
              </div>
            </div>

            <!-- block -->
            <div class="uk-block">
              <div data-uk-scrollspy="{cls:'uk-animation-slide-top', delay:800}">
                <h3 class="uk-module-title"><span class="tm-primary-color">We</span> are online</h3>
                <div class="uk-flex">
                  <div class="tm-block-icon uk-icon-7s-portfolio"></div>
                  <div class="tm-block-content">
                    Do not dwell in the past, do not dream of the future, do it now
                  </div>
                </div>
              </div>
            </div>

            <!-- block -->
            <div class="uk-block">
              <div data-uk-scrollspy="{cls:'uk-animation-slide-top', delay:950}">
                <h3 class="uk-module-title"><span class="tm-primary-color">We</span> Listen</h3>
                <div class="uk-flex">
                  <div class="tm-block-icon uk-icon-7s-signal"></div>
                  <div class="tm-block-content">
                    Do not dwell in the past, do not dream of the future, do it now
                  </div>
                </div>
              </div>
            </div>

            <!-- block -->
            <div class="uk-block">
              <div data-uk-scrollspy="{cls:'uk-animation-slide-top', delay:1100}">
                <h3 class="uk-module-title"><span class="tm-primary-color">We</span> Delegate</h3>
                <div class="uk-flex">
                  <div class="tm-block-icon uk-icon-7s-pin"></div>
                  <div class="tm-block-content">
                    Do not dwell in the past, do not dream of the future, do it now
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
</div>