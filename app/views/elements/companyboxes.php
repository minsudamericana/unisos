<div class="tm-block-top-a uk-block uk-block-default" id="tm-top-a" style="margin-bottom: 40px;">
  <div class="uk-container uk-container-center">
    <section class="tm-top-a uk-grid" data-uk-grid-margin="" data-uk-grid-match="{target:'> div > .uk-panel'}">
      <div class="uk-width-1-1">
    
            <div class="tm-article">
                <h3 class="uk-module-title"><span class="tm-primary-color">Nuestras</span> Empresas</h3>
                <?php foreach ($industrialList->result() as $ind): ?>
                  <?php if ($ind->status == 1): ?>
                    <div class="uk-width-medium-2-10" style="float:left; margin-right:5px;">
                      <a href="<?php echo base_url() . 'industrial/company/' . $ind->slug; ?>">
                        <div class="uk-panel uk-panel-box tm-panel-card" style="min-height: 180px; text-align: center;">
                          <div class="uk-panel-teaser">
                            <img class="tm-card-avatar" src="<?php echo base_url() . 'assets/uploads/files/industrial/' . $ind->logo; ?>" alt="Diane Moreno - CEO">
                          </div>
                          <div class="tm-card-content">
                            <h3 class="uk-panel-title"><?php echo $ind->title;?></h3>
                          </div>
                        </div>
                      </a>
                    </div>
                  <?php endif ?>
                <?php endforeach ?>
            </div>


      </div>
    </section>
  </div>
</div>