<!-- main -->
<div class="tm-block-main uk-block uk-block-default" id="tm-main">
  <div class="uk-container uk-container-center">
    <div class="tm-middle uk-grid" data-uk-grid-margin="" data-uk-grid-match="">
      <div class="tm-main uk-width-medium-1-1">
        <div class="tm-content-grid" id="tm-content-grid">
          <div class="tm-grid">
            <div class="">
              <div class="uk-text-center">
                <h3 class="uk-module-title-alt uk-margin-large-bottom">Testimonios de Clientes</h3><br>

                <!-- slideshow scroller -->
                <div class="tm-slideshow-scroller uk-slidenav-position" data-uk-slideshow="{autoplay:true, animation: 'fade', pauseOnHover: true, duration: 500, autoplayInterval: 10000, kenburns: false, slices: 25}">
                  <ul class="uk-slideshow uk-overlay-active">
                    
                    <!-- testimonial -->
                    <li>
                      <blockquote class="tm-testimonial uk-clearfix uk-text-left">
                        <i class="quote-icon uk-icon-7s-chat"></i>
                        <p><img alt="Gary Long" class="tm-testimonial-avatar uk-align-left uk-margin-top" height="64" src="<?php echo base_url();?>assets/img/demo/avatar/client-1-thumb.png" width="64">Believe in yourself! Have faith in your abilities! Without a humble but reasonable confidence in your own powers you cannot be successful or happy.</p>
                        <span class="author uk-margin-top">Gary Long</span>
                        <span class="jobtitle">CEO</span>
                      </blockquote>
                    </li>
                    
                    <!-- testimonial -->
                    <li>
                      <blockquote class="tm-testimonial uk-clearfix uk-text-left">
                        <i class="quote-icon uk-icon-7s-chat"></i>
                        <p><img alt="Lori Carroll" class="tm-testimonial-avatar uk-align-left uk-margin-top" height="64" src="<?php echo base_url();?>assets/img/demo/avatar/client-2-thumb.png" width="64">Do not dream of the future, concentrate the mind on the present moment. Dream big. Go forward Never allow yourself to be made a victim. Accept no one's definition.</p>
                        <span class="author uk-margin-top">Lori Carroll</span>
                        <span class="jobtitle">Chief Editor</span>
                      </blockquote>
                    </li>
                    
                    <!-- testimonial -->
                    <li>
                      <blockquote class="tm-testimonial uk-clearfix uk-text-left">
                        <i class="quote-icon uk-icon-7s-chat"></i>
                        <p><img alt="Paul Morgan" class="tm-testimonial-avatar uk-align-left uk-margin-top" height="64" src="<?php echo base_url();?>assets/img/demo/avatar/client-3-thumb.png" width="64">Everything should be as simple as it is, but not simpler; Believe that life is worth living and your belief will help create the fact. We help your ideas come to life.</p>
                        <span class="author uk-margin-top">Paul Morgan</span>
                        <span class="jobtitle">Creative Designer</span>
                      </blockquote>
                    </li>
                  </ul>

                  <!-- slideshow navigation button -->
                  <div class="uk-margin">
                    <ul class="uk-dotnav uk-flex-center uk-hidden-touch">
                      <li data-uk-slideshow-item="0"><a href=""></a></li>
                      <li data-uk-slideshow-item="1"><a href=""></a></li>
                      <li data-uk-slideshow-item="2"><a href=""></a></li>
                    </ul>
                  </div>

                  <!-- slideshow navigation arrows -->
                  <div class="tm-slidenav uk-flex uk-flex-right uk-flex-middle">
                    <a class="uk-slidenav uk-slidenav-previous uk-hidden-touch" data-uk-slideshow-item="previous" href=""></a>
                    <a class="uk-slidenav uk-slidenav-next uk-hidden-touch" data-uk-slideshow-item="next" href=""></a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>