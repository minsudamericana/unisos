<?php 
  $userID = getMyID();
  $industrialID = getIndustrialUserByID($userID);
?>

<?php if ($this->ion_auth->logged_in()): ?>
<?php else: ?>
<div id="tm-toolbar" class="tm-toolbar uk-hidden-small">
  <div class="uk-container uk-container-center uk-clearfix">
    <div class="uk-float-right">
      <div>
        <p style="float:left;"><small>Registra tu Empresa o Registrate como Candidato y disfruta de los beneficios</small></p>
        <a class="uk-button-primary uk-button-mini uk-button" href="<?php echo base_url();?>rrhh/candidate" target="_self" style="margin-left:5px;color:#FFF;">Candidato</a> 
        <a class="uk-button-success uk-button-mini uk-button" href="<?php echo base_url();?>Internal/newRegister" target="_self" style="color:#FFF;">Industria</a>
      </div>
    </div>
  </div>
</div>
<?php endif ?>


<div id="tm-toolbar" class="tm-toolbar uk-hidden-small">
  <div class="uk-container uk-container-center uk-clearfix">
    
    <?php if ($this->ion_auth->logged_in()): ?>
      <?php if ($this->ion_auth->in_group('candidates')): ?>
        <?php $this->load->view('elements/navbar-cand');?>
      <?php elseif($this->ion_auth->in_group('members')): ?>
        <?php $this->load->view('elements/navbar-ind');?>
      <?php endif ?>
    <?php else: ?> 
    
    
    <div class="uk-float-left">
      <div>
        <ul class="uk-subnav uk-subnav-line">
          <li><a class="tm-modal-link" href="#login-modal" data-uk-modal="{center:true}">Iniciar Sesion</a></li>
          <li><a href="<?php echo base_url();?>Internal/newRegister">Registrarme</a></li>
        </ul>
      </div>
    </div>
    <?php endif ?>
    <div class="uk-float-right">
      <div>
        <a href="<?php echo getContactNfo()['linkedin'];?>" class="uk-icon-button uk-icon-linkedin" target="_blank"></a>
        <a href="<?php echo getContactNfo()['facebook'];?>" class="uk-icon-button uk-icon-facebook" target="_blank"></a>
        <a href="<?php echo getContactNfo()['twitter'];?>" class="uk-icon-button uk-icon-twitter" target="_blank"></a>
        <a href="https://github.com/unisos" class="uk-icon-button uk-icon-github" target="_blank"></a>
      </div>
    </div>
  </div>
</div>

<div class="tm-header tm-header-right" data-uk-sticky>
  <div class="uk-container uk-container-center">
    <div class="uk-flex uk-flex-middle uk-flex-space-between">

      <a class="tm-logo uk-hidden-small" href="<?php echo base_url();?>">
        <img src="<?php echo base_url();?>assets/img/unisos-logo.png" width="180" height="50" alt="demo">
      </a>

      <a class="tm-logo-small uk-visible-small" href="<?php echo base_url();?>">
        <img src="<?php echo base_url();?>assets/img/demo/default/logo/logo-small.svg" width="140" height="40" alt="demo">
      </a>

      <div class="uk-flex uk-flex-right">
        <div class="uk-hidden-small">
          <nav class="tm-navbar uk-navbar tm-navbar-transparent">
            <div class="uk-container uk-container-center">
              <ul class="uk-navbar-nav uk-hidden-small">
                
                <?php foreach ($menuInt->result() as $mi): ?>
                  <?php if ($mi->menu_disp == 1): ?>
                    <?php if ($this->uri->segment(2) == $mi->slug): ?>
                    <li class="uk-parent uk-active">
                    <?php else: ?>
                    <li class="uk-parent">  
                    <?php endif ?>
                        <a href="<?php echo base_url() . 'page/' . $mi->slug; ?>">
                             <?php echo $mi->title; ?>
                        </a>
                    </li>
                  <?php endif ?>
                <?php endforeach ?>
                <?php if ($this->uri->segment(1) == 'Software'): ?>
                <li class="uk-parent uk-active">
                <?php else: ?>
                <li class="uk-parent">
                <?php endif ?>
                  <a href="<?php echo base_url();?>Software/Home">
                    Software
                  </a>
                </li>
                <?php if ($this->uri->segment(1) == 'elearning'): ?>
                <li class="uk-parent uk-active">
                <?php else: ?>
                <li class="uk-parent">
                <?php endif ?>
                  <a href="<?php echo base_url();?>elearning">
                    E-Learning
                  </a>
                </li>
                <?php if ($this->uri->segment(1) == 'rrhh'): ?>
                <li class="uk-parent uk-active">
                <?php else: ?>
                <li class="uk-parent">
                <?php endif ?>
                  <a href="<?php echo base_url();?>rrhh/">
                    RRHH
                  </a>
                </li>
                <?php if ($this->uri->segment(1) == 'foro'): ?>
                <li class="uk-parent uk-active">
                <?php else: ?>
                <li class="uk-parent">
                <?php endif ?>
                  <a href="<?php echo base_url();?>foro/">
                    Foro
                  </a>
                </li>
                <?php if ($this->uri->segment(2) == 'faq'): ?>
                <li class="uk-parent uk-active">
                <?php else: ?>
                <li class="uk-parent">
                <?php endif ?>
                  <a href="<?php echo base_url();?>internal/faq">
                    F.A.Q.
                  </a>
                </li>
                <?php if ($this->uri->segment(1) == 'industrial'): ?>
                <li class="uk-parent uk-active">
                <?php else: ?>
                <li class="uk-parent">
                <?php endif ?>
                  <a href="<?php echo base_url();?>industrial">
                    Empresas
                  </a>
                </li>
                <?php if ($this->uri->segment(1) == 'blog'): ?>
                <li class="uk-parent uk-active">
                <?php else: ?>
                <li class="uk-parent">
                <?php endif ?>
                  <a href="<?php echo base_url();?>blog">
                    Gazeta Corporativa
                  </a>
                </li>
              </ul>
            </div>
          </nav>
        </div>

        <a href="#offcanvas" class="uk-navbar-toggle uk-visible-small" data-uk-offcanvas></a>

        <div class="uk-navbar-content tm-navbar-more uk-visible-large uk-margin-left" data-uk-dropdown="{mode:'click'}">
          <a class="uk-link-reset"></a>
          <div class="uk-dropdown uk-dropdown-flip">
            <form action="#" class="uk-search" data-uk-search="" id="search-page" method="post" name="search-box">
              <input class="uk-search-field" name="searchword" placeholder="buscar..." type="text"> <input name="task" type="hidden" value="search"> 
              <input name="option" type="hidden" value=""> <input name="Itemid" type="hidden" value="502">
            </form>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>

<div id="tm-toolbar" class="tm-toolbar uk-hidden-small">
  <div class="uk-container uk-container-center uk-clearfix">
    <i class="uk-icon-quote-left" style="float:left; font-size: 10px; margin-right: 3px;"></i>
      <div id="fraseCont" style="float: left; font-size: 13px; width: auto; line-height: 15px;"></div>
    <i class="uk-icon-quote-right" style="float:left; font-size: 10px; margin-left: 3px;"></i>
  </div>
</div>

    