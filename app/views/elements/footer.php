<!-- bottom-c -->
<div class="tm-block-bottom-c uk-block uk-block-primary" id="tm-bottom-c">
  <div class="uk-container uk-container-center">
    <section class="tm-bottom-c uk-grid" data-uk-grid-margin="" data-uk-grid-match="{target:'> div > .uk-panel'}">
      <div class="uk-width-1-1">
        <div class="uk-panel">
          <div class="uk-grid" data-uk-grid-margin="">
            <div class="uk-width-medium-1-2">
              <h3 class="uk-margin-small-top">Subscribite a nuestro Newsletter para enterarte de las novedades</h3>
            </div>
            <div class="uk-width-medium-1-2">

              <form class="uk-form" action="Utilities/postMailtoList" method="POST" id="newsletterForm">
                <div id="alert-msg-subscribe" class="alert-msg"></div>
                <div class="uk-grid uk-grid-small" data-uk-grid-margin>

                  <div class="uk-width-medium-2-5">
                    <div><input type="text" placeholder="Tu nombre" name="fname" id="fname" class="uk-width-1-1" required="required"></div>
                  </div>

                  <div class="uk-width-medium-2-5">
                    <div><input type="email" placeholder="E-Mail" name="email" id="email" class="uk-width-1-1" required="required"></div>
                  </div>

                  <div class="uk-width-medium-1-5">
                    <div class="form-group uk-margin-remove">
                      <button id="subscribe_button" type="submit" class="uk-button uk-button-default">Subscribirme</button>
                    </div>
                  </div>
                  
                  <div id="loadingDiv" style="z-index: 200; position: absolute; display:none;">
                    <img src="<?php echo base_url();?>assets/css/AjaxLoader.gif"> 
                  </div>    
                  <span class="alert alert-success alert-mail" role="alert" id="messageNewsletter" style="display:none;"></span>                      
                  <span class="alert alert-danger alert-mail" role="alert" id="newsletterError" style="display:none;">Se produjo un error</span>


                </div>
              </form>

            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
</div>

<a class="uk-button" id="mailChimpTrigger" href="#mailingConfirm" data-uk-modal style="display:none;"></a>
<div id="mailingConfirm" class="uk-modal">
  <div class="uk-modal-dialog uk-panel-box-primary">
    <a href="" class="uk-modal-close uk-close"></a>
    <p>
      Su Email se Agrego Correctamente a nuestra lista. <br>
      Por favor confirmar el correo que se le envio a su casilla.
    </p>
  </div>
</div>

<div class="tm-block-bottom-d uk-block uk-block-secondary tm-overlay-6" id="tm-bottom-d">
  <div class="uk-container uk-container-center">
    <section class="tm-bottom-d uk-grid" data-uk-grid-margin="" data-uk-grid-match="{target:'> div > .uk-panel'}">
      
      <div class="uk-width-1-1 uk-width-medium-1-4">
        <div class="uk-panel">
          <img alt="logo" height="40" src="<?php echo base_url();?>assets/img/unisos-logo-2.png" width="140">
          <p>
            <?php echo getSiteConfiguration()['site_name'];?><br>
            <?php // echo getContactNfo()['adress'];?><br>
            <br>
            <?php // echo getContactNfo()['phone'];?><br>
            <?php echo getContactNfo()['email'];?><br>
          </p>
        </div>
      </div>

      <div class="uk-width-1-1 uk-width-medium-1-4">
        <div class="uk-panel">
          <h3 class="uk-h3 uk-module-title uk-margin-bottom">Ultimas Noticias</h3>
          <div class="tm-item-block">
            <ul>
              <?php $limiter = 0; ?>
              <?php foreach ($postLst->result() as $pst): ?>
                <?php if ($limiter <= 2): ?>
                  <li>
                    <a href="<?php echo base_url() . 'blog/' . $pst->slug;?>"><?php echo $pst->title; ?></a> 
                    <div class="uk-margin-small-top">
                      <?php echo $pst->description; ?>
                    </div>
                  </li>
                  <?php $limiter = $limiter + 1; ?>
                <?php endif; ?>
              <?php endforeach; ?>
            </ul>
          </div>
        </div>
      </div>
      
      <div class="uk-width-1-1 uk-width-medium-1-4">
        <div class="uk-panel">
          <h3 class="uk-h3 uk-module-title uk-margin-bottom">Secciones</h3>
          <ul class="uk-list list-icons">
            <li>
              <i class="uk-icon-angle-right"></i>
              <a href="<?php echo base_url();?>Software/" target="_blank">Software</a>
            </li>
            <li>
              <i class="uk-icon-angle-right"></i>
              <a href="<?php echo base_url();?>elearning/" target="_blank">E-Learning</a>
            </li>
            <li>
              <i class="uk-icon-angle-right"></i>
              <a href="<?php echo base_url();?>rrhh/" target="_blank">RRHH</a>
            </li>
            <li>
              <i class="uk-icon-angle-right"></i>
              <a href="<?php echo base_url();?>foro/" target="_blank">Foro</a>
            </li>
            <li>
              <i class="uk-icon-angle-right"></i>
              <a href="<?php echo base_url();?>internal/faq" target="_blank">F.A.Q.</a>
            </li>
            <li>
              <i class="uk-icon-angle-right"></i>
              <a href="<?php echo base_url();?>industrial" target="_blank">Empresas</a>
            </li>
            <li>
              <i class="uk-icon-angle-right"></i>
              <a href="<?php echo base_url();?>contact" target="_blank">Contacto</a>
            </li>
          </ul>
        </div>
      </div>
      
      <div class="uk-width-1-1 uk-width-medium-1-4">
        <div class="uk-panel">
          <h3 class="uk-h3 uk-module-title uk-margin-bottom">Nuestros Valores</h3>
          <p>Nuestro compromiso es brindarle la mayor asistencia y acompañamiento a las PYME consolidades y las que estan en camino de mejorar sus procesos y recursos.</p>
        </div>
      </div>
    </section>
  </div>
</div>

<div class="tm-block-bottom-e uk-block uk-block-secondary" id="tm-bottom-e">
  <div class="uk-container uk-container-center">
    <section class="tm-bottom-e uk-grid" data-uk-grid-margin="" data-uk-grid-match="{target:'> div > .uk-panel'}">
      <div class="uk-width-1-1 uk-width-medium-1-2">
        <div class="uk-panel">
          <p>2016 &copy; <?php echo getSiteConfiguration()['site_name'];?>. Todos los derechos reservados.</p>
        </div>
      </div>
      <div class="uk-width-1-1 uk-width-medium-1-2">
        <div class="uk-panel">
          <div class="uk-text-right">
            <a href="<?php echo getContactNfo()['linkedin'];?>" class="uk-icon-button uk-icon-linkedin" target="_blank"></a>
            <a href="<?php echo getContactNfo()['facebook'];?>" class="uk-icon-button uk-icon-facebook" target="_blank"></a>
            <a href="<?php echo getContactNfo()['twitter'];?>" class="uk-icon-button uk-icon-twitter" target="_blank"></a>
            <a href="https://github.com/unisos" class="uk-icon-button uk-icon-github" target="_blank"></a>
          </div>
        </div>
      </div>
    </section>
  </div>
</div>

<div class="uk-modal" id="login-modal">
  <div class="uk-modal-dialog uk-panel-box uk-panel uk-panel-header uk-modal-dialog-small">
    <a class="uk-modal-close uk-close"></a>
    <div class="">
      <h3 class="uk-panel-title">Iniciar Sesion</h3>
      <form class="uk-form" action="<?php echo base_url();?>User/logUser" method="post" accept-charset="utf-8">
        <div class="uk-form-row">
          <input class="uk-width-1-1" name="logmail" id="logmail" placeholder="Usuario" size="18" type="text">
        </div>
        <div class="uk-form-row">
          <input class="uk-width-1-1" name="logpass" id="logpass" placeholder="Contraseña" size="18" type="password">
        </div>
        <div class="uk-form-row">
          <label for="remember-me">Mantener Sesion </label>
          <input checked name="logrem" id="logrem" type="checkbox" value="yes">
        </div>
        <div class="uk-form-row">
          <button class="uk-button uk-button-primary" name="Submit" type="submit" value="Log in">Iniciar Sesion</button>
          <div id="loadingDiv" style="width:100%;float:left;display:none;">
              <img src="<?php echo base_url();?>assets/img/AjaxLoader.gif"> Iniciando Sesion....
          </div>
          <span class="alert alert-success alert-mail" role="alert" id="contactOk" style="display:none;">Inicio sesion exitosamente</span>
          <span class="alert alert-danger alert-mail" role="alert" id="contactError" style="display:none;">Se produjo un error</span>
        </div>
        <ul class="uk-list uk-margin-bottom-remove">
          <li><a href="">Recordar Contraseña</a></li>
        </ul>
      </form>
    </div>
  </div>
</div>

<!-- <div class="uk-modal" id="register-modal">
  <div class="uk-modal-dialog uk-panel-box uk-panel uk-panel-header uk-modal-dialog-small">
    <a class="uk-modal-close uk-close"></a>
    <div class="">
      <h3 class="uk-panel-title">Registrarme</h3>
      <form id="registForm" action="<?php echo base_url();?>User/regUser" method="post" accept-charset="utf-8" class="uk-form">
        <div class="uk-form-row">
          <input class="uk-width-1-1" value="" id="regname" name="name" placeholder="Nombre" type="text">
        </div>
        <div class="uk-form-row">
          <input class="uk-width-1-1" value="" id="reglastname" name="lastname" placeholder="Apellido" type="text">
        </div>
        <div class="uk-form-row">
          <input class="uk-width-1-1" value="" id="regphone" name="phone" placeholder="Telefono" type="text">
        </div>
        <div class="uk-form-row">
          <input class="uk-width-1-1" value="" id="regmail" name="mail" placeholder="E-Mail" type="text">
        </div>
        <div class="uk-form-row">
          <input class="uk-width-1-1" value="" id="regpass" name="pass" placeholder="Contraseña" type="text">
        </div>
        <div class="uk-form-row">
          <input class="uk-width-1-1" value="" id="regrepeatpass" name="rpass" placeholder="Repetir Contraseña" type="text">
        </div>
        <input type="hidden" name="paisID" id="paisID" value="null">
        <input type="hidden" name="provinciaID" id="provinciaID" value="null">
        <input type="hidden" name="partidoID" id="partidoID" value="null">
        <input type="hidden" name="localidadID" id="localidadID" value="null">
        <input type="hidden" name="barrioID" id="barrioID" value="null">
        <input type="hidden" name="subbarrioID" id="subbarrioID" value="null">
        <div class="uk-form-row">
          <button class="uk-button uk-button-primary" name="Submit" type="submit" value="Log in">Registrarme</button>
          <div id="loadingDiv" style="width:100%;float:left;display:none;">
              <img src="<?php echo base_url();?>assets/img/AjaxLoader.gif"> Registrando....
          </div>
          <span class="alert alert-success alert-mail" role="alert" id="contactOk" style="display:none;">Se registro con Exito</span>
          <span class="alert alert-danger alert-mail" role="alert" id="contactError" style="display:none;">Se produjo un error</span>
        </div>
        <ul class="uk-list uk-margin-bottom-remove">
          <li><a href="">Inicia Sesion</a></li>
          <li><a href="">Recordar Contraseña</a></li>
        </ul>
      </form>
    </div>
  </div>
</div> -->


<script>
  // MailChimp Ajax Async Form Engine
  $( "#newsletterForm" ).submit(function( event ) {      
    
    event.preventDefault();

    var $form = $( this );            
    var urlForm = $form.attr('action');
    var name = $('#fname').val();
    var email = $('#email').val();
    
    
    $.ajax({
      method: 'POST',
      url: urlForm,
      data: { fname: name, email: email  },
      beforeSend: function() {
          $('#loadingDiv').show();
        },
    }).done(function(message) {
      $('#loadingDiv').hide();
      $('#messageNewsletter').css('display', 'none');
      $('#messageNewsletter').html(message);
      $('#subscribe_button').prop('disabled', true);
      $('#mailChimpTrigger').click();
    })
    .fail(function() {
      $('#loadingDiv').hide();
      $('#newsletterError').css('display', 'block');
      $('#subscribe_button').prop('disabled', true);
    });         
  });
</script>

<!-- Twitter Script -->
<script>window.twttr = (function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0],
    t = window.twttr || {};
  if (d.getElementById(id)) return t;
  js = d.createElement(s);
  js.id = id;
  js.src = 'https://platform.twitter.com/widgets.js';
  fjs.parentNode.insertBefore(js, fjs);

  t._e = [];
  t.ready = function(f) {
    t._e.push(f);
  };

  return t;
}(document, 'script', 'twitter-wjs'));</script>

<!-- Facebook SDK Script -->
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = '//connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v2.8&appId=885482321490748';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<!-- Cliengo Script -->
<script type="text/javascript">(function(){var ldk=document.createElement('script'); ldk.type='text/javascript'; ldk.async=true; ldk.src='http://weboptimizer.leadaki.com/weboptimizer/5831349fe4b092d5b71f2cd5/583134a1e4b092d5b71f2cd9.js' ; var s=document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ldk, s);})();</script>
<style>
  #converse-chat {
    right: 50px;
  }
</style>


<!-- Github Script. -->
<script async defer src="https://buttons.github.io/buttons.js"></script>

<!-- Analytics Script -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-87765604-1', 'auto');
  ga('send', 'pageview');

</script>

<script>
  function readTextFile(file, callback) {
      var rawFile = new XMLHttpRequest();
      rawFile.overrideMimeType("application/json");
      rawFile.open("GET", file, true);
      rawFile.onreadystatechange = function() {
          if (rawFile.readyState === 4 && rawFile.status == "200") {
              callback(rawFile.responseText);
          }
      }
      rawFile.send(null);
  }
  readTextFile("<?php echo base_url() ?>assets/json/phrases.json", function(text){
      var data = JSON.parse(text);
      var randomNumber = Math.floor(Math.random() * 32) + 1;
      $('#fraseCont').append(data.FrasesKaizen[randomNumber]);
  });
</script>