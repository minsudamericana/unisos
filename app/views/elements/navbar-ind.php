<?php 
  $userID = getMyID();
  $industrialID = getIndustrialUserByID($userID);
?>
<div class="uk-float-left">
  <div>
  Usuario: <?php echo getUserNfo($userID)['last_name'] . ', ' . getUserNfo($userID)['first_name'];?>
  </div>
  <div>
    <div class="uk-button-group">
      <div class="" data-uk-dropdown>
        <a class="uk-button-link uk-button" href="#" target="_self">Mi Cuenta<i class="uk-icon-caret-down"></i></a>
        <div class="uk-dropdown">
          <ul class="uk-nav uk-nav-dropdown">
            <li><a href="<?php echo base_url() . 'User/personalInfo';?>">Datos Personales</a></li>
            <li><a href="<?php echo base_url() . 'User/securityInfo';?>">Configuracion de Seguridad</a></li>
            <li><a href="<?php echo base_url() . 'User/logUserOut';?>">Salir</a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <div>
    <div class="uk-button-group">
      <div class="" data-uk-dropdown>
        <a class="uk-button-link uk-button" href="#" target="_self">Mi Industria<i class="uk-icon-caret-down"></i></a>
        <div class="uk-dropdown">
          <ul class="uk-nav uk-nav-dropdown">
            <li><a href="<?php echo base_url() . 'industrial/company/' . getIndustrialSlug($industrialID);?>">Ver mi Industria</a></li>
            <li><a href="<?php echo base_url() . 'industrial/editIndustrial/' . $industrialID;?>">Editar mi Industria</a></li>
            <!-- <li><a href="<?php echo base_url() . 'membership/userMembership/20';?>">Estado de Cuenta</a></li> -->
            <li><a href="<?php echo base_url() . 'rrhh/mycandidates/';?>">Ver mis Candidatos</a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <div>
    <div class="uk-button-group">
    <div class="" data-uk-dropdown>
      <a class="uk-button-link uk-button" href="#" target="_self">Mensajes: [<?php echo countNewMessages($userID);?>]<i class="uk-icon-caret-down"></i></a>
      <div class="uk-dropdown">
        <ul class="uk-nav uk-nav-dropdown">
          <li><a href="<?php echo base_url() . 'User/viewMsg/1';?>">Mensajes Nuevos</a></li>
          <li><a href="<?php echo base_url() . 'User/viewMsg/2';?>">Mensajes Leidos</a></li>
          <li><a href="<?php echo base_url() . 'User/viewMsg/3';?>">Mensajes Archivados</a></li>
        </ul>
      </div>
    </div>
  </div>
  </div>
</div>