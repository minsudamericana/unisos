<!doctype html>
<html lang="<?php echo $language;?>">
<head>
    <?php $this->load->view('assets/headnfo');?>
    <?php $this->load->view('assets/style');?>
</head>
<body class="home">
<?php $this->load->view('elements/navbar');?>
<div class="wrapper">
  <?php if ( is_array( $cartList->result()) && !empty($cartList->result())): ?>
    <div class="section">
           <div class="container">
               <h2 class="section-title">Tu Carrito</h2>
               <div class="row">
                <div class="col-md-12">
                  <div class="table-responsive">
                    <form id="confirmBuyForm" name="confirmBuyForm" method="POST" action="cartProceed">
                      <table class="table table-shopping">
                        <thead>
                          <tr>
                            <th class="text-center">
                            </th>
                            <th>
                              Producto
                            </th>
                            <th class="th-description">
                              Descripcion
                            </th>
                            <th class="text-right">
                              Precio
                            </th>
                            <th class="text-right">
                              Cant.
                            </th>
                            <th class="text-right">
                              Valor Final
                            </th>
                            <th>
                            </th>
                          </tr>
                        </thead>
                        <tbody class="cart-list">
                         
                          <?php /* modelo lista cart
                          <tr>
                            <td>
                              <div class="img-container">
                                <img src="http://www.aldeaecologica.com/images/stories/virtuemart/product/fitoplus-00011.jpg" alt="...">
                              </div>
                            </td>
                            <td class="td-name">
                               Moleskine Agenda
                            </td>
                            <td>
                              <b>Meeting Notes</b>
                              <p>
                                 Most beautiful agenda for the office.
                              </p>
                            </td>
                            <td class="td-number">
                              <small>$</small>49
                            </td>
                            <td class="td-number">
                              <small>x</small>25
                            </td>
                            <td class="td-number">
                              <small>$</small>1,225
                            </td>
                            <td class="td-actions">
                              <button type="button" rel="tooltip" data-placement="left" title="Remove item" class="btn btn-danger btn-simple btn-icon ">
                              <i class="fa fa-times"></i>
                              </button>
                            </td>
                          </tr>
                          */ ?>
                          <?php foreach ($cartList->result() as $cartItem) {  ?>
                          
                          <tr class="cart-item">
                            <td>                              
                              <div class="img-container">
                                <img src="<?php echo base_url() . 'assets/uploads/files/products/' . $cartItem->pic; ?>" alt="<?php echo $cartItem->name; ?>" style="float: left; width: 100%;"/>
                              </div>
                            </td>
                            <td class="td-name">
                              <?php echo  $cartItem->name ?>
                            </td>  
                            <td>                                                           
                              <?php echo $cartItem->desc ?>                            
                            </td>  

                            <td class="td-number">
                              <small>$</small><?php echo $cartItem->price ?>
                            </td>
                            <td class="td-number priceContainer">
                              <small>x</small>
                              <input type="hidden" name="priceProdUnit" class="priceProdUnit" value="<?php echo $cartItem->price ?>" ></input>
                              <input type="text" class="quantityProd" name="quantityProd" value="<?php echo $cartItem->prod_quantity; ?>"></input>
                              <a data-prod="<?php echo $cartItem->prod_id; ?>" class="quantityUp">+</a>
                              <a data-prod="<?php echo $cartItem->prod_id; ?>" class="quantityDown">-</a>                              
                            </td>
                            <td class="td-number totalPerProd">
                              <small>$</small><label name="priceProd" data-prod="<?php echo $cartItem->prod_id; ?>" class="totalPriceProd"> <?php echo $cartItem->price *  $cartItem->prod_quantity; ?> </label>
                            </td>
                            <td class="td-actions">
                              <button type="button" rel="tooltip" data-placement="left"  data-prod="<?php echo $cartItem->prod_id; ?>"  title="Remove item" class="deleteCartProd btn btn-danger btn-simple btn-icon ">
                              <i class="fa fa-times"></i>
                              </button>
                            </td>
                          </tr>  
                          <?php } ?>  

                          <?php /*
                          <ul class="cart-list">
                          <?php foreach ($cartList->result() as $cartItem) {  ?>
                            <li class="cart-item">
                                  <input type="hidden" name="priceProdUnit" class="priceProdUnit" value="<?php echo $cartItem->price ?>" ></input>
                                <label></label><br />
                                <input type="text" class="quantityProd" name="quantityProd" value="<?php echo $cartItem->prod_quantity; ?>"></input>
                                <button type="button" name="quantityUp" data-prod="<?php echo $cartItem->prod_id; ?>" class="quantityUp">Flechita pa arriba</button>
                                <button type="button" name="quantityDown" data-prod="<?php echo $cartItem->prod_id; ?>" class="quantityDown">Flechita pa abajo</button>
                                <button type="button" name="deleteProd" data-prod="<?php echo $cartItem->prod_id; ?>" class="deleteCartProd">eliminar prod</button>
                                  $<label name="priceProd" class="totalPriceProd"> <?php echo $cartItem->price *  $cartItem->prod_quantity; ?> </label>
                            </li>    
                          <?php } ?>
                        </ul>
                          
                        */ ?>                         
                        

                          <tr>
                            <td colspan="2">
                            </td>
                            <td>
                            </td>
                            <td class="td-total">
                               Total
                            </td>
                            <td class="td-price">
                              <small>$</small> <span id="cartTotal" /></span>
                            </td>
                            <td>
                              <button name="confirmBuy" id="confirmBuy" type="submit" class="btn btn-info btn-fill btn-l">Completar Compra <i class="fa fa-chevron-right"></i></button>
                            </td>
                            <td>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </form> 
                  </div>
                </div>
               </div>
           </div>
    </div>
    <?php else: ?>
    <div class="section">
       <div class="container">
           <h2 class="section-title">Tu Carrito</h2>
           <div class="row">
            <div class="col-md-12">
              <p>El Carrito se encuentra vacio...</p>
            </div>
           </div>
       </div>
    </div>
    <?php endif ?> 
    <div class="space-50"></div>
    <?php $this->load->view('elements/footer');?>
</div>
</body>
  <?php $this->load->view('assets/scripts');?>
  <script src="<?php echo base_url(); ?>assets/js/cart.js"></script>  
  <script>    
    $(document).ready(function(){
        calculateCartTotal();
    });
</script>
</html>