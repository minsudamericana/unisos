<!doctype html>
<html lang="<?php echo $language;?>">
<head>
    <?php $this->load->view('assets/headnfo');?>
    <?php $this->load->view('assets/style');?>
</head>
<body class="home">
<?php $this->load->view('elements/navbar');?>
<div class="wrapper">
    <div class="section">
           <div class="container">
               <h2 class="section-title">Confirmacion de compra</h2>
               <div class="row">
                    <table class="table table-shopping">
                        <thead>
                          <tr>
                            <th class="text-center">
                            </th>
                            <th>
                              Producto
                            </th>
                            <th class="th-description">
                              Descripcion
                            </th>
                            <th class="text-right">
                              Precio
                            </th>
                            <th class="text-right">
                              Cant.
                            </th>
                            <th class="text-right">
                              Valor Final
                            </th>
                            <th>
                            </th>
                          </tr>
                        </thead>
                        <tbody class="cart-list">
                    <?php if( is_array( $cartData) && !empty($cartData))  
                            foreach ($cartData as $cartItem) {  ?>
                        <tr>
                            <td>
                              <div class="img-container">
                                <img src="<?php echo base_url() . 'assets/uploads/files/products/' . $cartItem->pic; ?>" alt="<?php echo $cartItem->name; ?>" style="float: left; width: 100%;"/>
                              </div>
                            </td>
                            <td class="td-name">
                                <?php echo $cartItem->name ?> 
                            </td>
                            <td>
                              <?php echo $cartItem->desc ?> 
                            </td>
                            <td class="td-number">
                              <small><?php echo getStoreConfiguration()['currency'];?></small><?php echo $cartItem->price ?>
                            </td>
                            <td class="td-number">
                              <small>x</small><?php echo $cartItem->prod_quantity; ?>
                            </td>
                            <td class="td-number">
                              <small><?php echo getStoreConfiguration()['currency'];?></small> <span class="totalPriceProd"><?php echo $cartItem->price * $cartItem->prod_quantity; ?></span>
                            </td>
                          </tr>
                        <?php } ?>
                            <tr>
                                <td colspan="6"> Total: <?php echo getStoreConfiguration()['currency'];?><span id="cartTotal" /></span> </td>
                            </tr>
                      </tbody>
                    </table>
                </div>
               
                <div id="userData" class="centerForm">
                    
                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label for="promoCodeInsert">Codigo de Promoción</label> 
                          <input class="form-control" type="text"  name="promoCodeInsert" id="promoCodeInsert" value="" ></input>
                          <div class="promoCodeNone alert alert-danger" style="display:none;">El Codigo Promocional es invalido</div>
                          <div class="promoCodeValid alert alert-success" style="display:none;">El Codigo Promocional fue Procesado Correctamente!</div>
                          <div class="discountShow" style="display:none;">
                            <div>Precio Regular: <?php echo getStoreConfiguration()['currency'];?> <span class="regularPricePromoCode"></span></div>
                            <div>Precio con Descuento: <?php echo getStoreConfiguration()['currency'];?> <span class="discPricePromoCode"></span></div> 
                          </div>
                        </div>
                      </div>
                    </div>

                    <form  method="POST" action="confirmData">
                      <legend>Datos de Envio</legend>
                      <div class="form-group">
                        <label for="nombre">Nombre</label> 
                        <input class="form-control" type="text"  name="nombre" id="nombre" <?php if($this->ion_auth->logged_in()) { ?> disabled="true" value="<?php echo $usrInfo['first_name'] ?>" <?php } ?>></input>
                      </div>
                      <div class="form-group">
                        <label for="apellido">Apellido</label> 
                        <input class="form-control" type="text"  name="apellido" id="apellido" <?php if($this->ion_auth->logged_in()) { ?> disabled="true" value="<?php echo $usrInfo['last_name']  ?>" <?php } ?> ></input>
                      </div>                            
                      <div class="form-group">
                        <label for="telefono">Telefono</label>
                        <input class="form-control" type="text" name="telefono" id="telefono" <?php if($this->ion_auth->logged_in()) { ?> value="<?php echo $usrInfo['phone'] ?>" <?php } ?>></input>
                      </div>                    
                      <div class="form-group">
                        <label for="email">Email</label>
                        <input class="form-control" type="text" name="email" id="email" <?php if($this->ion_auth->logged_in()) { ?> disabled="true" value="<?php echo $usrInfo['email'] ?>" <?php } ?> ></input>
                      </div>
                      <?php if(!$this->ion_auth->logged_in()) { ?> 
                      <div class="form-group">
                        <label for="compania">Compañia</label> 
                        <input class="form-control" type="text"  name="compania" id="compania"  value="<?php echo $usrInfo['company']  ?>" ></input>
                      </div>
                      <div class="form-group">
                        <label for="regpass">Contraseña</label> 
                        <input type="password" value="" id="regpass" name="pass" class="form-control">
                      </div>
                      <div class="form-group">
                        <label for="regrepeatpass">Repetir contraseña</label> 
                        <input type="password" value="" id="regrepeatpass" name="rpass" class="form-control">
                      </div>
                      <span class="alert alert-danger alert-mail" role="alert" id="incorrectPass" style="display:none;">Las Contraseñas no son iguales</span>
                      <?php } ?>                                            
                      <div class="form-group">
                        <label for="ciudad">Ciudad</label>
                        <input class="form-control" type="text" name="ciudad" id="ciudad" <?php if($this->ion_auth->logged_in()) { ?> value="<?php echo $usrInfo['city'] ?>" <?php } ?> ></input>
                      </div>
                      <div class="form-group">
                        <label for="direccion">Direccion</label>
                        <input class="form-control" type="text" name="direccion" id="direccion" <?php if($this->ion_auth->logged_in()) { ?> value="<?php echo $usrInfo['adress'] ?>" <?php } ?>></input>       
                      </div>
                      <div class="form-group">
                        <label for="codPostal">Codigo postal</label>
                        <input class="form-control" type="text" name="codPostal" id="codPostal" <?php if($this->ion_auth->logged_in()) { ?> value="<?php echo $usrInfo['zip'] ?>" <?php } ?> ></input>
                      </div>
                      <div class="form-group">
                        <label for="mensaje">Mensaje</label> 
                        <textarea class="form-control" rows="6" name="mensaje" id="mensaje"></textarea>   
                      </div>
                      <div class="form-group">
                        <label class="radio radio-blue">
                          <input type="radio" name="tipoEntrega" data-toggle="radio" id="contraentrega" value="<?php echo SALE_TYPE_CONTRAENTREGA ?>" checked>
                          <i></i>Contraentrega
                        </label>
                        <label class="radio radio-blue">
                          <input type="radio" name="tipoEntrega" data-toggle="radio" id="payu" value="<?php echo SALE_TYPE_PAYU ?>">
                          <i></i>PayU
                        </label>
                      </div> 
                      <input type="hidden" name="totalCompra" id="totalCompra" value="<?php echo $cartItem->price *  $cartItem->prod_quantity; ?>" ></input>
                      <input type="submit" class="btn btn-info btn-fill btn-block" value="Enviar Pedido" />
                    </form>
                </div>
               </div>
           </div>
    </div><!-- section -->
    <div class="space-50"></div>
    <?php $this->load->view('elements/footer');?>
</div>
</body>
    <?php $this->load->view('assets/scripts');?>
    <script src="<?php echo base_url(); ?>assets/js/cart.js"></script>
    <script>
      $(document).ready(function(){
        calculateCartTotal();
      });
    </script>
</html>



