<!doctype html>
<html lang="<?php echo $language;?>">
<head>
    <?php $this->load->view('assets/headnfo');?>
    <?php $this->load->view('assets/style');?>
</head>
<body class="home">
<?php $this->load->view('elements/navbar');?>
<div class="wrapper">
    <div class="section">
        <div class="container">
            <h2 class="section-title">Registro de Pagos PayU</h2>
            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>
                                        #
                                    </th>
                                    <th>
                                        Estado de la transaccion
                                    </th>
                                    <th>
                                        ID de la transaccion
                                    </th>
                                    <th>
                                        Referencia de la venta
                                    </th>
                                    <th>
                                        Referencia de la transaccion
                                    </th>
                                    <th>
                                        Valor total
                                    </th>
                                    <th>
                                        Moneda
                                    </th>
                                    <th>
                                        Descripción
                                    </th>
                                    <th>
                                        Entidad
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $numerator = 1; ?>
                                <?php foreach ($payuReg->result() as $sg): ?>
                                <tr>
                                    <td>
                                        <?php echo $numerator; ?>
                                    </td>
                                    <td>
                                        <?php echo $sg->estadotx; ?>
                                    </td>
                                    <td>
                                        <?php echo $sg->transactionid; ?>
                                    </td>
                                    <td>
                                        <?php echo $sg->reference_pol; ?>
                                    </td>
                                    <td>
                                        <?php echo $sg->referenceCode; ?>
                                    </td>
                                    <td>
                                        <?php echo $sg->TX_VALUE; ?>
                                    </td>
                                    <td>
                                        <?php echo $sg->currency; ?>
                                    </td>
                                    <td>
                                        <?php echo $sg->extra1; ?>
                                    </td>
                                    <td>
                                        <?php echo $sg->lapPaymentMethod; ?>
                                    </td>
                                </tr>
                                <?php $numerator = $numerator + 1; ?>
                                <?php endforeach ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>           
</div>
<div class="space-50"></div>
    <?php $this->load->view('elements/footer');?>
</div>
</body>
    <?php $this->load->view('assets/scripts');?>    
</html>