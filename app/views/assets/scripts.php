
  <!-- uikit -->
  <script src="<?php echo base_url();?>assets/vendor/uikit/js/uikit.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url();?>assets/vendor/uikit/js/components/accordion.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url();?>assets/vendor/uikit/js/components/autocomplete.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url();?>assets/vendor/uikit/js/components/datepicker.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url();?>assets/vendor/uikit/js/components/grid.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url();?>assets/vendor/uikit/js/components/lightbox.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url();?>assets/vendor/uikit/js/components/parallax.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url();?>assets/vendor/uikit/js/components/pagination.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url();?>assets/vendor/uikit/js/components/slider.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url();?>assets/vendor/uikit/js/components/slideset.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url();?>assets/vendor/uikit/js/components/slideshow.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url();?>assets/vendor/uikit/js/components/slideshow-fx.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url();?>assets/vendor/uikit/js/components/sticky.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url();?>assets/vendor/uikit/js/components/timepicker.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url();?>assets/vendor/uikit/js/components/tooltip.min.js" type="text/javascript"></script>

  <!-- theme -->
  <script src="<?php echo base_url();?>assets/js/theme.js" type="text/javascript"></script>
  <script src="<?php echo base_url();?>assets/js/plyr.js" type="text/javascript"></script>