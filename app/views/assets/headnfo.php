<meta charset="<?php echo $charset;?>" />
<link rel="apple-touch-icon" sizes="76x76" href="<?php echo $appleicon;?>">
<link rel="icon" type="image/png" sizes="96x96" href="<?php echo $favicon;?>">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<title><?php echo $title;?></title>
<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
<meta name="viewport" content="width=device-width" />
<meta name="description" content="<?php echo $description;?>">
<meta name="keywords" content="<?php echo $keywords;?>">
<meta name="author" content="<?php echo $author;?>">


<!-- jquery -->
<script src="<?php echo base_url();?>assets/js/jquery.min.js" type="text/javascript"></script>

<script>
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '1009869059067304',
      xfbml      : true,
      version    : 'v2.6'
    });
  };
  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
</script>

<script type="text/javascript">
    window.smartlook||(function(d) {
    var o=smartlook=function(){ o.api.push(arguments)},h=d.getElementsByTagName('head')[0];
    var c=d.createElement('script');o.api=new Array();c.async=true;c.type='text/javascript';
    c.charset='utf-8';c.src='//rec.smartlook.com/recorder.js';h.appendChild(c);
    })(document);
    smartlook('init', '442b14ae7394a7271c23c6eb15d42a15313bbaf6');
</script>