<!DOCTYPE html>
<html lang="<?php echo $language;?>">
<head>
  <?php $this->load->view('assets/headnfo');?>
  <?php $this->load->view('assets/style');?>
</head>
<body id="tm-container">
  <div class="tm-container">
    <?php $this->load->view('elements/component');?>
    
    <?php $this->load->view('elements/navbar');?>
    
    <?php $this->load->view('elements/spotlight-internal');?>

    <?php $this->load->view('elements/breadcrum-gen');?>

    <div id="tm-main" class="tm-block-main uk-block uk-block-default">
      <div class="uk-container uk-container-center">
        <div class="tm-middle uk-grid" data-uk-grid-match data-uk-grid-margin>
          
          <div class="tm-main uk-width-medium-7-10">
            <main id="tm-content" class="tm-content">
              <article class="uk-article tm-article">
                <div class="tm-article-wrapper">
                  
                  <div class="tm-article-content uk-margin-large-bottom uk-margin-top-remove">
                    <div class="tm-article">
                      <div class="uk-width-1-1">
                        <img class="uk-thumbnail-expand" src="<?php echo base_url();?>assets/images/stock/software.jpg" alt="">
                        <h2 class="uk-module-title">Actualice hoy los sistemas internos de su Empresa</h2>
                        <p>
                        </p>
                        <h3>Gestion de Calidad ISO</h3>
                        <ul>
                          <li><a href="<?php echo base_url() ?>Software/getIsoft">iSoft</a></li>
                        </ul>
                        <h3>Planificacion de Recursos Empresariales (ERP)</h3>
                        <ul>
                          <li><a href="<?php echo base_url() ?>page/sap">SAP Bussiness One</a></li>
                          <li><a href="<?php echo base_url() ?>page/softland">Softland</a></li>
                        </ul>
                        <h3>Sistemas de Gestion Contable</h3>
                        <ul>
                          <li><a href="<?php echo base_url() ?>page/tango-gestion">Tango Gestion</a></li>
                          <li><a href="<?php echo base_url() ?>page/sistemas-bejerman">Sistemas Bejerman</a></li>
                        </ul>
                      </div>
                    </div>
                  </div>

                </div>
              </article>
            </main>
          </div>

          <?php  $this->load->view('elements/internal-sidebar');?>
        </div>
      </div>
    </div>
   
    <?php $this->load->view('elements/footer');?>

  </div>
  <?php $this->load->view('assets/scripts');?>
</body>
</html>