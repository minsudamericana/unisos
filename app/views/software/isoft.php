<!DOCTYPE html>
<html lang="<?php echo $language;?>">
<head>
  <?php $this->load->view('assets/headnfo');?>
  <?php $this->load->view('assets/style');?>
</head>
<body id="tm-container">
  <div class="tm-container">
    <?php $this->load->view('elements/component');?>
    
    <?php $this->load->view('elements/navbar');?>
    
    <?php $this->load->view('elements/spotlight-internal');?>

    <?php $this->load->view('elements/breadcrum-gen');?>

    <div id="tm-main" class="tm-block-main uk-block uk-block-default">
      <div class="uk-container uk-container-center">
        <div class="tm-middle uk-grid" data-uk-grid-match data-uk-grid-margin>
          
          <div class="tm-main uk-width-medium-7-10">
            <main id="tm-content" class="tm-content">
              <article class="uk-article tm-article">
                <div class="tm-article-wrapper">
                  <div class="tm-article-content uk-margin-large-bottom uk-margin-top-remove">
                    <div class="tm-article">
                      <img src="<?php echo base_url();?>assets/img/isoft-logo.png" alt="iSoft">
                      <h4 class="uk-article-title">
                        <small>
                          Sistema de Gestión de la Seguridad de Alimentos según ISO 22000:2005
                        </small>
                      </h4>
                      
                      <div class="uk-panel uk-panel-box">
                        <h3 class="uk-panel-title"><i class="uk-icon-envelope-o"></i> Demo</h3>
                        <p>Conozca iSoft accediendo a la Demo. <small>(recomendamos usar el soft en InternetExplorer)</small></p>
                        <p>
                          Accesos para iSoft                         
                        </p>
                        <table style="width:100%">
                          <tr>
                            <th>Usuario: ADMIN</th>
                            <th>Password: ADMIN</th> 
                            <th>
                              <a class="uk-button-line uk-button" href="http://isoft.isodata.com.ar/sys/demo/" target="_blank">Ingresar</a>
                            </th>
                          </tr>
                        </table>
                      </div>
                      <div class="tm-accordion uk-accordion" data-uk-accordion="{collapse:true}">

                        <h3 class="uk-accordion-title">
                          <i class="tm-toggle-button uk-float-right"></i>¿Cómo es posible implementar iSoft en mi empresa?
                        </h3>
                        <div class="uk-accordion-content">
                          iSoft posibilita dos formas de implementación: La implementación ASP o Application Service Provider, mediante la cual, iSoft hostea la aplicación en su Datacenter. Esta modalidad de implementación es totalmente segura y se garantiza la confidencialidad de la información, siendo el cliente el dueño de la misma. iSoft además, garantiza un acceso 24×7 al sistema y a la información. Es una solución ideal para cualquier tipo de empresa, en especial para aquellas empresas que por su tamaño no poseen un departamento de sistemas propio o una infraestructura necesaria para implementar el sistema. Esta modalidad también garantiza actualizaciones periódicas del sistema y una gran rapidez en la resolución de incidentes. Otro de las ventajas que se destacan, es que la empresa no tiene que desarrollar planes de resguardo de información o backup, ya que iSoft se encarga del resguardo de los datos.
                          <br><br>
                          Bajo esta modalidad, iSoft, garantiza que la empresa que contrata el servicio tiene su implementación dentro de las 24 hs hábiles desde que se acuerda el contrato. De forma complementaria y con la aceptación del cliente, dejando asentado en el contrato de servicio, personal especializado de iSoft, puede hacer evaluaciones periódicas del estatus del sistema de gestión, elevando dicho informe a quien corresponda de la empresa.
                          <br><br>
                          Otros puntos a tener en cuenta con este tipo de implementación son:
                          Seguridad y disponibilidad: La información del cliente se encuentra alojada en su espacio reservado. Se realizan copias de seguridad diarias. Los datos son siempre propiedad del cliente. El objetivo es disponer de un nivel de confidencialidad de los datos y de seguridad excelente, además de un nivel de disponibilidad del 100%. El cliente y la consultora establecen una nueva vía de comunicación Web, lo que refuerza el servicio prestado por la misma. El soporte tanto a nivel técnico como a nivel de consultoría es más sencillo. Otra forma de implementación, es alojar la aplicación en el datacenter del cliente, donde el cliente corre con todos los costos de hardware y software necesarios para el perfecto funcionamiento de la aplicación, como así también es el responsable de la seguridad y resguardo de su información.
                        </div>

                        <h3 class="uk-accordion-title">
                          <i class="tm-toggle-button uk-float-right"></i>¿Cuáles son los beneficios que me otorga implementar iSoft?
                        </h3>
                        <div class="uk-accordion-content">
                          Eficacia, dinamismo, accesibilidad, eliminación de papel, colaboración, ahorro de recursos, agilidad, organización, metodología, centralización de la información son algunas de las cualidades más destacadas de iSoft , además del rápido acceso desde cualquier lugar en el que se encuentre. Es muy útil para organizaciones multi-sitios que necesitan llevar el sistema de gestión de forma integrada y bajo una sola y única interfaz.
                          <br><br>
                          Para organizaciones certificadas<br>
                          iSoft se constituye como una excelente solución para la mejora continua. La informatización de un sistema de gestión maduro optimiza la eficacia y eficiencia del mismo. Es la herramienta ideal para integrar y escalar hacia otras normas y modelos de gestión: Calidad, Medio Ambiente, Riesgos Laborales, etc.
                          <br><br>
                          Para organizaciones en proceso de implementación<br>
                          La sencillez y claridad de iSoft se convierte en la perfecta guía durante el transcurso de la implementación de un sistema de gestión. Desde el inicio, iSoft acompaña a la organización a incorporar todos los elementos y facilitar la comunicación y compromiso de todas las partes.
                          <br><br>
                          Para PYMES<br>
                          iSoft es una herramienta sencilla y escalable, por lo que se adapta a sistemas y empresas sencillas y ágiles. El sistema de trabajo a través de Internet es ideal para pymes que no tengan un departamento de sistemas o los recursos que puedan destinar sean mínimos.
                          <br><br>
                          Para Grandes Organizaciones
                          El método de perfiles de acceso de iSoft permite gestionar de manera integral sistemas organizacionales complejos y de gran dotación. Organizaciones con diferentes sedes en diferentes localizaciones pueden gestionar desde un único soporte y una única base de datos toda la información.
                        </div>

                        <h3 class="uk-accordion-title">
                          <i class="tm-toggle-button uk-float-right"></i>¿Es costoso implementar iSoft?
                        </h3>
                        <div class="uk-accordion-content">
                          Por el contrario, iSoft posee un valor altamente competitivo. Empleamos recursos locales y los costos de implementación se cotizan de forma completamente transparente dada la complejidad de los procesos de su empresa, y la cantidad de usuarios que tendrán acceso al sistema. Los costos incluyen la implementación y capacitación de todos los usuarios así como actualizaciones y upgrades anuales.
                        </div>

                        <h3 class="uk-accordion-title">
                          <i class="tm-toggle-button uk-float-right"></i>¿Es posible contratar solo algunos de los módulos del sistema?
                        </h3>
                        <div class="uk-accordion-content">
                          Si, por supuesto. iSoft es fácilmente adaptable y modular. No hay necesidad de que una empresa adquiera la totalidad de las aplicaciones y módulos en forma simultánea. De este modo, no tienen que pagar de más por aquellos aspectos de la aplicación que probablemente nunca vayan a usar. A su vez, el sistema también permite crear módulos específicos de acuerdo a los requerimientos de cada cliente. Contamos con un equipo de programadores altamente capacitados para poder ofrecerle una solución a medida. Una empresa puede tener sus procesos automatizados a través de workflows que deleguen las tareas a miembros específicos del personal y luego realizar un seguimiento del desarrollo y estado de cualquier documento y procedimiento.
                        </div>

                        <h3 class="uk-accordion-title">
                          <i class="tm-toggle-button uk-float-right"></i>¿iSoft garantiza actualizaciones y soporte técnico?
                        </h3>
                        <div class="uk-accordion-content">
                          iSoft garantiza a sus clientes que siempre tendrán una aplicación actualizada y a tono con los estándares de la industria y las tendencias del mercado en sistemas de gestión. Porque además de trabajar junto a desarrolladores, nuestro equipo está formado por auditores líderes de todas las disciplinas que se encuentran constantemente actualizados de las ultimas actualizaciones y requerimientos normativos.
                        </div>

                        <h3 class="uk-accordion-title">
                          <i class="tm-toggle-button uk-float-right"></i>¿Es seguro instalar iSoft?
                        </h3>
                        <div class="uk-accordion-content">
                          iSoft controla el acceso a través de perfiles configurables de usuarios. Además la seguridad de la información alojada en nuestras servidores es garantizada mediante la realización de backups.
                        </div>

                        <h3 class="uk-accordion-title">
                          <i class="tm-toggle-button uk-float-right"></i>¿iSoft cumple con los requisitos de las normas de calidad?
                        </h3>
                        <div class="uk-accordion-content">
                          iSoft fue ideado con el objetivo de ayudar a las empresas a cumplir con las más estrictas normas de aseguramiento de la calidad, medio ambiente y seguridad ocupacional e inocuidad alimentaria.
                        </div>

                        
  


                      </div>

                    </div>
                  </div>
                </div>
              </article>
            </main>
          </div>
          
          <?php  $this->load->view('elements/internal-sidebar');?>

        </div>
      </div>
    </div>
   
    <?php $this->load->view('elements/footer');?>

  </div>
  <?php $this->load->view('assets/scripts');?>
</body>
</html>