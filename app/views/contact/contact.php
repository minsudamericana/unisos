<!DOCTYPE html>
<html lang="<?php echo $language;?>">
<head>
  <?php $this->load->view('assets/headnfo');?>
  <?php $this->load->view('assets/style');?>
</head>
<body id="tm-container">
  <div class="tm-container">
    <?php $this->load->view('elements/component');?>
    
    <?php $this->load->view('elements/navbar');?>
    
    <?php $this->load->view('elements/spotlight-internal');?>

    <?php $this->load->view('elements/breadcrum-gen');?>

    <div id="tm-main" class="tm-block-main uk-block uk-block-default">
      <div class="uk-container uk-container-center">
        <div class="tm-middle uk-grid" data-uk-grid-match data-uk-grid-margin>
          
          <div class="tm-main uk-width-medium-1-2">
            <main id="tm-content" class="tm-content">
              <article class="uk-article tm-article">
                <div class="tm-article-wrapper">
                  <div class="tm-article-content uk-margin-large-bottom uk-margin-top-remove">
                    <div class="tm-article">
                      
                      <div class="contact">
                        <h3 class="uk-panel-title">Contacto</h3>
                        <form role="form" id="contactForm" action="<?php echo base_url();?>Contact/contactSent" method="post" accept-charset="utf-8" class="uk-form uk-form-stacked">
                          <div class="uk-grid uk-grid-width-medium-1-1 uk-grid-width-1-1">
                            <div>
                              <div id="alert-msg-contact" class="alert-msg uk-margin-bottom"></div>
                            </div>
                            <div class="uk-form-icon uk-grid-margin">
                              <i class="uk-icon-user"></i>
                              <input type="text" class="uk-width-1-1" name="name" id="name" placeholder="Nombre" size="30" required="required">
                            </div>
                            <div class="uk-form-icon uk-grid-margin">
                              <i class="uk-icon-envelope"></i>
                              <input type="email" class="uk-width-1-1" name="email" id="email" placeholder="E-Mail" size="30" required="required">
                            </div>
                            <div class="uk-form-icon uk-grid-margin">
                              <i class="uk-icon-bookmark"></i>
                              <input type="text" class="uk-width-1-1 optional" name="subject" id="subject" placeholder="Asunto" size="60">
                            </div>
                            <div class="uk-grid-margin">
                                <textarea class="uk-width-1-1" name="message" id="message" cols="30" rows="3" placeholder="Su Mensaje" required="required"></textarea>
                            </div>
                            <div class="form-actions">
                              <button id="contactSend" class="uk-button uk-button-primary" type="submit">Enviar Mensaje</button>
                            </div>
                            <div id="message"></div>
                            <div id="loadingDiv" style="width:100%;float:left;display:none;">
                                <img src="<?php echo base_url();?>assets/img/AjaxLoader.gif"> Enviando....
                            </div>
                            <span class="alert alert-success alert-mail" role="alert" id="contactOk" style="display:none;">Su consulta se envio exitosamente</span>
                            <span class="alert alert-danger alert-mail" role="alert" id="contactError" style="display:none;">Se produjo un error</span>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </article>
            </main>
          </div>
          
          <?php  $this->load->view('elements/contact-sidebar');?>

        </div>
      </div>
    </div>

    <?php $this->load->view('elements/footer');?>

  </div>
  

  <script>
      // Attach a submit handler to the form
      $( "#contactForm" ).submit(function( event ) {       
        
          event.preventDefault();
        
        // Get some values from elements on the page:
        var $form = $( this );            
        var urlForm = $form.attr( "action" );
        var name = $("#name").val();
        var email = $("#email").val();
        var subject = $("#subject").val();
        var message = $("#message").val();
       
        
        $.ajax({
          method: "POST",
          url: urlForm,
          data: { name: name, email: email, subject: subject, message: message },
          beforeSend: function() {
              $('#loadingDiv').show();
            },
        }).done(function() {
          $('#loadingDiv').hide();
          $('#contactOk').css('display', 'block');
          $('#contactSend').prop('disabled', true);
        })
        .fail(function() {
          $('#loadingDiv').hide();
          $('#contactError').css('display', 'block');
          $('#contactSend').prop('disabled', true);
        });         
      });
</script>

  <?php $this->load->view('assets/scripts');?>
</body>
</html>