<!DOCTYPE html>
<html lang="<?php echo $language;?>">
<head>
  <?php $this->load->view('assets/headnfo');?>
  <?php $this->load->view('assets/style');?>
</head>
<body id="tm-container">
  <div class="tm-container">
    <?php $this->load->view('elements/component');?>
    
    <?php $this->load->view('elements/navbar');?>
    
    <?php $this->load->view('elements/spotlight-internal');?>

    <?php $this->load->view('elements/breadcrum-gen');?>

    <div id="tm-main" class="tm-block-main uk-block uk-block-default">
      <div class="uk-container uk-container-center">
        <div class="tm-middle uk-grid" data-uk-grid-match data-uk-grid-margin>
          
          <div class="tm-main uk-width-medium-7-10">
            <main id="tm-content" class="tm-content">
              <article class="uk-article tm-article">
                <div class="tm-article-wrapper">
                  <div class="tm-article-content uk-margin-large-bottom uk-margin-top-remove">
                    <div class="tm-article">

                    <h2 class="uk-module-title">Foro de la Comunidad ISO</h2>
                    <?php if ($this->ion_auth->in_group(3)): ?>
                    <p>El Foro esta disponible solo para Empresas Registradas.</p>
                    <?php else: ?>
                    <table class="uk-table uk-table-hover uk-table-striped">
                      <caption>Al participar del Foro, usted esta aceptando respetar las normas.</caption>
                      <thead>
                          <tr>
                            <th>Categoria</th>
                            <th>Topicos</th>
                            <th>Accion</th>
                            <th>Ult. Actividad</th>
                          </tr>
                      </thead>
                      <tfoot>
                          <tr>
                            <td>Categoria</td>
                            <td>Topicos</td>
                            <td>Accion</td>
                            <td>Ult. Actividad</td>
                          </tr>
                      </tfoot>
                      <tbody>
                        <?php foreach ($forumCat->result() as $frCt): ?>
                          <tr>
                            <td>
                              <?php echo $frCt->title; ?>
                            </td>
                            <td>
                              100
                            </td>
                            <td>
                              <a href="<?php echo base_url() . 'foro/category/' . $frCt->slug; ?>">Ver Topico</a>
                            </td>
                            <td>
                              Hace 6 Horas
                            </td>
                          </tr>
                        <?php endforeach ?>
                      </tbody>
                    </table>
                    <?php endif ?>

                    </div>
                  </div>
                </div>
              </article>
            </main>
          </div>
          
          <?php  $this->load->view('elements/internal-sidebar');?>

        </div>
      </div>
    </div>
   
    <?php $this->load->view('elements/footer');?>

  </div>
  <?php $this->load->view('assets/scripts');?>
</body>
</html>