<!DOCTYPE html>
<html lang="<?php echo $language;?>">
<head>
  <?php $this->load->view('assets/headnfo');?>
  <?php $this->load->view('assets/style');?>
</head>
<body id="tm-container">
  <div class="tm-container">
    <?php $this->load->view('elements/component');?>
    
    <?php $this->load->view('elements/navbar');?>
    
    <?php $this->load->view('elements/spotlight-internal');?>

    <?php $this->load->view('elements/breadcrum-gen');?>

    <div id="tm-main" class="tm-block-main uk-block uk-block-default">
      <div class="uk-container uk-container-center">
        <div class="tm-middle uk-grid" data-uk-grid-match data-uk-grid-margin>
          
          <div class="tm-main uk-width-medium-7-10">
            <main id="tm-content" class="tm-content">
              <article class="uk-article tm-article">
                <div class="tm-article-wrapper">
                  <div class="tm-article-content uk-margin-large-bottom uk-margin-top-remove">
                    <div class="tm-article">

                    <h2 class="uk-module-title">Foro de la Comunidad ISO: <?php echo $catTitle;?></h2>
                    <a class="uk-button-line uk-button" href="#createTopicModal" data-uk-modal target="_self">Crear Topico</a>
                    <table class="uk-table uk-table-hover uk-table-striped">
                      <caption>Al participar del Foro, usted esta aceptando respetar las normas.</caption>
                      <thead>
                          <tr>
                            <th>Topico</th>
                            <th>Fecha</th>
                            <th>Ult. Actividad</th>
                            <th>Autor</th>
                            <th><i class="uk-icon-comment"></i></th>
                            <th>Accion</th>
                          </tr>
                      </thead>
                      <tfoot>
                          <tr>
                            <td>Topico</td>
                            <td>Fecha</td>
                            <td>Ult. Actividad</td>
                            <td>Autor</td>
                            <td><i class="uk-icon-comment"></i></td>
                            <td>Accion</td>
                          </tr>
                      </tfoot>
                      <tbody>
                        <?php foreach ($forumTopic->result() as $frCt): ?>
                          <tr>
                            <td>
                              <?php echo $frCt->title; ?>
                            </td>
                            <td>
                              <?php echo $frCt->date; ?>
                            </td>
                            <td>
                              Hace 6 Horas
                            </td>
                            <td>
                              <?php echo getUserNfo($frCt->user_id)['first_name']; ?>, 
                              <?php echo getUserNfo($frCt->user_id)['last_name']; ?>
                            </td>
                            <td>
                              <?php echo countCommentsByTopicID($frCt->for_topic_id);?>
                            </td>
                            <td>
                              <a href="<?php echo base_url() . 'foro/topic/' . $frCt->slug; ?>">Ver Topico</a>
                            </td>
                          </tr>
                        <?php endforeach ?>
                      </tbody>
                    </table>

                    </div>
                  </div>
                </div>
              </article>
            </main>
          </div>
          
          <?php  $this->load->view('elements/internal-sidebar');?>

        </div>
      </div>
    </div>
   
    <?php $this->load->view('elements/footer');?>

  </div>
  <?php $this->load->view('assets/scripts');?>

  <div id="createTopicModal" class="uk-modal">
  <div class="uk-modal-dialog">
    <a href="" class="uk-modal-close uk-close"></a>
    <form class="uk-form uk-form-stacked" id="createTopicForum" name="createTopicForum" action="<?php echo base_url();?>Forum/saveForumTopic" method="post" accept-charset="utf-8">
      <input type="hidden" name="catID" id="catID" value="<?php echo $catID; ?>">
      <legend>Crear Topico</legend>
      <div class="uk-form-row">
          <label for="title" class="uk-form-label">Titulo</label>
          <input type="text" name="title" id="title" placeholder="Titulo de Topico" class="uk-width-1-1">
      </div>
      <div class="uk-form-row">
          <label for="body" class="uk-form-label">Cuerpo</label>
          <textarea name="body" id="body" style="float:left; width:100%;" placeholder="Cuerpo del Topico"></textarea>
      </div>
      <br>
      <input type="submit" id="Submitbtn" class="uk-button uk-button-primary" value="Crear Topico">
    </form>
  </div>

</div>
</body>
</html>