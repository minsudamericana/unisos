<!DOCTYPE html>
<html lang="<?php echo $language;?>">
<head>
  <?php $this->load->view('assets/headnfo');?>
  <?php $this->load->view('assets/style');?>
</head>
<body id="tm-container">
  <div class="tm-container">
    <?php $this->load->view('elements/component');?>
    
    <?php $this->load->view('elements/navbar');?>
    
    <?php $this->load->view('elements/spotlight-internal');?>

    <?php $this->load->view('elements/breadcrum-gen');?>

    <div id="tm-main" class="tm-block-main uk-block uk-block-default">
      <div class="uk-container uk-container-center">
        <div class="tm-middle uk-grid" data-uk-grid-match data-uk-grid-margin>
          
          <div class="tm-main uk-width-medium-7-10">
            <main id="tm-content" class="tm-content">
              <article class="uk-article tm-article">
                <div class="tm-article-wrapper">
                  <div class="tm-article-content uk-margin-large-bottom uk-margin-top-remove">
                    <div class="tm-article">
                      <div class="tm-article-wrapper">
                        <?php foreach ($topic->result() as $tp): ?>
                          <small>
                            Autor: <?php echo $tp->user_id; ?> | 
                            <?php echo $tp->user_id; ?> | 
                            <i class="uk-icon-comment"></i> 10 | 
                            <?php echo $tp->date; ?>
                          </small>
                          <h1 class="uk-article-title"><?php echo $topTitle;?></h1>
                          <div class="tm-article-content uk-margin-large-bottom uk-margin-top-remove">
                            <div class="tm-article">
                                <?php echo $tp->body; ?>
                            </div>
                            <a class="uk-button uk-button-primary" href="#commentTopicModal" data-uk-modal target="_self">Comentar</a>
                          </div>
                        <?php endforeach ?>
                      </div>
                      <div class="tm-article-wrapper">
                        <div class="uk-width-1-1 uk-grid-margin uk-row-first">
                            <ul class="uk-comment-list">
                                <?php foreach ($comments->result() as $cm): ?>
                                  <?php if ($cm->comment_father == null): ?>
                                    <li>
                                        <article class="uk-comment">
                                            <h4 class="uk-comment-title"><?php echo getUserNfo($cm->user_id)['first_name'] . ', ' .  getUserNfo($cm->user_id)['last_name']; ?> <small><?php echo getIndustrialName(getIndustrialUserByID($cm->user_id)); ?> | <?php echo $cm->date; ?></small></h4>
                                            <div class="uk-comment-body">
                                              <?php echo $cm->body; ?>
                                            </div>
                                            <br>
                                            <form class="uk-form uk-form-stacked" id="createcomment" name="createcomment" action="<?php echo base_url();?>Forum/createComment" method="post" accept-charset="utf-8">
                                              <input type="hidden" name="topID" id="topID" value="<?php echo $topID; ?>">
                                              <input type="hidden" name="commFather" id="commFather" value="<?php echo $cm->for_com_id; ?>">
                                              <div class="uk-form-row">
                                                  <textarea name="body" id="body" style="float:left; width:100%;" placeholder="Escribe tu comentario"></textarea>
                                              </div>
                                              <input type="submit" id="Submitbtn" class="uk-button uk-button-mini uk-button-primary" value="Responder" style="float:left;margin-right:10px;">
                                            </form>
                                            <div style="float:left;">
                                              <form id="createcomment" name="createcomment" action="<?php echo base_url();?>Forum/voteComments" method="post" accept-charset="utf-8" style="float:left;">
                                                <input type="hidden" name="topID" id="topID" value="<?php echo $topID; ?>" readonly>
                                                <input type="hidden" name="adjustVote" value="1" readonly>
                                                <input type="hidden" name="comID" value="<?php echo $cm->for_com_id; ?>" readonly>
                                                <span><?php echo $cm->plus_vote; ?> <i class="uk-icon-thumbs-o-up"></i></span> <input type="submit" class="uk-button uk-button-mini uk-button-primary" value="+1">
                                              </form>
                                              <form id="createcomment" name="createcomment" action="<?php echo base_url();?>Forum/voteComments" method="post" accept-charset="utf-8" style="float:left; margin-left:5px;">
                                                <input type="hidden" name="topID" id="topID" value="<?php echo $topID; ?>" readonly>
                                                <input type="hidden" name="adjustVote" value="2" readonly>
                                                <input type="hidden" name="comID" value="<?php echo $cm->for_com_id; ?>" readonly>
                                                <span><?php echo $cm->neg_vote; ?> <i class="uk-icon-thumbs-o-down"></i></span> <input type="submit" class="uk-button uk-button-mini uk-button-primary" value="-1">
                                              </form>
                                            </div>
                                        </article>
                                        <ul class="uk-comment-list" style="padding-left: 30px;">
                                          <?php
                                            $this->db->order_by('for_topic_id','asc');
                                            $this->db->where('comment_father', $cm->for_com_id);
                                            $subcom = $this->db->get('forum_comment');
                                          ?>
                                          <?php foreach ($subcom->result() as $subcm): ?>
                                          <li>
                                            <article class="uk-comment" style="float: left; width: 100%;">
                                                <h4 class="uk-comment-title"><?php echo getUserNfo($subcm->user_id)['first_name'] . ', ' .  getUserNfo($subcm->user_id)['last_name']; ?> <small><?php echo getIndustrialName(getIndustrialUserByID($subcm->user_id)); ?> | <?php echo $subcm->date; ?></small></h4>
                                                <div class="uk-comment-body">
                                                  <?php echo $subcm->body; ?>
                                                </div>
                                                <form id="createcomment" name="createcomment" action="<?php echo base_url();?>Forum/voteComments" method="post" accept-charset="utf-8" style="float:left;">
                                                  <input type="hidden" name="topID" id="topID" value="<?php echo $topID; ?>" readonly>
                                                  <input type="hidden" name="adjustVote" value="1" readonly>
                                                  <input type="hidden" name="comID" value="<?php echo $subcm->for_com_id; ?>" readonly>
                                                  <span><?php echo $subcm->plus_vote; ?> <i class="uk-icon-thumbs-o-up"></i></span> <input type="submit" class="uk-button uk-button-mini uk-button-primary" value="+1">
                                                </form>
                                                <form id="createcomment" name="createcomment" action="<?php echo base_url();?>Forum/voteComments" method="post" accept-charset="utf-8" style="float:left; margin-left:5px;">
                                                  <input type="hidden" name="topID" id="topID" value="<?php echo $topID; ?>" readonly>
                                                  <input type="hidden" name="adjustVote" value="2" readonly>
                                                  <input type="hidden" name="comID" value="<?php echo $cm->for_com_id; ?>" readonly>
                                                  <span><?php echo $subcm->neg_vote; ?> <i class="uk-icon-thumbs-o-down"></i></span> <input type="submit" class="uk-button uk-button-mini uk-button-primary" value="-1">
                                                </form>
                                            </article>
                                          </li>
                                          <?php endforeach ?>
                                        </ul>
                                    </li>
                                  <?php endif ?>
                                <?php endforeach ?>
                            </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </article>
            </main>
          </div>
          
          <?php  $this->load->view('elements/internal-sidebar');?>

        </div>
      </div>
    </div>
   
    <?php $this->load->view('elements/footer');?>

  </div>
  <?php $this->load->view('assets/scripts');?>

  <div id="commentTopicModal" class="uk-modal">
  <div class="uk-modal-dialog">
    <a href="" class="uk-modal-close uk-close"></a>
    <form class="uk-form uk-form-stacked" id="createcomment" name="createcomment" action="<?php echo base_url();?>Forum/createComment" method="post" accept-charset="utf-8">
      <input type="hidden" name="topID" id="topID" value="<?php echo $topID; ?>">
      <input type="hidden" name="commFather" id="commFather" value="null">
      <legend>Comentar</legend>
      <div class="uk-form-row">
          <label for="body" class="uk-form-label">Comentario</label>
          <textarea name="body" id="body" style="float:left; width:100%;" placeholder="Cuerpo del Topico"></textarea>
      </div>
      <br>
      <input type="submit" id="Submitbtn" class="uk-button uk-button-primary" value="Comentar">
    </form>
  </div>

</div>
</body>
</html>