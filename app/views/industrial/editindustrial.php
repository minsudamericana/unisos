<?php $myIndustrialID = getIndustrialUserByID(getMyID()); ?>
<!DOCTYPE html>
<html lang="<?php echo $language;?>">
<head>
  <?php $this->load->view('assets/headnfo');?>
  <?php $this->load->view('assets/style');?>
</head>
<body id="tm-container">
  <div class="tm-container">
    <?php $this->load->view('elements/component');?>
    <?php $this->load->view('elements/navbar');?>
    <?php $this->load->view('elements/spotlight-internal');?>
    <?php $this->load->view('elements/breadcrum-gen');?>
    <div id="tm-main" class="tm-block-main uk-block uk-block-default">
      <div class="uk-container uk-container-center">
        <div class="tm-middle uk-grid" data-uk-grid-match data-uk-grid-margin>
          <div class="tm-main uk-width-medium-7-10">
            <main id="tm-content" class="tm-content">
              <article class="uk-article tm-article">
                <div class="tm-article-wrapper">
                  <div class="tm-article-content uk-margin-large-bottom uk-margin-top-remove">
                    
                    <ul class="uk-tab uk-tab-grid uk-tab-top" data-uk-tab="{connect:'#tabs_example1', animation: 'fade'}">
                      <li class="uk-active"><a href="#">Informacion de Ficha</a></li>
                      <li><a href="#">Logotipo</a></li>
                      <li><a href="#">Certificaciones</a></li>
                    </ul>
                    <ul id="tabs_example1" class="uk-switcher uk-margin uk-tab-content">
                      <li>
                        <?php foreach ($industrial->result() as $ind): ?>                  
                        <div class="tm-article">
                          <form class="uk-form uk-form-stacked" id="editCommForm" name="editCommForm" action="<?php echo base_url();?>Industrial/saveIndustrialValues" method="post" accept-charset="utf-8" enctype="multipart/form-data">
                                <input type="hidden" name="indID" id="indID" value="<?php echo $indID; ?>">
                                <legend>Datos Basicos</legend>
                                <div class="uk-form-row">
                                    <label for="title" class="uk-form-label">Nombre Comercial</label>
                                    <input type="text" name="title" id="title" placeholder="Nombre Comercial" class="uk-width-1-1" value="<?php echo $ind->title; ?>">
                                </div>
                                <div class="uk-form-row">
                                    <label for="slug" class="uk-form-label">Slug (URL)</label>
                                    <input type="text"  name="slug" id="slug" placeholder="Slug (URL)" class="uk-width-1-1" value="<?php echo $ind->slug; ?>"  readonly>
                                    <small>* Se Genera Automaticamente al Guardar</small>
                                </div>
                                <legend>Clasificacion de Industria</legend>
                                <div class="uk-form-row">
                                    <select name="indcatID" id="indcatID" class="uk-width-4-10">
                                      <option value="">Categorias</option>
                                      <?php foreach ($indcat->result() as $indcat): ?>
                                        <?php if ($indcat->indcat_id == getIndSubcatNfo($ind->indsubcat_id)['indcat_id']): ?>
                                          <option value="<?php echo $indcat->indcat_id; ?>" selected><?php echo $indcat->name; ?></option>
                                        <?php endif ?>
                                        <option value="<?php echo $indcat->indcat_id; ?>"><?php echo $indcat->name; ?></option>  
                                      <?php endforeach ?>
                                    </select>
                                    <select name="indsubcatID" id="indsubcatID" class="uk-width-4-10">
                                      <?php if ($ind->indsubcat_id != null): ?>
                                        <option value="<?php echo $ind->indsubcat_id;?>" selected><?php echo getIndSubcatNfo($ind->indsubcat_id)['name'];?></option>
                                      <?php endif ?>
                                      <option value="">Sub Categorias</option>
                                    </select>
                                </div>
                                <hr>
                                <legend>Direccion</legend>
                                <div class="uk-form-row">
                                    <label for="" class="uk-form-label">Direccion (Nombre de Calle)</label>
                                    <input type="text"  name="adress" id="adress" placeholder="Direccion" class="uk-width-10-10" value="<?php echo $ind->adress; ?>">
                                </div>
                                <div class="uk-form-row">
                                    <label for="" class="uk-form-label">Altura (Numero de Calle)</label>
                                    <input type="text"  name="number" id="number" placeholder="Altura" class="uk-width-10-10" value="<?php echo $ind->number; ?>">
                                </div>
                                <div class="uk-form-row">
                                    <select name="paisID" id="paisID" class="uk-width-3-10">
                                      <option value="">Pais</option>
                                      <?php if ($ind->pais_id != null): ?>
                                        <option value="<?php echo $ind->pais_id; ?>" selected><?php echo getCountryByID($ind->pais_id)['nombre'];?></option>
                                      <?php endif ?>
                                      <?php foreach ($pais->result() as $sg): ?>
                                        <?php if ($ind->pais_id != null): ?>
                                          <?php if ($sg->id != $ind->pais_id): ?>
                                            <option value="<?php echo $sg->id; ?>"><?php echo $sg->nombre; ?></option>
                                          <?php endif ?>
                                        <?php else: ?>
                                          <option value="<?php echo $sg->id; ?>"><?php echo $sg->nombre; ?></option>  
                                        <?php endif ?>
                                      <?php endforeach ?>
                                    </select>
                                    <select name="provinciaID" id="provinciaID" class="uk-width-3-10">
                                      <?php if ($ind->provincia_id != null): ?>
                                        <option value="<?php echo $ind->provincia_id;?>" selected><?php echo getProvinceByID($ind->provincia_id)['nombre'];?></option>
                                      <?php endif ?>
                                      <option value="">Provincia</option>
                                    </select>
                                    <select name="partidoID" id="partidoID" class="uk-width-3-10">
                                      <?php if ($ind->partido_id != null): ?>
                                        <option value="<?php echo $ind->partido_id;?>" selected><?php echo getPartidoByID($ind->partido_id)['nombre'];?></option>
                                      <?php endif ?>
                                      <option value="">Localidad</option>
                                    </select>
                                    <select name="localidadID" id="localidadID" class="uk-width-3-10">
                                      <?php if ($ind->localidad_id != null): ?>
                                        <option value="<?php echo $ind->localidad_id;?>" selected><?php echo getLocalidadByID($ind->localidad_id)['nombre'];?></option>
                                      <?php endif ?>
                                      <option value="">Partido</option>
                                    </select>
                                    <select name="barrioID" id="barrioID" class="uk-width-3-10">
                                      <?php if ($ind->barrio_id != null): ?>
                                        <option value="<?php echo $ind->barrio_id;?>" selected><?php echo getBarrioByID($ind->barrio_id)['nombre'];?></option>
                                      <?php endif ?>
                                      <option value="">Barrio</option>
                                    </select>
                                    <select name="subbarrioID" id="subbarrioID" class="uk-width-3-10">
                                      <?php if ($ind->subbarrio_id != null OR $ind->subbarrio_id != 0): ?>
                                        <option value="<?php echo $ind->subbarrio_id;?>" selected><?php echo getSubBarrioByID($ind->subbarrio_id)['nombre'];?></option>
                                      <?php endif ?>
                                      <option value="">Sub Barrio</option>
                                    </select>
                                </div>
                                <hr>
                                <legend>Datos de Contacto</legend>
                                <div class="uk-form-row">
                                    <label for="" class="uk-form-label">Numero de Telefono</label>
                                    <input type="text"  name="phone" id="phone" placeholder="Telefono" class="uk-width-1-1" value="<?php echo $ind->phone; ?>">
                                </div>
                                <div class="uk-form-row">
                                    <label for="" class="uk-form-label">Numero de Fax</label>
                                    <input type="text"  name="fax" id="fax" placeholder="Fax" class="uk-width-1-1" value="<?php echo $ind->fax; ?>">
                                </div>
                                <div class="uk-form-row">
                                    <label for="" class="uk-form-label">Mail de Contacto</label>
                                    <input type="text"  name="email" id="email" placeholder="E-Mail" class="uk-width-1-1" value="<?php echo $ind->email; ?>">
                                </div>
                                <div class="uk-form-row">
                                    <label for="" class="uk-form-label">URL de Facebook</label>
                                    <input type="text"  name="facebook" id="facebook" placeholder="Facebook" class="uk-width-1-1" value="<?php echo $ind->facebook; ?>">
                                </div>
                                <div class="uk-form-row">
                                    <label for="" class="uk-form-label">URL de Twitter</label>
                                    <input type="text"  name="twitter" id="twitter" placeholder="Twitter" class="uk-width-1-1" value="<?php echo $ind->twitter; ?>">
                                </div>
                                <div class="uk-form-row">
                                    <label for="" class="uk-form-label">URL de Canal de Youtube</label>
                                    <input type="text"  name="youtube" id="youtube" placeholder="Youtube" class="uk-width-1-1" value="<?php echo $ind->youtube; ?>">
                                </div>
                                <div class="uk-form-row">
                                    <label for="" class="uk-form-label">URL de LinkedIN</label>
                                    <input type="text"  name="linkedin" id="linkedin" placeholder="LinkedIN" class="uk-width-1-1" value="<?php echo $ind->linkedin; ?>">
                                </div>
                                <hr>
                                <legend>Informacion</legend>
                                <div class="uk-form-row">
                                    <label for="" class="uk-form-label">Descripcion Corta de la Industria</label>
                                    <input type="text"  name="shortdesc" id="shortdesc" placeholder="Descripcion Corta" class="uk-width-1-1" value="<?php echo $ind->shortdesc; ?>">
                                </div>
                                <div class="uk-form-row">
                                    <label for="" class="uk-form-label">Descripcion de la Industria</label>
                                    <input type="text"  name="desc" id="desc" placeholder="Descripcion" class="uk-width-1-1" value="<?php echo $ind->desc; ?>">
                                </div>
                                <div class="uk-form-row">
                                    <label for="" class="uk-form-label">Cuerpo de la Ficha</label>
                                    <textarea class="bodyFichaInd" name="body" id="body" cols="30" rows="10"><?php echo $ind->body; ?></textarea>
                                </div>
                                <hr>
                                <legend>Informacion Extra</legend>
                                <div class="uk-form-row">
                                    <label for="user_id" class="uk-form-label">Usuario Apoderado</label>
                                    <input type="text"  name="user_id" id="user_id" placeholder="Usuario Apoderado" class="uk-width-1-1" value="<?php echo getUserNfo($ind->user_id)['username']; ?>" readonly>
                                    <small>* Las industrias son intransferibles. Para mas informacion, contactarse con Soporte Tecnico.</small>
                                </div>
                                <div class="uk-form-row">
                                    <label for="lat" class="uk-form-label">Latitud (Geocoordenada)</label>
                                    <input type="text"  name="lat" id="lat" placeholder="Latitud" class="uk-width-1-1" value="<?php echo $ind->lat; ?>" readonly>
                                    <small>* Se Genera Automaticamente al Guardar</small>
                                </div>
                                <div class="uk-form-row">
                                    <label for="lng" class="uk-form-label">Longitud (Geocoordenada)</label>
                                    <input type="text"  name="lng" id="lng" placeholder="Longitud" class="uk-width-1-1" value="<?php echo $ind->lng; ?>" readonly>
                                    <small>* Se Genera Automaticamente al Guardar</small>
                                </div>
                                <hr>
                                <legend>Activacion</legend>
                                <div class="uk-form-row">
                                    <label for="" class="uk-form-label">Disponibilidad de Ficha</label>
                                    <?php if ($ind->status == 1): ?>
                                      <input type="radio" name="status" id="status" value="1" checked> Activado <input type="radio" name="status" id="status" value="2"> Desactivado
                                    <?php else: ?>
                                      <input type="radio" name="status" id="status" value="1"> Activado <input type="radio" name="status" id="status" value="2" checked> Desactivado
                                    <?php endif ?>
                                </div>
                                <input type="submit" id="Submitbtn" class="uk-button uk-button-primary" value="Actualizar Cambios">
                          </form>
                        </div>
                        <?php endforeach ?>
                      </li>
                      <li>
                        <form class="uk-form uk-form-stacked" id="editCommLogo" name="editCommLogo" action="<?php echo base_url();?>Industrial/saveIndustrialLogo" method="post" accept-charset="utf-8" enctype="multipart/form-data">
                          <input type="hidden" name="indID" id="indID" value="<?php echo $indID; ?>">
                          <legend>Logotipo</legend>
                          <div class="uk-form-row">
                              <div class="uk-width-6-10" style="float:left;">
                                <label for="logo" class="uk-form-label">Logotipo</label>
                                <input type="file" name="logo" id="logo">
                              </div>
                              <div class="uk-width-3-10" style="float:left;">
                                <img src="<?php echo base_url() . 'assets/uploads/files/industrial/' . $ind->logo; ?>">
                                <div class="uk-link" id="deleteProfilePic">Borrar Imagen</div>
                              </div>
                          </div>
                          <input type="submit" id="Submitbtn" class="uk-button uk-button-primary" value="Actualizar Cambios">
                        </form>
                      </li>
                      <li>
                        <form class="uk-form uk-form-stacked" id="editCommLogo" name="editCommLogo" action="<?php echo base_url();?>Industrial/saveIndustrialCerts" method="post" accept-charset="utf-8" enctype="multipart/form-data">
                          <input type="hidden" name="indID" id="indID" value="<?php echo $indID; ?>">
                          <legend>Certificaciones</legend>
                          <div class="uk-form-row">
                              <div class="uk-width-10-10">
                                <label for="certs" class="uk-form-label">Certificaciones</label>
                                <div class="uk-form-row">
                                  
                                  <?php foreach ($certificates->result() as $crt): ?>
                                  <div class="uk-width-3-10" style="float:left;">
                                    <?php if (checkIfHasCert($myIndustrialID, $crt->ind_cert_type_id) == true): ?>
                                      <input type="checkbox" name="certs[]" id="certs[]" value="<?php echo $crt->ind_cert_type_id; ?>" checked> <?php echo $crt->name; ?><br>
                                    <?php else: ?>
                                      <input type="checkbox" name="certs[]" id="certs[]" value="<?php echo $crt->ind_cert_type_id; ?>"> <?php echo $crt->name; ?><br>
                                    <?php endif ?>
                                  </div>
                                  <?php endforeach ?>

                                </div>
                              </div>
                          </div>
                          <input type="submit" id="Submitbtn" class="uk-button uk-button-primary" value="Actualizar Cambios">
                        </form>
                      </li>
                    </ul>
                    
                    
                  </div>
                </div>
              </article>
            </main>
          </div>
          <div class="tm-main uk-width-medium-3-10">
            <main id="tm-content" class="tm-content">
              <article class="uk-article tm-article">
                <div class="tm-article-wrapper">
                  <h2 class="uk-module-title">Ficha Completada</h2>
                  <div class="uk-progress uk-progress-primary">
                    <?php $porcentajeCompleto = countNullRowsInInd(getMyID()); ?>
                    <div class="uk-progress-bar" style="width: <?php echo $porcentajeCompleto; ?>%"><?php echo $porcentajeCompleto; ?>%</div>
                  </div>
                </div>
              </article>
            </main>
          </div>
        </div>
      </div>
    </div>
    <?php $this->load->view('elements/footer');?>
  </div>
  <?php $this->load->view('assets/scripts');?>
  <script src="<?php echo base_url();?>assets/js/tinymce/tinymce.min.js" type="text/javascript"></script>
  <script>
    tinymce.init({
    language: 'es',
    selector: '.bodyFichaInd',
    height: 500,
    plugins: [
      "advlist autolink autosave link image lists charmap print preview hr anchor pagebreak spellchecker",
      "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
      "table contextmenu directionality emoticons template textcolor paste fullpage textcolor colorpicker textpattern"
    ],
    toolbar1: "bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | styleselect formatselect fontselect fontsizeselect",
    toolbar2: "cut copy paste | searchreplace | bullist numlist | outdent indent blockquote | undo redo | link unlink anchor image media code | insertdatetime preview | forecolor backcolor",
    toolbar3: "table | hr removeformat | ltr rtl | spellchecker",
    menubar: false,
    toolbar_items_size: 'small',
  });
  </script>
  <script>
  $('#deleteProfilePic').click(function(){
      $.ajax({
          url: "<?php echo base_url();?>Industrial/removeIndustrialLogo"
      })
      .done(function(data) {
        console.log(data);
      });
  });
  // Provincia, Localidad, Partido Combo
  $("#paisID").change(function() {
      paisID = $('#paisID').val();
      $("#paisID option:selected").each(function() {
          $.post("/location/getProvincia", {
              paisID : paisID
          }, function(data) {          
              $("#provinciaID").html(data);
          });
      });
  });
  $("#provinciaID").change(function() {
      provinciaID = $('#provinciaID').val();
      $("#provinciaID option:selected").each(function() {
          $.post("/location/getPartido", {
              provinciaID : provinciaID
          }, function(data) {          
              $("#partidoID").html(data);
          });
      });
  });
  $("#partidoID").change(function() {
      partidoID = $('#partidoID').val();
      $("#partidoID option:selected").each(function() {
          $.post("/location/getLocalidad", {
              partidoID : partidoID
          }, function(data) {          
              $("#localidadID").html(data);
          });
      });
  });
  $("#localidadID").change(function() {
      localidadID = $('#localidadID').val();
      $("#localidadID option:selected").each(function() {
          $.post("/location/getBarrio", {
              localidadID : localidadID
          }, function(data) {          
              $("#barrioID").html(data);
          });
      });
  });
  $("#barrioID").change(function() {
      barrioID = $('#barrioID').val();
      $("#barrioID option:selected").each(function() {
          $.post("/location/getSubbarrio", {
              barrioID : barrioID
          }, function(data) {          
              $("#subbarrioID").html(data);
          });
      });
  });
  // Provincia, Localidad, Partido Combo End
  // Industrial Subcategory
  $("#indcatID").change(function() {
      indcatID = $('#indcatID').val();
      $("#indcatID option:selected").each(function() {
          $.post("/industrial/getIndSubcategoryForDd", {
              indcatID : indcatID
          }, function(data) {          
              $("#indsubcatID").html(data);
          });
      });
  });
  // Industrial Subcategory End
  </script>
</body>
</html>

