<!DOCTYPE html>
<html lang="<?php echo $language;?>">
<head>
  <?php $this->load->view('assets/headnfo');?>
  <?php $this->load->view('assets/style');?>
</head>
<body id="tm-container">
  <div class="tm-container">
    <?php $this->load->view('elements/component');?>
    
    <?php $this->load->view('elements/navbar');?>
    
    <?php $this->load->view('elements/spotlight-internal');?>

    <div class="tm-breadcrumbs">
      <div class="uk-container uk-container-center">
        <div class="uk-hidden-small">
          <ul class="uk-breadcrumb">
            <li><a href="index.html">Empresas</a></li>
            <li><a href="index.html">Metalurgicas</a></li>
            <li class="uk-active"><span>Fundicion</span></li>
          </ul>
        </div>
      </div>
    </div>

    <div id="tm-main" class="tm-block-main uk-block uk-block-default">
      <div class="uk-container uk-container-center">
        <div class="tm-middle uk-grid" data-uk-grid-match data-uk-grid-margin>
          
          <?php  $this->load->view('elements/internal-industrial');?>
                   
          <?php foreach ($industrial->result() as $ind): ?>
            <div class="tm-main uk-width-medium-7-10">
              <main id="tm-content" class="tm-content">
                <div class="tm-middle uk-grid" data-uk-grid-match data-uk-grid-margin>
                  <div class="tm-main uk-width-medium-10-10" style="margin-top: 0px;">
                    <i class="uk-icon-area-chart"></i> Visitas: <?php echo $ind->visit; ?>
                  </div>
                  <div class="tm-main uk-width-medium-10-10" style="margin-top: 0px;">
                    <h2 class="uk-module-title" style="margin-bottom: 0;">
                      <?php echo $ind->title; ?><br>
                      <small><?php echo getIndSubcatNfo($ind->indsubcat_id)['name']; ?></small>
                    </h2>
                    <p style="margin-top: 0;">  
                      <?php echo $ind->adress; ?>
                      <?php echo $ind->number; ?>,
                      <?php echo getCountryByID($ind->pais_id)['nombre']; ?>,
                      <?php echo getProvinceByID($ind->provincia_id)['nombre']; ?>,
                      <?php echo getLocalidadByID($ind->localidad_id)['nombre']; ?>,
                      <?php echo getPartidoByID($ind->partido_id)['nombre']; ?>,
                      <?php echo getBarrioByID($ind->barrio_id)['nombre']; ?>,
                      <?php echo getSubBarrioByID($ind->subbarrio_id)['nombre']; ?>.
                    </p>
                    <p style="margin-top: 0;">
                      <?php echo $ind->desc; ?>
                    </p>
                  </div>
                  <div class="tm-main uk-width-medium-5-10" style="margin-top: 0;">
                    <img src="<?php echo base_url();?>assets/uploads/files/industrial/<?php echo $ind->logo; ?>" alt="">
                  </div>
                  <div class="tm-main uk-width-medium-5-10" style="margin-top: 0;">
                    <?php getCertListByIndID($ind->ind_id) ?>
                    <?php echo $ind->phone; ?><br>
                    <?php echo $ind->fax; ?><br>
                    <?php echo $ind->email; ?><br>
                  </div>
                  <div class="tm-main uk-width-medium-10-10" style="margin-top: 5px;">
                    <a class="uk-button-color uk-button-mini uk-button" href="#modal-c" data-uk-modal target="_self">Contactar</a>
                    <a href="<?php echo $ind->facebook; ?>" class="uk-icon-button uk-icon-facebook" target="_blank" style="margin-top: 0;"></a>
                    <a href="<?php echo $ind->twitter; ?>" class="uk-icon-button uk-icon-twitter" target="_blank" style="margin-top: 0;"></a>
                    <a href="<?php echo $ind->youtube; ?>" class="uk-icon-button uk-icon-youtube" target="_blank" style="margin-top: 0;"></a>
                    <a href="<?php echo $ind->linkedin; ?>" class="uk-icon-button uk-icon-linkedin" target="_blank" style="margin-top: 0;"></a>
                    <a href="#" class="uk-icon-button uk-icon-globe" target="_blank" style="margin-top: 0;"></a>
                  </div>
                  <div class="tm-main uk-width-medium-10-10" style="margin-top: 8px;">
                    <main id="tm-content" class="tm-content">
                        <?php echo $ind->body; ?><br>
                    </main>
                  </div>
                  <div class="tm-main uk-width-medium-10-10">
                    <?php if ($ind->lat == 0 OR $ind->lng == 0): ?>
                      <?php 
                        $completeAdress = $ind->adress . '' . $ind->number  . ', ' . 
                        getSubBarrioByID($ind->subbarrio_id)['nombre']  . ', ' . 
                        getBarrioByID($ind->barrio_id)['nombre']  . ', ' . 
                        getPartidoByID($ind->partido_id)['nombre']  . ', ' . 
                        getLocalidadByID($ind->localidad_id)['nombre']  . ', ' . 
                        getProvinceByID($ind->provincia_id)['nombre']  . ', ' . 
                        getCountryByID($ind->pais_id)['nombre']  . '.';
                        $lat = geocode($completeAdress)[0];
                        $lng = geocode($completeAdress)[1];
                        // Save Lat & Lgn States
                        saveGeocode($lat, $lng, $ind->ind_id);
                      ?>
                      <script>
                        location.reload();
                      </script>
                    <?php else: ?>
                    <?php endif ?>
                    <div id="map"></div>
                  </div>
                </div>
              </main>
            </div>
          <?php endforeach ?>
        </div>
      </div>
    </div>


   
    <?php $this->load->view('elements/footer');?>

  </div>
  <?php $this->load->view('assets/scripts');?>
  <script src="https://maps.googleapis.com/maps/api/js?libraries=geometry&callback=initMap"></script>
  
  <script>
  var latDest = <?php echo $ind->lat; ?>;
  var lonDest = <?php echo $ind->lng; ?>; 
  var map;
  function initMap() {
    map = new google.maps.Map(document.getElementById('map'), {
      center: {lat: latDest, lng: lonDest},
      zoom: 8
    });
  }
  </script>

  <div id="modal-c" class="uk-modal" aria-hidden="false">
    <div class="uk-modal-dialog uk-panel-box">
      <a class="uk-modal-close uk-close"></a>
      <h3 class="uk-panel-title">Enviar Comentario</h3>
      <form role="form" id="contact_form" class="uk-form uk-form-stacked">
        <div class="uk-grid uk-grid-width-medium-1-1 uk-grid-width-1-1">
          <div class="uk-form-icon">
            <i class="uk-icon-user"></i>
            <input type="text" class="uk-width-10-10" name="contact-name" placeholder=" Nombre" size="30" required="required">
          </div>
          <div class="uk-form-icon uk-grid-margin">
            <i class="uk-icon-envelope"></i>
            <input type="email" class="uk-width-10-10" name="contact-email" placeholder=" E-Mail" size="30" required="required">
          </div>
          <div class="uk-form-icon uk-grid-margin">
            <i class="uk-icon-bookmark"></i>
            <input type="text" class="uk-width-10-10 optional" name="contact-subject" placeholder=" Asunto" size="60">
          </div>
          <div class="uk-grid-margin">
              <textarea class="uk-width-10-10" name="contact-message" cols="30" rows="3" placeholder="Mensaje" required="required"></textarea>
          </div>
          <div class="uk-grid-margin">
            <button id="Submitbtn" class="uk-width-10-10 uk-button uk-button-primary" type="submit">Enviar Mensaje</button>
          </div>
        </div>
      </form>
    </div>
  </div>

</body>
</html>