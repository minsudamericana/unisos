<!DOCTYPE html>
<html lang="<?php echo $language;?>">
<head>
  <?php $this->load->view('assets/headnfo');?>
  <?php $this->load->view('assets/style');?>
</head>
<body id="tm-container">
  <div class="tm-container">
    <?php $this->load->view('elements/component');?>
    
    <?php $this->load->view('elements/navbar');?>
    
    <?php $this->load->view('elements/spotlight-internal');?>

    <div class="tm-breadcrumbs">
      <div class="uk-container uk-container-center">
        <div class="uk-hidden-small">
          <ul class="uk-breadcrumb">
            <li><a href="index.html">Empresas</a></li>
            <li><a href="index.html">Metalurgicas</a></li>
            <li class="uk-active"><span>Fundicion</span></li>
          </ul>
        </div>
      </div>
    </div>

    <div id="tm-main" class="tm-block-main uk-block uk-block-default">
      <div class="uk-container uk-container-center">
        <div class="tm-middle uk-grid" data-uk-grid-match data-uk-grid-margin>
          
          <?php  $this->load->view('elements/internal-industrial');?>
          
          <div class="tm-main uk-width-medium-7-10">
            <main id="tm-content" class="tm-content">
              <article class="uk-article tm-article">
                <div class="tm-article-wrapper" style="float: left;">
                  <div class="tm-article-content uk-margin-large-bottom uk-margin-top-remove">
                    <div class="tm-article">
                        <?php foreach ($industrialList->result() as $ind): ?>
                          <?php if ($ind->status == 1): ?>
                            <div class="uk-width-medium-3-10" style="float:left; margin-right:5px;">
                              <a href="<?php echo base_url() . 'industrial/company/' . $ind->slug; ?>">
                                <div class="uk-panel uk-panel-box tm-panel-card" style="min-height: 250px;">
                                  <div class="uk-panel-teaser">
                                    <img class="tm-card-avatar" src="<?php echo base_url() . 'assets/uploads/files/industrial/' . $ind->logo; ?>" alt="Diane Moreno - CEO">
                                  </div>
                                  <div class="tm-card-content">
                                    <h3 class="uk-panel-title"><?php echo $ind->title;?></h3>
                                    <h4 class="tm-card-title"><?php getCertListByIndID($ind->ind_id) ?></h4>
                                  </div>
                                </div>
                              </a>
                            </div>
                          <?php endif ?>
                        <?php endforeach ?>
                    </div>
                  </div>
                </div>
              </article>
            </main>
          </div>

        </div>
      </div>
    </div>
   
    <?php $this->load->view('elements/footer');?>

  </div>
  <?php $this->load->view('assets/scripts');?>
</body>
</html>