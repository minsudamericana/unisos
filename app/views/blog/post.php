<!DOCTYPE html>
<html lang="<?php echo $language;?>">
<head>
  <?php $this->load->view('assets/headnfo');?>
  <?php $this->load->view('assets/style');?>
</head>
<body id="tm-container">
  <div class="tm-container">
    <?php $this->load->view('elements/component');?>
    
    <?php $this->load->view('elements/navbar');?>
    
    <?php $this->load->view('elements/spotlight-internal');?>

    <?php $this->load->view('elements/breadcrum-gen');?>  
    
    <div id="tm-main" class="tm-block-main uk-block uk-block-default">
      <div class="uk-container uk-container-center">
        <div class="tm-middle uk-grid" data-uk-grid-match data-uk-grid-margin>

          <div class="tm-main uk-width-medium-7-10">
            <main id="tm-content" class="tm-content">
              <div class="uk-grid tm-leading-article">
                <div class="uk-width-1-1">
                    <?php foreach ($post->result() as $pt): ?> 
                      <article class="uk-article tm-article">
                        <div class="tm-article-wrapper">
                          <div class="tm-article-featured-image">
                            <?php $postIMG = base_url() . 'assets/uploads/files/post/' . $pt->imagen_assoc_id; ?>
                            <img class="uk-border-rounded" src="<?php echo $postIMG?>" alt="<?php echo $pt->title; ?>" width="851" height="355">
                          </div>
                          <h1 class="uk-article-title">
                            <?php echo $pt->title; ?>
                          </h1>
                          <h4><?php echo $pt->description; ?></h4>
                          <p class="uk-article-meta"> Escrito por <?php echo getUserNfo($pt->post_id)['username'];?> en 
                            <time datetime="<?php echo $pt->date;?>"><?php echo str_replace('-', '/', $pt->date); ?></time>. 
                            Publicado en . 
                          </p>
                          <div class="tm-article-content uk-margin-large-bottom uk-margin-top-remove">
                            
                            <div class="tm-article">
                              <?php echo $pt->body; ?>
                            </div>

                          </div>
                        </div>
                        <?php
                          // Tags Conversor
                          $tagsArray = explode(';', $pt->tags);
                        ?>
                        <p class="tm-tags-list">
                          <?php foreach ($tagsArray as $key => $value): ?>
                            <a><?php echo $value; ?></a>
                          <?php endforeach ?>
                        </p>
                      </article>
                    <?php endforeach ?>
                </div>
                <div id="fb-root"></div>
                <script>(function(d, s, id) {
                  var js, fjs = d.getElementsByTagName(s)[0];
                  if (d.getElementById(id)) return;
                  js = d.createElement(s); js.id = id;
                  js.src = "//connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v2.6&appId=885482321490748";
                  fjs.parentNode.insertBefore(js, fjs);
                }(document, 'script', 'facebook-jssdk'));</script>

                <?php
                  $facebookAppID = 1009869059067304;
                  $urlTarget = current_url();
                  $numberPost = 5;
                ?>
                
                <div class="uk-width-1-1" style="margin-top: 10px;">
                  <div class="social-buttons">
                    <?php makeFbBtn($urlTarget, 1); ?>
                    <?php makeTwBtn($urlTarget, 1); ?>
                    <?php makeLkBtn($urlTarget, 1); ?>
                    <?php makePntBtn($postIMG, $urlTarget, 1) ?>
                  </div>

                    <?php if ($this->ion_auth->logged_in()): ?>
                    <article class="uk-article tm-article">
                      <div class="tm-article-wrapper">
                        <h3 class="title">Comentarios</h3>
                          <div class="media-area">
                            <div class="fb-comments" data-href="<?php echo $urlTarget;?>" data-numposts="<?php echo $numberPost;?>"></div>
                          </div>
                      </div>
                    </article>
                    <?php else: ?> 
                    <article class="uk-article tm-article">
                      <div class="tm-article-wrapper">
                        <p>Para comentar por favor inicie <a href="<?php echo base_url();?>/User/login">sesion</a> o <a href="<?php echo base_url();?>/User/register">registrese</a>.</p>
                      </div>
                    </article>
                  <?php endif ?>
            
                </div>

              </div>
            </main>
          </div>

          <?php  $this->load->view('elements/blog-sidebar');?>

        </div>
      </div>
    </div>

    <?php $this->load->view('elements/footer');?>

  </div>
  <?php $this->load->view('assets/scripts');?>
  <style>
    .uk-button i:before {
      font-size: 30px;
      margin-left: 0;
    }
    div.social-buttons > a {
        padding: 5px 15px 0px 15px;
    }
  </style>
</body>
</html>