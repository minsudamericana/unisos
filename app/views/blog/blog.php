<!DOCTYPE html>
<html lang="<?php echo $language;?>">
<head>
  <?php $this->load->view('assets/headnfo');?>
  <?php $this->load->view('assets/style');?>
</head>
<body id="tm-container">
  <div class="tm-container">
    <?php $this->load->view('elements/component');?>
    
    <?php $this->load->view('elements/navbar');?>
    
    <?php $this->load->view('elements/spotlight-internal');?>

    <?php $this->load->view('elements/breadcrum-gen');?>  
    
    <div id="tm-main" class="tm-block-main uk-block uk-block-default">
      <div class="uk-container uk-container-center">
        <div class="tm-middle uk-grid" data-uk-grid-match data-uk-grid-margin>
          <div class="tm-main uk-width-medium-7-10">
            <main id="tm-content" class="tm-content">
              <div class="uk-grid tm-leading-article">
                <div class="uk-width-1-1">
                    <?php foreach ($posts->result() as $pst): ?>
                      <article class="uk-article tm-article" data-permalink="<?php echo base_url() . 'blog/post/' . $pst->slug; ?>">
                        <div class="tm-article-wrapper">
                          <div class="tm-article-featured-image">
                            <a class="uk-overlay uk-overlay-hover" href="<?php echo base_url() . 'blog/post/' . $pst->slug; ?>" title="article">
                              <img class="uk-border-rounded" src="<?php echo base_url() . 'assets/uploads/files/post/' . $pst->imagen_assoc_id; ?>" alt="blog article">
                              <div class="uk-overlay-panel uk-overlay-background uk-overlay-icon uk-overlay-fade"></div>
                              <div class="tm-overlay-box"></div>
                            </a>
                          </div>
                          <h1 class="uk-article-title">
                            <a href="<?php echo base_url() . 'blog/post/' . $pst->slug; ?>" title="<?php echo $pst->title; ?>"><?php echo $pst->title; ?></a>
                          </h1>
                          <p class="uk-article-meta"> Escrito por <?php echo getUserNfo($pst->id)['username'];?> en
                            <time datetime="2016-01-15"><?php echo str_replace('-', '/', $pst->date); ?></time>. 
                            Publicado en <?php echo getCatPostNfo($pst->categorie_id)['slug']; ?> . </p>
                          <div class="tm-article-content uk-margin-large-bottom uk-margin-top-remove">
                            <div class="tm-article tm-dropcap-block">
                              <p><?php echo $pst->description; ?></p>
                            </div>
                            <p><a class="uk-button uk-button-primary uk-margin-top uk-float-right" href="<?php echo base_url() . 'blog/post/' . $pst->slug; ?>" title="<?php echo $pst->title; ?>">Leer mas...</a>
                            </p>
                          </div>
                        </div>
                      </article>
                    <?php endforeach ?>
                </div>
              </div>
            </main>
          </div>

          <?php  $this->load->view('elements/blog-sidebar');?>

        </div>
      </div>
    </div>


    <?php $this->load->view('elements/footer');?>

  </div>
  <?php $this->load->view('assets/scripts');?>
</body>
</html>