<!DOCTYPE html>
<html lang="<?php echo $language;?>">
<head>
  <?php $this->load->view('assets/headnfo');?>
  <?php $this->load->view('assets/style');?>
</head>
<body id="tm-container">
  <div class="tm-container">
    <?php $this->load->view('elements/component');?>
    
    <?php $this->load->view('elements/navbar');?>
    
    <?php $this->load->view('elements/spotlight-internal');?>

    <?php $this->load->view('elements/breadcrum-gen');?>

    <div id="tm-main" class="tm-block-main uk-block uk-block-default">
      <div class="uk-container uk-container-center">
        <div class="tm-middle uk-grid" data-uk-grid-match data-uk-grid-margin>
          
          <div class="tm-main uk-width-medium-7-10">
            <main id="tm-content" class="tm-content">
              <article class="uk-article tm-article">
                <div class="tm-article-wrapper">
                  
                  <div class="tm-article-content uk-margin-large-bottom uk-margin-top-remove">
                    <div class="tm-article">
                      <div class="uk-width-1-1">
                        <h2 class="uk-module-title">E-Learning: La plataforma del Futuro para el Estudiante</h2>
                        <p>
                          En nuestra plataforma, los recursos humanos de su empresa, podran perfeccionarse y certificarse en una gran variedad de especializaciones claves para desempeñar sus cargos. <br>
                          Desde habilidades Empresariales e Interpersonales, hasta capacitaciones en Sistemas de Gestion, como de Normas de Calidad. <br>
                          Los cursos estan confeccionados cuidadosamente por equipos multidisciplinarios de Ingenieros, Licenciados y Profesores Universitarios.
                        </p>
                        <img class="uk-thumbnail-expand" src="<?php echo base_url();?>assets/images/stock/elearning.jpg" alt="">
                      </div>
                    </div>
                  </div>

                  <?php if ($this->ion_auth->logged_in()): ?>

                    <?php if ($this->ion_auth->in_group(3)): ?>
                      <div class="tm-article-content uk-margin-large-bottom uk-margin-top-remove">
                        <div class="tm-article">
                          <p>La plataforma de E-Learning es solo para las Empresas Registradas.</p>  
                        </div>
                      </div>
                    <?php else: ?>
                      <div class="tm-article-content uk-margin-large-bottom uk-margin-top-remove">
                        <div class="tm-article">
                          <a class="uk-button-success uk-button-large uk-button" href="<?php echo base_url();?>elearning/list">Ver Cursos</a>
                          <a class="uk-button-success uk-button-large uk-button" href="<?php echo base_url();?>elearning/myCourses">Mis Cursos</a>
                          <a class="uk-button-success uk-button-large uk-button" href="<?php echo base_url();?>elearning/myCalifications">Mis Calificaciones</a>
                        </div>
                      </div>
                    <?php endif ?>
                  <?php else: ?>
                  <div class="tm-article-content uk-margin-large-bottom uk-margin-top-remove">
                    <div class="tm-article">
                      <div class="uk-alert-success uk-danger">&nbsp;&nbsp;Para Acceder a los Cursos en Su totalidad, se requiere una subscripcion</div>
                      <br>
                      <a class="uk-button-success uk-button-large uk-button" href="<?php echo base_url();?>elearning/list">Ver Cursos</a>
                    </div>
                  </div>
                  <?php endif ?>
                </div>
              </article>
            </main>
          </div>

          <?php  $this->load->view('elements/internal-sidebar');?>
        </div>
      </div>
    </div>
   
    <?php $this->load->view('elements/footer');?>

  </div>
  <?php $this->load->view('assets/scripts');?>
</body>
</html>