<?php
  $rolledInCourse = checkIfRolled($myID, $curseID);
?>

<!DOCTYPE html>
<html lang="<?php echo $language;?>">
<head>
  <?php $this->load->view('assets/headnfo');?>
  <?php $this->load->view('assets/style');?>
</head>
<body id="tm-container">
  <div class="tm-container">
    <?php $this->load->view('elements/component');?>
    
    <?php $this->load->view('elements/navbar');?>
    
    <?php $this->load->view('elements/spotlight-internal');?>

    <?php $this->load->view('elements/breadcrum-gen');?>

    <div id="tm-main" class="tm-block-main uk-block uk-block-default">
      <div class="uk-container uk-container-center">
        <div class="tm-middle uk-grid" data-uk-grid-match data-uk-grid-margin>
          
          <div class="tm-main uk-width-medium-7-10">
            <main id="tm-content" class="tm-content">
              <article class="uk-article tm-article">
                <div class="tm-article-wrapper">
                  <div class="tm-article-content uk-margin-large-bottom uk-margin-top-remove">
                    <div class="tm-article">
                      <div class="uk-width-medium-3-3" style="float:left;">
                        <img class="uk-border-rounded" src="<?php echo base_url() . 'assets/uploads/files/elearning/' . $pic;?>" alt="<?php echo $titleSpot; ?>" style="float:left; margin-right:10px;">
                        <h2 class="uk-module-title"><?php echo $titleSpot; ?></h2>
                        <p><?php echo $curseDesc; ?></p>
                      </div>
                      <div class="uk-width-medium-3-3" style="float:left; margin-top:15px;">
                        <div class="uk-grid">

                          <div class="uk-width-medium-2-10">
                            <ul class="uk-tab uk-tab-left" data-uk-tab="{connect:'#tabsCourse', animation: 'fade'}">
                              <li class="uk-active">
                                <a href="#">Clases</a>
                              </li>
                              <li>
                                <a href="#">Materiales</a>
                              </li>
                              <li>
                                <a href="#">Material Multimedia</a>
                              </li>
                              <li>
                                <a href="#">Examenes</a>
                              </li>
                              <li>
                                <a href="#">Calificaciones</a>
                              </li>
                            </ul>
                          </div>

                          <div class="uk-width-medium-8-10">
                            <ul id="tabsCourse" class="uk-switcher uk-margin uk-tab-content" style="border: none; padding: 5px !important;">
                              <li>
                                <?php foreach ($cursoClassNfo->result() as $csc): ?>
                                  <h4>
                                    <?php if ($rolledInCourse): ?>
                                    <a href="<?php echo base_url() . 'elearning/class/' . $csc->curso_id . '-' . $csc->slug;?>">
                                      <?php echo $csc->title; ?>
                                    </a>
                                    <?php else: ?>
                                      <?php echo $csc->title; ?>
                                      <h4>Por favor subscribase al curso para ver mas</h4>
                                    <?php endif ?>
                                  </h4>
                                  <p><?php echo $csc->desc; ?></p>
                                <?php endforeach ?>
                              </li>
                              <li>
                                <?php foreach ($cursoMaterial->result() as $mat): ?>
                                  <h4>
                                    <?php echo $mat->title;?>
                                  </h4>
                                    <h5>Tipo de Archivo: <?php echo $mat->material_type;?></h5>
                                    <?php if ($rolledInCourse): ?>
                                      <a href="<?php echo base_url() . 'assets/uploads/files/archives/elearning/' . $mat->archive;?>">
                                        Descargar
                                      </a>
                                    <?php else: ?>
                                      <h4>Por favor subscribase al curso para ver mas</h4>
                                    <?php endif ?>
                                <?php endforeach ?>
                              </li>
                              <li>
                                <h4>Material Multimedia</h4>
                                <?php foreach ($videoMaterial->result() as $vdou): ?>
                                  <a class="uk-button" href="#<?php echo $vdou->curso_class_video_id;?>-<?php echo $vdou->curso_id;?>-<?php echo $vdou->curso_class_id;?>" data-uk-modal="{center:true}"><?php echo $vdou->title;?></a>
                                <?php endforeach ?>
                              </li>
                              <li>
                                <?php foreach ($cursoTest->result() as $exms): ?>
                                    <h5><?php echo $exms->exam_title;?></h5>
                                    <?php if ($rolledInCourse): ?>
                                      <p>
                                        Clase: <?php echo $exms->curso_class_id;?><br>
                                        <?php if (checkIFExamIsDoIt($myID, $exms->curso_exam_id)): ?>
                                          Ya realizaste este Examen. <br>
                                        <?php else: ?>
                                          <a class="uk-button-line uk-button" href="<?php echo base_url() . 'elearning/exam/' . $exms->curso_exam_id . '-' . $curseID;?>">Realizar Examen</a><br>
                                        <?php endif ?>
                                        Calificacion: <?php echo getExamCal($myID, $exms->curso_exam_id);?>
                                      </p>
                                    <?php else: ?>
                                      <h4>Por favor subscribase al curso para ver mas</h4>
                                    <?php endif ?>
                                <?php endforeach ?>
                              </li>
                              <li>
                                <h4>Curso terminado en un <?php echo getPortencualOfTheCourseByUserIDCourseID($myID, $curseID);?>%</h4>
                                <p>
                                  Promedio de Nota Final: <?php echo getPromCalificationFromCourseByUserIDCourseID($curseID, $myID); ?><br>
                                  <small>Los examenes no realizados tendran una calificacion de 0, la cual sera promediable.</small><br>
                                  <?php echo countExamsFromCourseByUser($myID, $curseID);?>/<?php echo countExamsFromCourse($curseID);?> Examenes Finalizados.
                                </p>
                              </li>
                            </ul>
                          </div>

                        </div>
                      </div>
                      <?php if ($rolledInCourse != TRUE): ?>
                        <div class="uk-width-medium-3-3" style="float:left; margin-top: 15px;">
                          <h2 class="uk-module-title">Usted aún no se dio de Alta en este Curso</h2>
                          <p>Para disfrutar de este curso completo, debe subscribirse.</p>
                          <form id="loginForm" action="<?php echo base_url();?>Elearning/enrollme" method="post" accept-charset="utf-8">
                            <input type="hidden" name="userID" id="userID" value="<?php echo $myID;?>" readonly>
                            <input type="hidden" name="courseID" id="courseID" value="<?php echo $curseID;?>" readonly>
                            <button type="submit" class="uk-button-success uk-button-large uk-button">Darme de Alta</button>
                          </form>
                        </div>
                      <?php endif ?>
                    </div>
                  </div>
                </div>
              </article>
            </main>
          </div>

          <?php  $this->load->view('elements/internal-sidebar');?>
        </div>
      </div>
    </div>
   
    <?php $this->load->view('elements/footer');?>
    
    <?php foreach ($videoMaterial->result() as $vdos): ?>
      <div class="uk-modal" id="<?php echo $vdos->curso_class_video_id;?>-<?php echo $vdos->curso_id;?>-<?php echo $vdos->curso_class_id;?>">
        <div class="uk-modal-dialog uk-panel-box uk-panel uk-panel-header uk-modal-dialog-large">
          <a class="uk-modal-close uk-close"></a>
          <div class="">
            <div style="height: 461.25px; float: none; clear: both; margin: 2px auto;">
              <embed src="<?php echo $vdos->link;?>?version=3&amp;hl=en_US&amp;rel=0&amp;autohide=1&amp;autoplay=0" wmode="transparent" type="application/x-shockwave-flash" width="100%" height="100%" allowfullscreen="true" title="Adobe Flash Player">
            </div>
          </div>
        </div>
      </div>
    <?php endforeach ?>

  </div>
  <?php $this->load->view('assets/scripts');?>
</body>
</html>