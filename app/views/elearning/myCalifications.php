<!DOCTYPE html>
<html lang="<?php echo $language;?>">
<head>
  <?php $this->load->view('assets/headnfo');?>
  <?php $this->load->view('assets/style');?>
</head>
<body id="tm-container">
  <div class="tm-container">
    <?php $this->load->view('elements/component');?>
    
    <?php $this->load->view('elements/navbar');?>
    
    <?php $this->load->view('elements/spotlight-internal');?>

    <?php $this->load->view('elements/breadcrum-gen');?>

    <div id="tm-main" class="tm-block-main uk-block uk-block-default">
      <div class="uk-container uk-container-center">
        <div class="tm-middle uk-grid" data-uk-grid-match data-uk-grid-margin>
          
          <div class="tm-main uk-width-medium-7-10">
            <main id="tm-content" class="tm-content">
              <article class="uk-article tm-article">
                <div class="tm-article-wrapper">
                  <div class="tm-article-content uk-margin-large-bottom uk-margin-top-remove">
                    <div class="tm-article">
                      <table class="uk-table">
                        <caption>Cursadas Actuales</caption>
                        <thead>
                          <tr>
                            <th>Nombre</th>
                            <th>Descripcion</th>
                            <th>Link</th>
                            <th>Estado</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php for ($i = 0; $i < $coursesLimiter; $i++) { ?>
                          <tr>
                            <?php foreach ($coursesHistorial[$i]->result() as $csc): ?>
                              <td><?php echo $csc->name; ?></td>
                              <td><?php echo $csc->desc; ?></td>
                              <td><a href="<?php echo base_url() . 'elearning/course/' . $csc->curso_id . '-' . $csc->slug; ?>">Ver Curso</a></td>
                              <td>Completo</td>
                            <?php endforeach ?>
                          </tr>
                          <?php }?>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </article>
            </main>
          </div>

          <?php  $this->load->view('elements/internal-sidebar');?>
        </div>
      </div>
    </div>
   
    <?php $this->load->view('elements/footer');?>

  </div>
  <?php $this->load->view('assets/scripts');?>
</body>
</html>