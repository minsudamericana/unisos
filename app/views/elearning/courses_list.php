<!DOCTYPE html>
<html lang="<?php echo $language;?>">
<head>
  <?php $this->load->view('assets/headnfo');?>
  <?php $this->load->view('assets/style');?>
</head>
<body id="tm-container">
  <div class="tm-container">
    <?php $this->load->view('elements/component');?>
    
    <?php $this->load->view('elements/navbar');?>
    
    <?php $this->load->view('elements/spotlight-internal');?>

    <?php $this->load->view('elements/breadcrum-gen');?>

    <div id="tm-main" class="tm-block-main uk-block uk-block-default">
      <div class="uk-container uk-container-center">
        <div class="tm-middle uk-grid" data-uk-grid-match data-uk-grid-margin>
          
          <div class="tm-main uk-width-medium-7-10">
            <main id="tm-content" class="tm-content">
              <article class="uk-article tm-article">
                <div class="tm-article-wrapper">
                  <div class="tm-article-content uk-margin-large-bottom uk-margin-top-remove">
                    <div class="tm-article">
          						<h2 class="uk-module-title">Conozca nuestra solucion a problemas formativos de las Empresas.</h2>
                      <p>
                        En nuestra plataforma, los recursos humanos de su empresa, podran perfeccionarse y certificarse en una gran variedad de especializaciones claves para desempeñar sus cargos. <br>
                        Desde habilidades Empresariales e Interpersonales, hasta capacitaciones en Sistemas de Gestion, como de Normas de Calidad. <br>
                        Los cursos estan confeccionados cuidadosamente por equipos multidisciplinarios de Ingenieros, Licenciados y Profesores Universitarios.
                      </p>
                        <?php foreach ($cursosList->result() as $csl): ?>
                          <div class="uk-width-medium-1-3" style="float:left;margin-right:5px;">
                            <div class="uk-panel uk-panel-box tm-panel-card">
                              <div class="uk-panel-teaser">
                                <img class="tm-card-avatar" src="<?php echo base_url() . 'assets/uploads/files/elearning/' . $csl->pic; ?>" alt="<?php echo $csl->name; ?>" >
                              </div>
                              <a class="tm-card-link" href="<?php echo base_url() . 'elearning/course/' . $csl->curso_id . '-' . $csl->slug;?>"></a>
                              <div class="tm-card-content">
                                <h3 class="uk-panel-title"><?php echo $csl->name; ?></h3>
                                <h4 class="tm-card-title"><?php echo $csl->desc; ?></h4>
                              </div>
                            </div>
                          </div>
                        <?php endforeach ?>
                    </div>
                  </div>
                </div>
              </article>
            </main>
          </div>

          <?php  $this->load->view('elements/internal-sidebar');?>
        </div>
      </div>
    </div>
   
    <?php $this->load->view('elements/footer');?>

  </div>
  <?php $this->load->view('assets/scripts');?>
</body>
</html>