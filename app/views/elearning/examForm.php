<!DOCTYPE html>
<html lang="<?php echo $language;?>">
<head>
  <?php $this->load->view('assets/headnfo');?>
  <?php $this->load->view('assets/style');?>
</head>
<body id="tm-container">
  <div class="tm-container">
    <?php $this->load->view('elements/component');?>
    
    <?php $this->load->view('elements/navbar');?>
    
    <?php $this->load->view('elements/spotlight-internal');?>

    <?php $this->load->view('elements/breadcrum-gen');?>

    <div id="tm-main" class="tm-block-main uk-block uk-block-default">
      <div class="uk-container uk-container-center">
        <div class="tm-middle uk-grid" data-uk-grid-match data-uk-grid-margin>
          
          <div class="tm-main uk-width-medium-7-10">
            <main id="tm-content" class="tm-content">
              <article class="uk-article tm-article">
                <div class="tm-article-wrapper">
                  <div class="tm-article-content uk-margin-large-bottom uk-margin-top-remove">
                    <div class="tm-article">
  
                      <?php foreach ($cursoExamNfo->result() as $exm): ?>
                        <?php $cursoID = $exm->curso_id ?>
                        <div class="tm-article">
                          <h2 class="uk-module-title"><?php echo $exm->exam_title;?></h2>
                          <p><?php echo $exm->exam_desc;?></p>
                          <p>
                            <?php echo countQuestAnswInExam($examID);?> 
                            Preguntas Respondidas de 
                            <?php echo countQuestInExam($examID);?>.
                          </p>
                          <p>Las preguntas se responderan una sola vez. Para cambios de calificacion, debera comunicarse por e-mail con nuestro departamento de Pedagogia.</p>
                        </div>
                      <?php endforeach ?>            
                      
                      <?php if (countQuestAnswInExam($examID) == countQuestInExam($examID)): ?>
                      <?php else: ?>
                      <div class="uk-width-1-1 uk-grid-margin uk-row-first" style="margin-top: 0; margin-bottom: 10px;">
                        <?php for ($questionNumber = 1; $questionNumber <= countQuestInExam($examID); $questionNumber++) { ?>
                          <?php if (getLastQuestAnswerByExamIDUserID($myID, $examID) >= $questionNumber): ?>
                            <div class="uk-badge-note uk-badge-notification uk-badge completedAnswer"><?php echo $questionNumber; ?></div>
                          <?php else: ?>
                            <div class="uk-badge-note uk-badge-notification uk-badge"><?php echo $questionNumber; ?></div>
                          <?php endif ?>
                        <?php } ?>
                      </div>
                      <?php endif ?>

                      <?php $questNumerator = 1;?>
                      <?php $limiter = 1; ?>
                      <?php if (getLastQuestAnswerByExamIDUserID($myID, $examID) == null): ?>
                        <?php foreach ($cursoExamQuest->result() as $exmQuest): ?>
                            <?php if ($limiter == 1): ?>
                              <div id="questNum_<?php echo $questNumerator;?>">
                                <div class="uk-width-1-1 uk-form uk-grid-margin uk-row-first"style="float:left;margin-top: -42px;">
                                  <div class="uk-width-1-1 uk-form uk-grid-margin uk-row-first"style="float:left;">
                                      <div class="uk-width-1-6">
                                        <div class="uk-panel uk-panel-box uk-panel-box-primary" style="height: 40px;">
                                            <h3 class="uk-panel-title"><?php echo $questNumerator; ?></h3>
                                        </div>
                                      </div>
                                  </div>
                                  <div class="uk-width-1-1 uk-form uk-grid-margin uk-row-first">
                                      <div class="uk-width-1-2" style="float:left;">
                                        <div class="uk-panel uk-panel-box uk-panel-box-primary" style="float:left; height: 196px; width: 85%;">
                                            <?php echo $exmQuest->question;?>
                                        </div>
                                      </div>
                                      <div class="uk-width-1-2" style="float:left; height: 300px;">
                                        <div class="uk-panel uk-panel-box" style="height: 196px;">
                                            <fieldset>
                                              <?php 
                                                $this->db->where('curso_exam_id', $exmQuest->curso_exam_id);
                                                $this->db->where('questNum', $questNumerator);
                                                $questions = $this->db->get('curso_exam_quest');
                                                $numerator = 1;
                                                $numberOfQuestions = countQuestInExam($exmQuest->curso_exam_id);
                                              ?>
                                              <?php foreach ($questions->result() as $qst): ?>
                                                
                                                    <?php $state = checkIfQuestIsDoIt($qst->curso_exam_quest_id, $myID); ?>
                                                      <?php if ($state == true): ?>
                                                      <p>Esta pregunta ya fue respondida.</p>
                                                      <?php else: ?>
                                                        <p id="answeredQuestion_<?php echo $numerator;?>" style="display:none;">Esta pregunta ya fue respondida.</p>
                                                        <form id="questionForm_<?php echo $numerator;?>" action="" method="post" accept-charset="utf-8">
                                                          <input type="hidden" id="questionID" name="questionID" value="<?php echo $qst->curso_exam_quest_id; ?>" readonly>
                                                          <input type="hidden" id="userID" name="userID" value="<?php echo $myID; ?>" readonly>
                                                          <input type="hidden" id="cursoID" name="cursoID" value="<?php echo $cursoID; ?>" readonly>
                                                          <input type="hidden" id="questNum" name="questNum" value="<?php echo $exmQuest->questNum; ?>; ?>" readonly>
                                                          <input type="hidden" id="examID" name="examID" value="<?php echo $exmQuest->curso_exam_id; ?>" readonly>
                                                          <div class="uk-form-row">
                                                            <input type="radio" id="respVal" name="respVal" value="1"> <?php echo $qst->resp_one;?><br>
                                                          </div>
                                                          <div class="uk-form-row">
                                                            <input type="radio" id="respVal" name="respVal" value="2"> <?php echo $qst->resp_two;?><br>
                                                          </div>
                                                          <div class="uk-form-row">
                                                            <input type="radio" id="respVal" name="respVal" value="3"> <?php echo $qst->resp_three;?><br>
                                                          </div>
                                                          <div class="uk-form-row">
                                                            <button type="submit" id="correctQuest" class="uk-button-default uk-button">Enviar Respuesta</button>
                                                          </div>
                                                        </form>
                                                      <?php endif ?>
                                                  <?php $numerator++; ?>
                                                  <?php $respVal = questValInFunc($qst->curso_exam_quest_id, $myID); ?>

                                              <?php endforeach ?>
                                            </fieldset>
                                        </div>
                                      </div>
                                  </div>
                                </div>
                                <?php $questNumerator++;?>
                              </div>
                            <?php endif ?>
                            <?php $limiter++; ?>
                        <?php endforeach ?>
                      <?php else: ?>
                        <?php foreach ($cursoExamQuest->result() as $exmQuest): ?>
                          <?php  if ((getLastQuestAnswerByExamIDUserID($myID, $examID)) == ($exmQuest->questNum - 1)): ?>
                            <div id="questNum_<?php echo $questNumerator;?>">
                          <?php else: ?>
                            <div id="questNum_<?php echo $questNumerator;?>" style="display:none;">
                          <?php  endif ?>
                              <div class="uk-width-1-1 uk-form uk-grid-margin uk-row-first"style="float:left;margin-top: -42px;">
                                <div class="uk-width-1-1 uk-form uk-grid-margin uk-row-first"style="float:left;">
                                    <div class="uk-width-1-6">
                                      <div class="uk-panel uk-panel-box uk-panel-box-primary" style="height: 40px;">
                                          <h3 class="uk-panel-title"><?php echo $questNumerator; ?></h3>
                                      </div>
                                    </div>
                                </div>
                                <div class="uk-width-1-1 uk-form uk-grid-margin uk-row-first">
                                    <div class="uk-width-1-2" style="float:left;">
                                      <div class="uk-panel uk-panel-box uk-panel-box-primary" style="float:left; height: 196px; width: 85%;">
                                          <?php echo $exmQuest->question;?>
                                      </div>
                                    </div>
                                    <div class="uk-width-1-2" style="float:left; height: 300px;">
                                      <div class="uk-panel uk-panel-box" style="height: 196px;">
                                          <fieldset>
                                            <?php 
                                              $this->db->where('curso_exam_id', $exmQuest->curso_exam_id);
                                              $this->db->where('questNum', $questNumerator);
                                              $questions = $this->db->get('curso_exam_quest');
                                              $numerator = 1;
                                              $numberOfQuestions = countQuestInExam($exmQuest->curso_exam_id);
                                            ?>
                                            <?php foreach ($questions->result() as $qst): ?>
                                              
                                                  <?php $state = checkIfQuestIsDoIt($qst->curso_exam_quest_id, $myID); ?>
                                                    <?php if ($state == true): ?>
                                                    <p>Esta pregunta ya fue respondida.</p>
                                                    <?php else: ?>
                                                      <p id="answeredQuestion_<?php echo $numerator;?>" style="display:none;">Esta pregunta ya fue respondida.</p>
                                                      <form id="questionForm_<?php echo $numerator;?>" action="" method="post" accept-charset="utf-8">
                                                        <input type="hidden" id="questionID" name="questionID" value="<?php echo $qst->curso_exam_quest_id; ?>" readonly>
                                                        <input type="hidden" id="userID" name="userID" value="<?php echo $myID; ?>" readonly>
                                                        <input type="hidden" id="cursoID" name="cursoID" value="<?php echo $cursoID; ?>" readonly>
                                                        <input type="hidden" id="questNum" name="questNum" value="<?php echo $exmQuest->questNum; ?>; ?>" readonly>
                                                        <input type="hidden" id="examID" name="examID" value="<?php echo $exmQuest->curso_exam_id; ?>" readonly>
                                                        <div class="uk-form-row">
                                                          <input type="radio" id="respVal" name="respVal" value="1"> <?php echo $qst->resp_one;?><br>
                                                        </div>
                                                        <div class="uk-form-row">
                                                          <input type="radio" id="respVal" name="respVal" value="2"> <?php echo $qst->resp_two;?><br>
                                                        </div>
                                                        <div class="uk-form-row">
                                                          <input type="radio" id="respVal" name="respVal" value="3"> <?php echo $qst->resp_three;?><br>
                                                        </div>
                                                        <div class="uk-form-row">
                                                          <button type="submit" id="correctQuest" class="uk-button-default uk-button">Enviar Respuesta</button>
                                                        </div>
                                                      </form>
                                                    <?php endif ?>
                                                <?php $numerator++; ?>
                                                <?php $respVal = questValInFunc($qst->curso_exam_quest_id, $myID); ?>

                                            <?php endforeach ?>
                                          </fieldset>
                                      </div>
                                    </div>
                                </div>
                              </div>
                              <?php $questNumerator++;?>
                            </div>

                        <?php endforeach ?>
                      <?php endif ?>
                      

                      <?php if (checkIfCurseExamItsCompleted($myID, $examID, countQuestInExam($examID))): ?>
                        <div class="uk-width-1-1 uk-grid-margin uk-row-first" style="margin-top: 10px;">
                            <div class="uk-progress uk-progress-success uk-progress-striped uk-active">
                                <div class="uk-progress-bar" style="width: 100%">Examen Completado, Promediando Calificacion. Por favor espere...</div>
                            </div>
                        </div>
                      <?php else: ?>
                        <div class="uk-width-1-1 uk-grid-margin uk-row-first" style="margin-top: 10px;">
                            <div class="uk-progress">
                                <div id="progressBar" class="uk-progress-bar" style="width: 55%;">Completado en un <span id="examPercent">55</span>%</div>
                            </div>
                        </div>
                      <?php endif ?>

                    </div>
                  </div>
                </div>
              </article>
            </main>
          </div>

          <?php  $this->load->view('elements/internal-sidebar');?>
        </div>
      </div>
    </div>
   
    <?php $this->load->view('elements/footer');?>

  </div>
  <?php $this->load->view('assets/scripts');?>

<script>
  function calculatePercent(val1, val2) {
      var percent = (val1 * 100)/val2;
      var percentRound = Math.round(percent);
      $('#examPercent').html(percentRound);
      $('#progressBar').css('width', percentRound + '%');
  }
  calculatePercent(<?php echo countQuestAnswInExam($examID);?> , <?php echo countQuestInExam($examID);?>);
</script>

<?php for ($i=1; $i <= $numberOfQuestions; $i++) { ?>
  <script type='text/javascript' language='javascript'>
      $('#questionForm_<?php echo $i?>').submit(function(event) {  

          event.preventDefault();

          var $form = $( this );                  
          var urlForm = '<?php echo base_url();?>Elearning/saveQuestionInExam';
          var questionID = $("#questionID").val();
          var userID = $("#userID").val();
          var cursoID = $("#cursoID").val();
          var examID = $("#examID").val();
          var questNum = $("#questNum").val();
          var respVal = $('input[name=respVal]:checked').val()

          $.ajax({
            method: 'POST',
            url: urlForm,
            data: {questionID: questionID, userID: userID, respVal: respVal, cursoID: cursoID, examID : examID, questNum: questNum},
            beforeSend: function() {
                console.log('enviando respuesta');
              },
          }).done(function() {
            $('#answeredQuestion_<?php echo $i;?>').show();
            $('#questionForm_<?php echo $i;?>').hide();
            location.reload();
          })
          .fail(function() {
          });   

      });
  </script>
<?php }?>

  <style>
    .uk-badge.completedAnswer {
        background: #258bce;
        border: 1px solid #258bce;
        color: #fff;
    }
  </style>

</body>
</html>


