<!doctype html>
<html lang="<?php echo $language;?>">
<head>
    <?php $this->load->view('assets/headnfo');?>
    <?php $this->load->view('assets/style');?>
</head>
<body class="home">
<?php $this->load->view('elements/navbar');?>
<div class="wrapper">
    
    <div class="section">
       <div class="container">
           <h2 class="section-title">Crear Cuenta</h2>
           <div class="row">
            <div class="col-md-12">
              <form id="registForm" action="<?php echo base_url();?>User/regUser" method="post" accept-charset="utf-8">
                    <div class="form-group">
                        <input type="text" value="" id="regname" name="name" placeholder="Nombre" class="form-control">
                    </div>
                    <div class="form-group">
                        <input type="text" value="" id="reglastname" name="lastname" placeholder="Apellido" class="form-control">
                    </div>
                    <div class="form-group">
                        <input type="text" value="" id="regcompany" name="company" placeholder="Compañia" class="form-control">
                    </div>
                    <div class="form-group">
                        <input type="text" value="" id="regphone" name="phone" placeholder="Telefono" class="form-control">
                    </div>
                    <div class="form-group">
                        <input type="text" value="" id="regmail" name="mail" placeholder="E-Mail" class="form-control">
                    </div>
                    <div class="form-group">
                        <input type="password" value="" id="regpass" name="pass" placeholder="Contraseña" class="form-control">
                    </div>
                    <div class="form-group">
                        <input type="password" value="" id="regrepeatpass" name="rpass" placeholder="Repetir Contraseña" class="form-control">
                    </div>
                    <span class="alert alert-danger alert-mail" role="alert" id="incorrectPass" style="display:none;">Las Contraseñas no son iguales</span>
                    <button type="submit" class="btn btn-info btn-fill btn-block">Crear Cuenta</button>
                </form>
                <div id="loadingDiv" style="width:100%;float:left;display:none;">
                    <img src="<?php echo base_url();?>assets/img/AjaxLoader.gif"> Registrando....
                </div>
                <span class="alert alert-success alert-mail" role="alert" id="contactOk" style="display:none;">Se registro con Exito</span>
                <span class="alert alert-danger alert-mail" role="alert" id="contactError" style="display:none;">Se produjo un error</span>
            </div>
           </div>
       </div>
    </div>

    <div class="space-50"></div>
    <?php $this->load->view('elements/footer');?>
</div>
</body>
  <?php $this->load->view('assets/scripts');?>
</html>