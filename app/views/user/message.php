<!DOCTYPE html>
<html lang="<?php echo $language;?>">
<head>
  <?php $this->load->view('assets/headnfo');?>
  <?php $this->load->view('assets/style');?>
</head>
<body id="tm-container">
  <div class="tm-container">
    <?php $this->load->view('elements/component');?>
    <?php $this->load->view('elements/navbar');?>
    <?php $this->load->view('elements/spotlight-internal');?>
    <?php $this->load->view('elements/breadcrum-gen');?>
    <div id="tm-main" class="tm-block-main uk-block uk-block-default">
      <div class="uk-container uk-container-center">
        <div class="tm-middle uk-grid" data-uk-grid-match data-uk-grid-margin>
          <div class="tm-main uk-width-medium-10-10">
            <main id="tm-content" class="tm-content">
              <article class="uk-article tm-article">
                <div class="tm-article-wrapper">
                  <div class="tm-article-content uk-margin-large-bottom uk-margin-top-remove">
                    <div class="tm-article">
                        <?php if ($msgFilter == 1): ?>
                            <a class="uk-button-primary uk-button" href="<?php echo base_url();?>User/viewMsg/1" target="_self">Mensajes Nuevos</a>
                        <?php else: ?>
                            <a class="uk-button-line uk-button" href="<?php echo base_url();?>User/viewMsg/1" target="_self">Mensajes Nuevos</a>
                        <?php endif ?>
                        <?php if ($msgFilter == 2): ?>
                            <a class="uk-button-primary uk-button" href="<?php echo base_url();?>User/viewMsg/2" target="_self">Mensajes Leidos</a>
                        <?php else: ?>
                            <a class="uk-button-line uk-button" href="<?php echo base_url();?>User/viewMsg/2" target="_self">Mensajes Leidos</a>
                        <?php endif ?>
                        <?php if ($msgFilter == 3): ?>
                            <a class="uk-button-primary uk-button" href="<?php echo base_url();?>User/viewMsg/3" target="_self">Mensajes Archivados</a>
                        <?php else: ?>
                            <a class="uk-button-line uk-button" href="<?php echo base_url();?>User/viewMsg/3" target="_self">Mensajes Archivados</a>
                        <?php endif ?>
                        <table class="uk-table uk-table-hover uk-table-striped">
                          <thead>
                            <tr>
                              <th>#</th>
                              <th>Nombre</th>
                              <th>Telefono</th>
                              <th>E-Mail</th>
                              <th>Mensaje</th>
                              <th>Accion</th>
                            </tr>
                          </thead>
                          <tfoot>
                            <tr>
                              <td>#</td>
                              <td>Nombre</td>
                              <td>Telefono</td>
                              <td>E-Mail</td>
                              <td>Mensaje</td>
                              <td>Accion</td>
                            </tr>
                          </tfoot>
                          <tbody>
                            <?php $counter = 1; ?>
                            <?php if ($messages->result() > 0): ?>
                                <?php foreach ($messages->result() as $sg): ?>
                                    <?php if ($msgFilter == $sg->state): ?>
                                        <tr>
                                          <td><?php echo $counter; ?></td>
                                          <td><?php echo $sg->name; ?></td>
                                          <td><?php echo $sg->phone; ?></td>
                                          <td><?php echo $sg->mail; ?></td>
                                          <td><?php echo word_limiter($sg->message, 6); ?></td>
                                          <td>
                                            <a href="#viewMsg-<?php echo $sg->msg_id; ?>" data-uk-modal>Leer</a> 
                                            | 
                                            <?php if ($msgFilter == 1): ?>
                                                <a href="<?php echo base_url() . 'User/stateMsg/' . $sg->msg_id . '/3/' . $msgFilter;?>">Archivar</a>
                                            <?php elseif($msgFilter == 2): ?>
                                                <a href="<?php echo base_url() . 'User/stateMsg/' . $sg->msg_id . '/2/' . $msgFilter;?>">Marcar Como No Leido</a>
                                            <?php elseif($msgFilter == 3): ?>
                                                <a href="<?php echo base_url() . 'User/stateMsg/' . $sg->msg_id . '/1/' . $msgFilter;?>">Restaurar</a> 
                                                | 
                                                <a href="<?php echo base_url() . 'User/deleteMsg/' . $sg->msg_id . '/' . $msgFilter;?>">Eliminar</a>
                                            <?php endif ?>
                                          </td>
                                        </tr>
                                        <?php $counter = $counter + 1; ?>
                                    <?php endif ?>
                                <?php endforeach ?>
                                <?php if ($counter == 1): ?>
                                    <tr>
                                        <td>No tienes mensajes...</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                <?php endif ?>
                            <?php else: ?>
                                <tr>
                                    <td>No tienes mensajes...</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            <?php endif ?>
                          </tbody>
                        </table>
                    </div>
                  </div>
                </div>
              </article>
            </main>
          </div>
        </div>
      </div>
    </div>
    <?php $this->load->view('elements/footer');?>
  </div>
  <?php $this->load->view('assets/scripts');?>
    
    <?php foreach ($messages->result() as $sg): ?>
        <?php if ($msgFilter == $sg->state): ?>
            <div id="viewMsg-<?php echo $sg->msg_id; ?>" class="uk-modal">
              <div class="uk-modal-dialog uk-panel-box">
                <a href="" class="uk-modal-close uk-close"></a>
                <h3 class="uk-panel-title">Mensaje de <?php echo $sg->name; ?></h3>
                <p>
                    Telefono: <?php echo $sg->name; ?><br>
                    E-Mail: <?php echo $sg->mail; ?><br>
                    Mensaje: <?php echo $sg->message; ?>
                </p>
                <?php if ($msgFilter == 1): ?>
                    <a class="uk-button-default uk-button" href="<?php echo base_url() . 'User/stateMsg/' . $sg->msg_id . '/2/' . $msgFilter;?>" target="_self">Marcar Como Leido</a>
                    <a class="uk-button-default uk-button" href="<?php echo base_url() . 'User/stateMsg/' . $sg->msg_id . '/3/' . $msgFilter;?>" target="_self">Archivar</a>
                <?php elseif ($msgFilter == 2): ?>
                    <a class="uk-button-default uk-button" href="<?php echo base_url() . 'User/stateMsg/' . $sg->msg_id . '/1/' . $msgFilter;?>" target="_self">Marcar Como No Leido</a>
                    <a class="uk-button-default uk-button" href="<?php echo base_url() . 'User/stateMsg/' . $sg->msg_id . '/3/' . $msgFilter;?>" target="_self">Archivar</a>
                <?php elseif ($msgFilter == 3): ?>
                    <a class="uk-button-default uk-button" href="<?php echo base_url() . 'User/stateMsg/' . $sg->msg_id . '/1/' . $msgFilter;?>" target="_self">Restaurar</a>
                    <a class="uk-button-default uk-button" href="<?php echo base_url() . 'User/deleteMsg/' . $sg->msg_id . '/' . $msgFilter;?>" target="_self">Eliminar</a>
                <?php endif ?>
                <a class="uk-button-primary uk-button" href="#respond-<?php echo $sg->msg_id;?>" data-uk-modal>Responder</a>
              </div>
            </div>
        <?php endif ?>
    <?php endforeach ?>
    
    <?php 
      $userID = getMyID();
      $industrialID = getIndustrialUserByID($userID);
      $indName = getIndustrialName($industrialID);
    ?>

    <?php foreach ($messages->result() as $sg): ?>
        <div id="respond-<?php echo $sg->msg_id;?>" class="uk-modal">
          <div class="uk-modal-dialog uk-panel-box">
            <a href="" class="uk-modal-close uk-close"></a>
            <h3 class="uk-panel-title">Responder a <?php echo $sg->name; ?></h3>
            <form class="uk-form uk-form-stacked" id="respondForm" action="<?php echo base_url();?>User/respondMail" method="post" accept-charset="utf-8">
                <div class="form-group">
                    <input type="hidden" name="IndName" id="IndName" value="<?php echo $indName;?>" readonly>
                    <input type="hidden" name="mailToResp" id="mailToResp" value="<?php echo $sg->mail; ?>" readonly>
                    <label for="slug" class="uk-form-label">Respuesta:</label>
                    <textarea name="respondTxt" id="respondTxt" style="width:100%;"></textarea>
                </div>
                <br>
                <button type="submit" class="btn btn-info btn-fill btn-block">Enviar</button>
            </form>
          </div>
        </div>
    <?php endforeach ?>

</body>
</html>