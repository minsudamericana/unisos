<!DOCTYPE html>
<html lang="<?php echo $language;?>">
<head>
  <?php $this->load->view('assets/headnfo');?>
  <?php $this->load->view('assets/style');?>
</head>
<body id="tm-container">
  <div class="tm-container">
    <?php $this->load->view('elements/component');?>
    <?php $this->load->view('elements/navbar');?>
    <?php $this->load->view('elements/spotlight-internal');?>
    <?php $this->load->view('elements/breadcrum-gen');?>
    <div id="tm-main" class="tm-block-main uk-block uk-block-default">
      <div class="uk-container uk-container-center">
        <div class="tm-middle uk-grid" data-uk-grid-match data-uk-grid-margin>
          <div class="tm-main uk-width-medium-7-10">
            <main id="tm-content" class="tm-content">
              <article class="uk-article tm-article">
                <div class="tm-article-wrapper">
                  <div class="tm-article-content uk-margin-large-bottom uk-margin-top-remove">
                    <div class="tm-article">
                        <form class="uk-form uk-form-stacked" id="loginForm" action="<?php echo base_url();?>Auth/change_password" method="post" accept-charset="utf-8">
                            <legend>Cambio de Contraseña</legend>
                            <div class="uk-form-row">
                                <label for="old_password">Antigua Contraseña</label>
                                <input type="password" name="old_password" id="old" value="" placeholder="" class="uk-width-1-1">
                            </div>
                            <div class="uk-form-row">
                                <label for="new_password">Nueva Contraseña</label>
                                <input type="password" name="new_password" id="new" value="" placeholder="" class="uk-width-1-1">
                            </div>
                            <div class="uk-form-row">
                                <label for="new_password_confirm">Confirmar Nueva Contraseña</label>
                                <input type="password" name="new_password_confirm" id="new_confirm" value="" placeholder="" class="uk-width-1-1">
                            </div>
                            <input type="hidden" name="user_id" value="<?php echo $userID;?>" id="user_id">
                            <br>
                            <button type="submit" class="uk-button uk-button-primary">Guardar</button>
                        </form>
                        <div id="loadingDiv" style="width:100%;float:left;display:none;">
                            <img src="<?php echo base_url();?>assets/img/AjaxLoader.gif"> Guardando Datos....
                        </div>
                        <span class="alert alert-success alert-mail" role="alert" id="contactOk" style="display:none;">
                            Se cambiaron los datos exitosamente
                        </span>
                        <span class="alert alert-danger alert-mail" role="alert" id="contactError" style="display:none;">
                            Se produjo un error
                        </span>
                    </div>
                  </div>
                </div>
              </article>
            </main>
          </div>
        </div>
      </div>
    </div>
    <?php $this->load->view('elements/footer');?>
  </div>
  <?php $this->load->view('assets/scripts');?>
</body>
</html>