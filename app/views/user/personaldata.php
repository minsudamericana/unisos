<!DOCTYPE html>
<html lang="<?php echo $language;?>">
<head>
  <?php $this->load->view('assets/headnfo');?>
  <?php $this->load->view('assets/style');?>
</head>
<body id="tm-container">
  <div class="tm-container">
    <?php $this->load->view('elements/component');?>
    <?php $this->load->view('elements/navbar');?>
    <?php $this->load->view('elements/spotlight-internal');?>
    <?php $this->load->view('elements/breadcrum-gen');?>
    <div id="tm-main" class="tm-block-main uk-block uk-block-default">
      <div class="uk-container uk-container-center">
        <div class="tm-middle uk-grid" data-uk-grid-match data-uk-grid-margin>
          <div class="tm-main uk-width-medium-7-10">
            <main id="tm-content" class="tm-content">
              <article class="uk-article tm-article">
                <div class="tm-article-wrapper">
                  <div class="tm-article-content uk-margin-large-bottom uk-margin-top-remove">
                    <div class="tm-article">
                        <?php foreach ($userNfo->result() as $usd): ?>
                        <form class="uk-form uk-form-stacked" id="loginForm" action="<?php echo base_url();?>User/changePersonalNfo" method="post" accept-charset="utf-8">
                            <legend>Informacion Personal</legend>
                            <div class="uk-form-row">
                                <label class="uk-form-label" for="usernameMod">Nombre de Usuario</label>
                                <input type="text" name="usernameMod" id="usernameMod" value="<?php echo $usd->username;?>" placeholder="Nombre de Usuario" class="uk-width-1-1">
                            </div>
                            <div class="uk-form-row">
                                <label class="uk-form-label" for="fnameMod">Nombre</label>
                                <input type="text" name="fnameMod" id="fnameMod" value="<?php echo $usd->first_name;?>" placeholder="Nombre" class="uk-width-1-1">
                            </div>
                            <div class="uk-form-row">
                                <label class="uk-form-label" for="lnameMod">Apellido</label>
                                <input type="text" name="lnameMod" id="lnameMod" value="<?php echo $usd->last_name;?>" placeholder="Apellido" class="uk-width-1-1">
                            </div>
                            <div class="uk-form-row">
                                <label class="uk-form-label" for="aboutMod">Sobre mi</label>
                                <input type="text" name="aboutMod" id="aboutMod" value="<?php echo $usd->about;?>" placeholder="Sobre mi" class="uk-width-1-1">
                            </div>
                            <div class="uk-form-row">
                                <label class="uk-form-label" for="companyMod">Compañia</label>
                                <input type="text" name="companyMod" id="companyMod" value="<?php echo $usd->company;?>" placeholder="Compañia" class="uk-width-1-1">
                            </div>
                            <legend>Informacion de Ubicacion</legend>
                            <div class="uk-form-row">
                                <label class="uk-form-label" for="adressMod">Direccion</label>
                                <input type="text" name="adressMod" id="adressMod" value="<?php echo $usd->adress;?>" placeholder="Direccion" class="uk-width-1-1">
                            </div>
                            <div class="uk-form-row">
                                <label class="uk-form-label" for="paisID">Pais</label>
                                <select id="paisID" name="paisID" class="uk-width-1-1">
                                  <option value="">Seleccione un Pais</option>
                                  <?php foreach ($pais->result() as $sg): ?>
                                    <option value="<?php echo $sg->id; ?>"><?php echo $sg->nombre; ?></option>  
                                  <?php endforeach ?>
                                </select>
                            </div>
                            <div class="uk-form-row">
                                <label class="uk-form-label" for="provinciaID">Provincia</label>
                                <select id="provinciaID" name="provinciaID" class="uk-width-1-1">
                                </select>
                            </div>
                            <div class="uk-form-row">
                                <label class="uk-form-label" for="partidoID">Partido</label>
                                <select id="partidoID" name="partidoID" class="uk-width-1-1">
                                </select>
                            </div>
                            <div class="uk-form-row">
                                <label class="uk-form-label" for="localidadID">Localidad</label>
                                <select id="localidadID" name="localidadID" class="uk-width-1-1">
                                </select>
                            </div>
                            <div class="uk-form-row">
                                <label class="uk-form-label" for="barrioID">Barrio</label>
                                <select id="barrioID" name="barrioID" class="uk-width-1-1">
                                </select>
                            </div>
                            <div class="uk-form-row">
                                <label class="uk-form-label" for="subbarrioID">Sub Barrio</label>
                                <select id="subbarrioID" name="subbarrioID" class="uk-width-1-1">
                                </select>
                            </div>
                            <div class="uk-form-row">
                                <label class="uk-form-label" for="zipMod">Codigo Postal</label>
                                <input type="text" name="zipMod" id="zipMod" value="<?php echo $usd->zip;?>" placeholder="Codigo Postal" class="uk-width-1-1">
                            </div>
                            <legend>Informacion de Contacto</legend>
                            <div class="uk-form-row">
                                <label class="uk-form-label" for="phoneMod">Telefono</label>
                                <input type="text" name="phoneMod" id="phoneMod" value="<?php echo $usd->phone;?>" placeholder="Telefono" class="uk-width-1-1">
                            </div>
                            <div class="uk-form-row">
                                <label class="uk-form-label" for="emailMod">E-Mail</label>
                                <input type="text" name="emailMod" id="emailMod" value="<?php echo $usd->email;?>" placeholder="E-Mail" class="uk-width-1-1">
                            </div>
                            <br>
                            <button type="submit" class="uk-button uk-button-primary">Guardar</button>
                        </form>
                        <?php endforeach ?>
                        <div id="loadingDiv" style="width:100%;float:left;display:none;">
                          <img src="<?php echo base_url();?>assets/img/AjaxLoader.gif"> Guardando Datos....
                        </div>
                        <span class="alert alert-success alert-mail" role="alert" id="contactOk" style="display:none;">Se cambiaron los datos exitosamente</span>
                        <span class="alert alert-danger alert-mail" role="alert" id="contactError" style="display:none;">Se produjo un error</span>
                    </div>
                  </div>
                </div>
              </article>
            </main>
          </div>
        </div>
      </div>
    </div>
    <?php $this->load->view('elements/footer');?>
  </div>
  <?php $this->load->view('assets/scripts');?>
  <script>
  // Provincia, Localidad, Partido Combo
  $("#paisID").change(function() {
      paisID = $('#paisID').val();
      $("#paisID option:selected").each(function() {
          $.post("/location/getProvincia", {
              paisID : paisID
          }, function(data) {          
              $("#provinciaID").html(data);
          });
      });
  });
  $("#provinciaID").change(function() {
      provinciaID = $('#provinciaID').val();
      $("#provinciaID option:selected").each(function() {
          $.post("/location/getPartido", {
              provinciaID : provinciaID
          }, function(data) {          
              $("#partidoID").html(data);
          });
      });
  });
  $("#partidoID").change(function() {
      partidoID = $('#partidoID').val();
      $("#partidoID option:selected").each(function() {
          $.post("/location/getLocalidad", {
              partidoID : partidoID
          }, function(data) {          
              $("#localidadID").html(data);
          });
      });
  });
  $("#localidadID").change(function() {
      localidadID = $('#localidadID').val();
      $("#localidadID option:selected").each(function() {
          $.post("/location/getBarrio", {
              localidadID : localidadID
          }, function(data) {          
              $("#barrioID").html(data);
          });
      });
  });
  $("#barrioID").change(function() {
      barrioID = $('#barrioID').val();
      $("#barrioID option:selected").each(function() {
          $.post("/location/getSubbarrio", {
              barrioID : barrioID
          }, function(data) {          
              $("#subbarrioID").html(data);
          });
      });
  });
  // Provincia, Localidad, Partido Combo End
  // Industrial Subcategory
  $("#indcatID").change(function() {
      indcatID = $('#indcatID').val();
      $("#indcatID option:selected").each(function() {
          $.post("/industrial/getIndSubcategoryForDd", {
              indcatID : indcatID
          }, function(data) {          
              $("#indsubcatID").html(data);
          });
      });
  });
  // Industrial Subcategory End
  </script>
</body>
</html>