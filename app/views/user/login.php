<!DOCTYPE html>
<html lang="<?php echo $language;?>">
<head>
  <?php $this->load->view('assets/headnfo');?>
  <?php $this->load->view('assets/style');?>
</head>
<body id="tm-container">
  <div class="tm-container">
    <?php $this->load->view('elements/component');?>
    <?php $this->load->view('elements/navbar');?>
    <?php $this->load->view('elements/spotlight-internal');?>
    <?php $this->load->view('elements/breadcrum-gen');?>
    <div id="tm-main" class="tm-block-main uk-block uk-block-default">
      <div class="uk-container uk-container-center">
        <div class="tm-middle uk-grid" data-uk-grid-match data-uk-grid-margin>
          <div class="tm-main uk-width-medium-7-10">
            <main id="tm-content" class="tm-content">
              <article class="uk-article tm-article">
                <div class="tm-article-wrapper">
                  <div class="tm-article-content uk-margin-large-bottom uk-margin-top-remove">
                    <div class="tm-article">
                      <h1 class="uk-article-title">Iniciar Sesion</h1>
                      <?php if ($loginFail == TRUE): ?>
                          <span class="alert alert-danger alert-mail" id="contactError">Usuario y/o Contraseña incorrecta</span>
                      <?php endif ?>
                      <form id="loginForm" action="<?php echo base_url();?>User/logUser" method="post" accept-charset="utf-8">
                          <div class="form-group">
                              <input type="text" name="logmail" id="logmail" value="" placeholder="E-Mail" class="form-control">
                          </div>
                          <div class="form-group">
                              <input type="password" name="logpass" id="logpass" value="" placeholder="Contraseña" class="form-control">
                          </div>
                          <div class="form-group">
                              <label class="checkbox checkbox-blue" for="logrem">
                              <input type="checkbox" name="logrem" id="logrem" data-toggle="checkbox">
                              Mantener Sesion
                              </label>
                          </div>
                          <div class="form-group">
                              <a href="">¿Olvido su Contraseña?</a>
                          </div>
                          <button type="submit" class="btn btn-info btn-fill btn-block">Iniciar Sesion</button>
                      </form>
                      <div id="loadingDiv" style="width:100%;float:left;display:none;">
                          <img src="<?php echo base_url();?>assets/img/AjaxLoader.gif"> Iniciando Sesion....
                      </div>
                      <span class="alert alert-success alert-mail" role="alert" id="contactOk" style="display:none;">Inicio sesion exitosamente</span>
                      <span class="alert alert-danger alert-mail" role="alert" id="contactError" style="display:none;">Se produjo un error</span>
                    </div>
                  </div>
                </div>
              </article>
            </main>
          </div>
        </div>
      </div>
    </div>
    <?php $this->load->view('elements/footer');?>
  </div>
  <?php $this->load->view('assets/scripts');?>
</body>
</html>