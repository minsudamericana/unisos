<!doctype html>
<html lang="<?php echo $language;?>">
<head>
    <?php $this->load->view('assets/headnfo');?>
    <?php $this->load->view('assets/style');?>
</head>
<body class="home">
<?php $this->load->view('elements/navbar');?>
<div class="wrapper">
    <?php $this->load->view('elements/big-card');?>
    
    <div class="section">
           <div class="container">
               <h2 class="section-title">Encontra lo que buscas</h2>
               <div class="row">


                    <div class="col-md-3">
                        <div class="card card-refine card-plain">
                            <div class="header">
                                <h4 class="title">Filtrar
                                    <button class="btn btn-default btn-xs btn pull-right btn-simple" rel="tooltip" title="Reset Filter">
                                        <i class="fa fa-refresh"></i>
                                    </button>
                                 </h4>
                            </div>
                            <div class="content">
                                  <div class="panel-group" id="accordion">

                                  <div class="panel panel-default">
                                    <div class="panel-heading">
                                      <h6 class="panel-title">
                                        <a data-toggle="collapse" href="#refinePrice">
                                          Precio
                                          <i class="fa fa-caret-up pull-right"></i>
                                        </a>
                                      </h6>
                                    </div>
                                    <div id="refinePrice" class="panel-collapse collapse in">
                                      <div class="panel-body">
                                         <span class="price price-left"><?php echo getStoreConfiguration()['currency'];?> 100</span>
                                         <span class="price price-right"><?php echo getStoreConfiguration()['currency'];?> 850</span>
                                         <div class="clearfix"></div>
                                         <div id="refine-price-range" class="slider slider-info"></div>
                                      </div>
                                    </div>
                                  </div>

                                  <div class="panel panel-default">
                                    <div class="panel-heading">
                                      <h6 class="panel-title">
                                        <a data-toggle="collapse" href="#refineClothing" class="collapsed">
                                          Segmento
                                          <i class="fa fa-caret-up pull-right"></i>
                                        </a>
                                      </h6>
                                    </div>
                                    <div id="refineClothing" class="panel-collapse collapse in">
                                      <div class="panel-body">
                                         <label class="checkbox">
                                            <input type="checkbox" value="all" data-toggle="checkbox" checked="">
                                            Todas
                                          </label>
                                          <?php foreach ($segments->result() as $sg): ?>
                                            <label class="checkbox">
                                              <input type="checkbox" value="<?php echo $sg->prod_segment_id; ?>" data-toggle="checkbox">
                                              <?php echo $sg->title; ?>
                                            </label>
                                          <?php endforeach ?>
                                      </div>
                                    </div>
                                  </div>


                                   <div class="panel panel-default">
                                    <div class="panel-heading">
                                      <h6 class="panel-title">
                                        <a data-toggle="collapse" href="#refineDesigner">
                                          Marca
                                          <i class="fa fa-caret-up pull-right"></i>
                                        </a>
                                      </h6>
                                    </div>
                                    <div id="refineDesigner" class="panel-collapse collapse in">
                                      <div class="panel-body panel-scroll">
                                         <label class="checkbox">
                                            <input type="checkbox" value="all" data-toggle="checkbox" checked="">
                                            Todas
                                          </label>
                                          <?php foreach ($brands->result() as $bd): ?>
                                            <label class="checkbox">
                                              <input type="checkbox" value="<?php echo $bd->prod_brand_id; ?>" data-toggle="checkbox">
                                              <?php echo $bd->title; ?>
                                            </label>
                                          <?php endforeach ?>
                                      </div>
                                    </div>
                                  </div>

                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-9">
                        <div class="row">
                          
                          <?php foreach ($products->result() as $prod): ?>
                            <div class="col-md-4" data-brand="<?php echo $prod->brand_id; ?>" data-price="<?php echo $prod->price; ?>" data-segment="">
                              <div class="card card-product card-plain">
                                <div class="image">
                                  <a href="<?php echo base_url() . 'product/' . $prod->slug; ?>">
                                    <img src="<?php echo base_url() . 'assets/uploads/files/products/' . $prod->pic; ?>" alt="<?php echo $prod->name; ?>" style="float: left; width: 100%;"/>
                                  </a>
                                </div>
                                <div class="content">
                                  <a href="<?php echo base_url() . 'product/' . $prod->slug; ?>">
                                    <h4 class="title"><?php echo $prod->name; ?></h4>
                                  </a>
                                  <small>
                                    <?php foreach (getProductSegment($prod->prod_id) as $num => $seg): ?>
                                      <?php echo $seg; ?> 
                                    <?php endforeach ?>
                                  </small>
                                  <br>
                                  <small><?php echo getBrandNfo($prod->brand_id)['title'];?></small>
                                  <p class="description">
                                    <?php echo $prod->short_desc; ?>
                                  </p>
                                  <div class="footer">
                                    <span class="price"><?php echo getStoreConfiguration()['currency'];?>  <?php echo $prod->price; ?></span>
                                      <button class="btn btn-danger btn-simple pull-right" rel="tooltip" title="Remove from wishlist" data-placement="left">
                                      <i class="fa fa-heart"></i>
                                    </button>
                                  </div>  
                                </div>
                              </div>
                            </div>
                          <?php endforeach ?>


                          <div class="col-md-3 col-md-offset-4">
                          <button rel="tooltip" title="This is a morphing button" class="btn btn-default btn-round" id="successBtn" data-toggle="morphing" data-rotation-color="gray">Load more...</button>
                          </div>

                        </div>
                    </div>
               </div>
           </div>
    </div><!-- section -->

    <div class="space-50"></div>
    <?php $this->load->view('elements/footer');?>
</div>
</body>
  <?php $this->load->view('assets/scripts');?>
</html>