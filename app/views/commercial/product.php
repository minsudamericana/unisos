<!doctype html>
<html lang="<?php echo $language;?>">
<head>
    <?php $this->load->view('assets/headnfo');?>
    <?php $this->load->view('assets/style');?>
</head>
<body class="home">

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v2.6&appId=885482321490748";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<?php $this->load->view('elements/navbar');?>
<div class="wrapper">
    
    <?php $bla = 0; ?>
    <?php foreach ($product->result() as $prod): ?>
      <div class="container-fluid breacrum-container">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <ol class="breadcrumb">
                <li><a href="<?php echo base_url(); ?>">Home</a></li>
                <li><?php echo getCatLinkBreadcrum($prod->cat_id); ?></li>
                <li class="active"><?php echo $prod->name;?></li>
              </ol>
            </div>
          </div>
        </div>
      </div>
      <div class="section">
          <div class="container">
            <div class="row">
              <div class="col-md-8">
                <div class="row">
                  <div class="col-md-3">          
                    <ul class="nav nav-text picsDistList" role="tablist" id="flexiselDemo1">
                      <li class="active">
                        <a href="#product-page1" role="tab" data-toggle="tab" aria-expanded="false">
                          <img src="<?php echo base_url() . 'assets/uploads/files/products/' . $prod->pic; ?>" alt="<?php echo $prod->name; ?>"/>
                        </a>
                      </li>
                      <?php if ($prod->pic2 != null): ?>
                      <li class="">
                        <a href="#product-page2" role="tab" data-toggle="tab" aria-expanded="false">
                          <img src="<?php echo base_url() . 'assets/uploads/files/products/' . $prod->pic2; ?>" alt="<?php echo $prod->name; ?>"/>
                        </a>
                      </li>
                      <?php endif ?>
                      <?php if ($prod->pic3 != null): ?>
                      <li class="">
                        <a href="#product-page3" role="tab" data-toggle="tab" aria-expanded="false">
                          <img src="<?php echo base_url() . 'assets/uploads/files/products/' . $prod->pic3; ?>" alt="<?php echo $prod->name; ?>"/>
                        </a>
                      </li>
                      <?php endif ?>
                    </ul>
                  </div>
                  <div class="col-md-9">
                    <div class="tab-content picDistActual">
                      <div class="tab-pane active" id="product-page1">
                        <img src="<?php echo base_url() . 'assets/uploads/files/products/' . $prod->pic; ?>" alt="<?php echo $prod->name; ?>" style="float: left; width: 100%;"/>
                      </div>
                      <?php if ($prod->pic2 != null): ?>
                      <div class="tab-pane" id="product-page2">
                        <img src="<?php echo base_url() . 'assets/uploads/files/products/' . $prod->pic2; ?>" alt="<?php echo $prod->name; ?>" style="float: left; width: 100%;"/>
                      </div>
                      <?php endif ?>
                      <?php if ($prod->pic3 != null): ?>
                      <div class="tab-pane" id="product-page3">
                        <img src="<?php echo base_url() . 'assets/uploads/files/products/' . $prod->pic3; ?>" alt="<?php echo $prod->name; ?>" style="float: left; width: 100%;"/>
                      </div>
                      <?php endif ?>
                    </div>
                  </div>
                </div>
                <div class="clearfix"></div>
              </div>
              <div class="col-md-4">
                  <div class="product-details">
                    <h3 class="title"><?php echo $prod->name; ?></h3>
                    <small class="recommend">
                      <?php foreach (getProductSegment($prod->prod_id) as $num => $seg): ?>
                        <img class="segmentIcon" src="<?php echo base_url() . 'assets/uploads/files/icons/' . $seg; ?>" alt="">
                      <?php endforeach ?>
                    </small>
                    <p class="description">
                      <?php echo $prod->short_desc; ?>
                    </p>
                    <span class="price"><?php echo getStoreConfiguration()['currency'];?> <?php echo $prod->price; ?></span>
                  </div>
                  <button type="button" data-prod="<?php echo $prod->prod_id; ?>" class="btn btn-fill addToCart kale-btn-norma">Agregar al Carrito</button>
                  <button type="button" data-toggle="modal" data-target="#addToList" data-prod="<?php echo $prod->prod_id; ?>" class="btn btn-fill addToList kale-btn-norma">Agrega a tu Lista</button>
                  <?php $this->load->view('elements/social-share');?>
              </div>

            </div>

            <div class="row">
              <div class="col-md-4">
                  <div class="product-details">
                    <h3 class="title">Descripcion del Producto</h3>
                    <p class="description">
                      <?php echo $prod->desc; ?>
                    </p>
                    <h3 class="title">Ingredientes</h3>
                    <p class="description">
                      <?php echo $prod->ingredients; ?>
                    </p>
                  </div>
              </div>
              
              <div class="col-md-8">
                  <div class="product-details reclist">
                    <h3 class="title">Recomendaciones</h3>
                    <div class="row">
                      <?php foreach ($productDest->result() as $prodDs): ?>                
                        <div class="col-md-4 highlight-dest">
                          <a href="<?php echo base_url() . 'product/' . $prodDs->slug; ?>">
                            <img src="<?php echo base_url() . 'assets/uploads/files/products/' . $prodDs->pic; ?>" alt="<?php echo $prodDs->name; ?>"/>
                            <h3 class="title"><?php echo $prodDs->name; ?></h3>
                            <small class="recommend">
                              <?php foreach (getProductSegment($prodDs->prod_id) as $num => $seg): ?>
                                <img class="segmentIcon" src="<?php echo base_url() . 'assets/uploads/files/icons/' . $seg; ?>" alt="">
                              <?php endforeach ?>
                            </small>
                            <p class="description">
                              <?php echo $prodDs->short_desc; ?>
                            </p>
                            <span class="price"><?php echo getStoreConfiguration()['currency'];?> <?php echo $prodDs->price; ?></span>
                          </a>
                        </div>
                      <?php endforeach ?>
                    </div>
                  </div>
              </div>
            </div>
            
            <?php
              $urlTarget = current_url();
              $numberPost = 5;
            ?>
            <div class="section">
              <h3 class="title">Comentarios</h3>
              <div class="row">
                <div class="col-md-12">
                  <div class="media-area">
                    <div class="fb-comments" data-href="<?php echo $urlTarget;?>" data-numposts="<?php echo $numberPost;?>"></div>
                  </div>
                </div>
              </div>
            </div>

          </div>
      </div>
    <?php endforeach; ?>

    <div class="space-50"></div>
    <?php $this->load->view('elements/footer');?>
</div>
</body>
  <?php $this->load->view('assets/scripts');?>
   <script src="<?php echo base_url(); ?>assets/js/cart.js"></script>
</html>