<!doctype html>
<html lang="<?php echo $language;?>">
<head>
    <?php $this->load->view('assets/headnfo');?>
    <?php $this->load->view('assets/style');?>
</head>
<body class="home">
<?php $this->load->view('elements/navbar');?>
<div class="wrapper">
    <div class="parallax">
        <div class="parallax-image">
            <img src="../assets/img/store.jpg" alt="..." />
        </div>
    </div>
      
    <div class="section">
           <div class="container">

               <div class="row">
                    <div class="col-md-3">
                        <div class="card card-refine card-plain">
                            <div class="header">
                                <h4 class="title kaleTitle">Redefinir
                                    <button class="btn btn-default btn-xs btn pull-right btn-simple" rel="tooltip" title="Resetear Filtro">
                                        <i class="fa fa-refresh"></i>
                                    </button>
                                 </h4>
                            </div>
                            <div class="content">
                                  <div class="panel-group" id="accordion">

                                  <div class="panel panel-default">
                                    <div class="panel-heading">
                                      <h6 class="panel-title">
                                        <a class="kaleTitle" data-toggle="collapse" href="#refinePrice">
                                          Precio
                                          <i class="fa fa-caret-up pull-right"></i>
                                        </a>
                                      </h6>
                                    </div>
                                    <div id="refinePrice" class="panel-collapse collapse in">
                                      <div class="panel-body">
                                         <span class="price price-left">
                                          <?php echo getStoreConfiguration()['currency'];?> <?php echo getMinProductPrice(); ?>
                                         </span>
                                         <span class="price price-right">
                                          <?php echo getStoreConfiguration()['currency'];?> <?php echo getMaxProductPrice(); ?>
                                         </span>
                                         <div class="clearfix"></div>
                                         <div id="refine-price-range" class="slider slider-info"></div>
                                      </div>
                                    </div>
                                  </div>

                                  <div class="panel panel-default">
                                    <div class="panel-heading">
                                      <h6 class="panel-title">
                                        <a data-toggle="collapse" href="#refineClothing" class="collapsed kaleTitle">
                                          Segmento
                                          <i class="fa fa-caret-up pull-right"></i>
                                        </a>
                                      </h6>
                                    </div>
                                    <div id="refineClothing" class="panel-collapse collapse in">
                                      <div class="panel-body">
                                         <label class="checkbox">
                                            <input type="checkbox" value="all" data-toggle="checkbox" id="segments_all" checked="">
                                            Todas
                                          </label>
                                          <?php foreach ($segments->result() as $sg): ?>
                                            <label class="checkbox filterSegments">
                                              <input type="checkbox" value="<?php echo $sg->prod_segment_id; ?>" data-toggle="checkbox">
                                              <?php echo $sg->title; ?>
                                            </label>
                                          <?php endforeach ?>
                                      </div>
                                    </div>
                                  </div>


                                  <div class="panel panel-default">
                                    <div class="panel-heading">
                                      <h6 class="panel-title">
                                        <a data-toggle="collapse" href="#refineDesigner" class="kaleTitle">
                                          Marca
                                          <i class="fa fa-caret-up pull-right"></i>
                                        </a>
                                      </h6>
                                    </div>
                                    <div id="refineDesigner" class="panel-collapse collapse in">
                                      <div class="panel-body panel-scroll">
                                         <label class="checkbox">
                                            <input type="checkbox" value="all" data-toggle="checkbox" id="brands_all" checked="">
                                            Todas
                                          </label>
                                          <?php foreach ($brands->result() as $bd): ?>
                                            <label class="checkbox filterBrands"  >
                                              <input type="checkbox" value="<?php echo $bd->prod_brand_id; ?>" data-toggle="checkbox">
                                              <?php echo $bd->title; ?>
                                            </label>
                                          <?php endforeach ?>
                                      </div>
                                    </div>
                                  </div>

                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-9">
                        <div class="row">
                          
                          <?php 
                          $produs = $products->result();
                          foreach ($produs as $prod): ?>
                            <div class="col-md-4 highlight-dest categoryView" data-brand="<?php echo $prod->brand_id; ?>" data-price="<?php echo $prod->price; ?>" data-segment="<?php echo $prod->prod_segment_id ?>">
                              <div class="card card-product card-plain">
                                <div class="image">
                                  <a href="<?php echo base_url() . 'product/' . $prod->slug; ?>">
                                    <img src="<?php echo base_url() . 'assets/uploads/files/products/' . $prod->pic; ?>" alt="<?php echo $prod->name; ?>"/>
                                  </a>
                                </div>
                                <div class="content">
                                  <a href="<?php echo base_url() . 'product/' . $prod->slug; ?>">
                                    <h4 class="title"><?php echo $prod->name ?></h4>
                                  </a>
                                  <small class="segmentContainer">
                                    <?php foreach (getProductSegment($prod->prod_id) as $num => $seg): ?>
                                      <img class="segmentIcon" src="<?php echo base_url() . 'assets/uploads/files/icons/' . $seg; ?>" alt="">
                                    <?php endforeach ?>
                                  </small>
                                  <br>
                                  <small><?php echo getBrandNfo($prod->brand_id)['title'];?></small>
                                  <p class="description">
                                    <?php echo word_limiter($prod->short_desc, 12); ?>
                                  </p>
                                  <div class="footer">
                                    <span class="price"><?php echo getStoreConfiguration()['currency'];?>  <?php echo $prod->price; ?></span>
                                  </div>  
                                </div>
                              </div>
                            </div>
                          <?php endforeach ?>

                        </div>
                    </div>
               </div>

               <div class="row">
                 <div class="col-md-12">
                  <ul class="pagination pagination-no-border paginationKale">
                    <li><a href="#">«</a></li>
                    <li class="active"><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><a href="#">5</a></li>
                    <li><a href="#">»</a></li>
                  </ul>
                 </div>
               </div>
           </div>
    </div><!-- section -->

    <div class="space-50"></div>
    <?php $this->load->view('elements/footer');?>
</div>
</body>
  <?php $this->load->view('assets/scripts');?>

  <script type="text/javascript">

    var brandsSelected = new Array();
    var segmentsSelected = new Array();
    var minPrice = parseInt($('.price-left').text().replace(/[^0-9]/g, ''));
    var maxPrice = parseInt($('.price-right').text().replace(/[^0-9]/g, ''));
    var isPressed = false;

    $('.filterBrands').click(function() {

        var selectedBrand = $(this).children('input');  
        if($.inArray(selectedBrand.val(), brandsSelected ) == -1) {
          brandsSelected.push(selectedBrand.val());          
        } else {
          brandsSelected = $.grep(brandsSelected, function(value) {
            return value != selectedBrand.val();
          });
        }

        if(brandsSelected.length == 0) {
          $('.categoryView').show();
          $('#brands_all').prop('checked', true);
        } else {
          $('#brands_all').prop('checked', false);
          $('.categoryView').each(function() {

            var elmt = $(this);
            if($.inArray(elmt.attr('data-brand'), brandsSelected) == -1){
               elmt.hide();
            } else elmt.show();

          });  
        }        
    });

    $('.filterSegments').click(function() {

        var selectedSegment = $(this).children('input');  
        if($.inArray(selectedSegment.val(), segmentsSelected ) == -1) {
          segmentsSelected.push(selectedSegment.val());          
        } else {
          segmentsSelected = $.grep(segmentsSelected, function(value) {
            return value != selectedSegment.val();
          });
        }

        if(segmentsSelected.length == 0) {
          $('.categoryView').show();
          $('#segments_all').prop('checked', true);
        } else {
          $('#segments_all').prop('checked', false);
          $('.categoryView').each(function() { 

            var elmt = $(this);
            if($.inArray(elmt.attr('data-segment'), segmentsSelected) == -1){
               elmt.hide();
            } else elmt.show();

          });  
        }        
    });


    $('#refine-price-range').click(function() {
      isPressed = true;
      filterPrice(); 
    });

    $('#refine-price-range').mousedown(function(){
      isPressed = true;
      filterPrice() 
    });

    $(document).mouseup(function(){
        if(isPressed){
            filterPrice(); 
            isPressed = false;
        }
    });

    function filterPrice() {
        minPrice = parseInt($('.price-left').text().replace(/[^0-9]/g, ''));
        maxPrice = parseInt($('.price-right').text().replace(/[^0-9]/g, ''));
      
        $('.categoryView').each(function() {
          var elmt = $(this);
          
          if(applyFilter(elmt)) {            
            if(elmt.attr('data-price') <= maxPrice  && elmt.attr('data-price') >= minPrice  ){
              elmt.show();
            } else elmt.hide();            
          } else elmt.hide();  

        });
    }

    function applyFilter(elmt) {
      
      var display = true;
      if(segmentsSelected.length > 0 && $.inArray(elmt.attr('data-segment'), segmentsSelected) == -1){
        display = false;
      } 

      if(brandsSelected > 0 && $.inArray(elmt.attr('data-brand'), brandsSelected) == -1){        
        display = false;
      } 
      return display;
    }


  </script>




</html>