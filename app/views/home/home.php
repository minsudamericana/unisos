<!DOCTYPE html>
<html lang="<?php echo $language;?>">
<head>
  <?php $this->load->view('assets/headnfo');?>
  <?php $this->load->view('assets/style');?>
</head>
<body id="tm-container">

  <div class="tm-container">
    <?php $this->load->view('elements/component');?>

    <?php $this->load->view('elements/navbar');?>
    
    <?php $this->load->view('elements/heroslide');?>

    <?php $this->load->view('elements/companyboxes');?>

    <?php $this->load->view('elements/spotlight');?>

    <?php $this->load->view('elements/service-card');?>
    
    <?php // $this->load->view('elements/commercial-slide');?>

    <?php // $this->load->view('elements/whychoose');?>
    
    <?php // $this->load->view('elements/testimonial-widget');?>

    <?php $this->load->view('elements/about-card');?>
   
    <?php $this->load->view('elements/footer');?>

  </div>
  <?php $this->load->view('assets/scripts');?>
</body>
</html>