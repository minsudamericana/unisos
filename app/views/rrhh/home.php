<!DOCTYPE html>
<html lang="<?php echo $language;?>">
<head>
  <?php $this->load->view('assets/headnfo');?>
  <?php $this->load->view('assets/style');?>
</head>
<body id="tm-container">
  <div class="tm-container">
    <?php $this->load->view('elements/component');?>
    
    <?php $this->load->view('elements/navbar');?>
    
    <?php $this->load->view('elements/spotlight-internal');?>

    <?php $this->load->view('elements/breadcrum-gen');?>

    <div id="tm-main" class="tm-block-main uk-block uk-block-default">
      <div class="uk-container uk-container-center">
        <div class="tm-middle uk-grid" data-uk-grid-match data-uk-grid-margin>
          
          <div class="tm-main uk-width-medium-7-10">
            <main id="tm-content" class="tm-content">
              <article class="uk-article tm-article">
                <div class="tm-article-wrapper">
                  <div class="tm-article-content uk-margin-large-bottom uk-margin-top-remove">
                    <div class="tm-article">
                      <img class="uk-thumbnail-expand" src="<?php echo base_url();?>assets/images/stock/rrhh.jpg" alt="">
                      <hr>
                      <?php if ($this->ion_auth->in_group(3)): ?>
                      <p>
                        Ingrese totalmente Gratis a la Base de Datos de RRHH de Empresas Certificadas.
                      </p>
                      <?php else: ?>
                      <p>
                        Acceda hoy a miles de Profesionales que buscan destacarse del resto.
                      </p>
                      <?php endif ?>

                      <?php if ($this->ion_auth->logged_in()): ?>

                        <?php if ($this->ion_auth->in_group(3)): ?>
                          <?php 
                            $userID = getMyID();
                            $candID = getCandUserByID($userID);
                          ?>
                          <a class="btn btn-info" href="<?php echo base_url() . 'rrhh/editCand/' . $candID;?>">Editar mi CV</a>
                        <?php else: ?>
                        <a class="btn btn-info" href="<?php echo base_url();?>rrhh/directory">Ver Directorio</a>
                        <a class="btn btn-info" href="<?php echo base_url();?>rrhh/mycandidates">Ver Mis Candidatos</a>
                        <?php endif ?>
                      <?php else: ?>
                        <p>
                          Para poder acceder a las siguientes secciones debera estar registrado o haber iniciado sesion.
                          <br>
                          <i>Candidatos / Directorio / Mi CV</i>
                          <br>
                          <a class="btn btn-info" href="<?php echo base_url();?>rrhh/candidate">Registrar mi CV</a>
                        </p>
                      <?php endif ?>
                    </div>
                  </div>
                </div>
              </article>
            </main>
          </div>
          
          <?php  $this->load->view('elements/internal-sidebar');?>

        </div>
      </div>
    </div>
   
    <?php $this->load->view('elements/footer');?>

  </div>
  <?php $this->load->view('assets/scripts');?>
</body>
</html>