<!DOCTYPE html>
<html lang="<?php echo $language;?>">
<head>
  <?php $this->load->view('assets/headnfo');?>
  <?php $this->load->view('assets/style');?>
</head>
<body id="tm-container">
  <div class="tm-container">
    <?php $this->load->view('elements/component');?>
    
    <?php $this->load->view('elements/navbar');?>
    
    <?php $this->load->view('elements/spotlight-internal');?>

    <?php $this->load->view('elements/breadcrum-gen');?>

    <div id="tm-main" class="tm-block-main uk-block uk-block-default">
      <div class="uk-container uk-container-center">
        <div class="tm-middle uk-grid" data-uk-grid-match data-uk-grid-margin>
          
          <?php  $this->load->view('elements/internal-cand-sidebar');?>
          
          <div class="tm-main uk-width-medium-7-10">
            <main id="tm-content" class="tm-content">
              <article class="uk-article tm-article">
                <div class="tm-article-wrapper">
                  <div class="tm-article-content uk-margin-large-bottom uk-margin-top-remove">
                    <div class="tm-article">

                          
                        <?php foreach ($candidate->result() as $cd): ?>
                        <?php if ($cd->status == 1): ?>
                          <div class="tm-middle uk-grid" data-uk-grid-match data-uk-grid-margin>
                            <div class="tm-main uk-width-medium-10-10" style="margin-top: 0px;">
                              <?php if ($cd->certificated_state == 1): ?>
                                <div class="uk-alert-success uk-alert">Cuenta Verificada por Unisos</div>
                              <?php else: ?>
                                <div class="uk-alert-warning uk-warning" style="margin-bottom:0px; padding-left: 15px;">Cuenta Sin Verificar</div>
                              <?php endif ?>
                              <h2 class="uk-module-title" style="margin-bottom: 0;">
                                <?php echo $cd->lastname; ?>, <?php echo $cd->firstname; ?>
                                <small> | <?php echo $cd->profesion; ?></small>
                              </h2>
                              <small><i>"<?php echo $cd->short_desc; ?>"</i></small>
                            </div>
                            
                            <div class="tm-main uk-width-medium-2-10" style="margin-top: 0px;">
                              <h3 class="uk-module-title" style="margin-bottom: 5px;padding-bottom: 5px;">
                                Fotografia
                              </h3>
                              <?php if ($cd->pic != null): ?>
                                <img class="pic" src="<?php echo base_url();?>assets/uploads/files/rrhh/<?php echo $cd->pic;?>" alt="" style="margin-top:0px;padding: 10px;">
                              <?php else: ?>
                                <img class="pic" src="<?php echo base_url();?>assets/img/avatar.jpg" alt="" style="margin-top:0px;padding: 10px;">
                              <?php endif ?>
                            </div>

                            <div class="tm-main uk-width-medium-3-10" style="margin-top: 0px;">
                              <h3 class="uk-module-title" style="margin-bottom: 5px;padding-bottom: 5px;">
                                Informacion General
                              </h3>
                              <p style="margin-top:0px;line-height: 16px;font-size: 15px;">
                                <?php echo getDocType($cd->doc_type); ?>: <?php echo number_format($cd->doc, 0, 0, '.'); ?> <br>
                                Fecha de Nacimiento: <?php echo $cd->birthdate; ?> <br>
                                Edad: <?php calcAge($cd->birthdate); ?> <br>
                                Salario Requerido: $<?php echo number_format($cd->salary, 0, 0, '.'); ?> <br>
                                Sexo: <?php echo getSex($cd->sex); ?><br>
                                Titulo de Grado: <?php echo getCatNameByID($cd->cat_id); ?>
                              </p>
                            </div>

                            <div class="tm-main uk-width-medium-5-10" style="margin-top: 0px;">
                              <h3 class="uk-module-title" style="margin-bottom: 5px;padding-bottom: 5px;">
                                Informacion de Contacto
                              </h3>
                              <p style="margin-top:0px;line-height: 16px;font-size: 15px;">
                                Telefono: <?php echo $cd->phone; ?><br>
                                E-Mail: <?php echo $cd->mail; ?><br>
                              </p>
                              <a class="uk-button-color uk-button-mini uk-button" href="#modal-c" data-uk-modal target="_self">Contactar</a>
                            </div>

                            <div class="tm-main uk-width-medium-1-3" style="margin-top: 0px;">
                              <h3 class="uk-module-title" style="margin-bottom: 5px;padding-bottom: 5px;">Experiencia</h3>
                              <?php getExpByCandID($cd->rrhh_cand_id);?>
                            </div>

                            <div class="tm-main uk-width-medium-1-3" style="margin-top: 0px;">
                              <h3 class="uk-module-title" style="margin-bottom: 5px;padding-bottom: 5px;">Educacion</h3>
                              <?php getEduByCandID($cd->rrhh_cand_id); ?>
                            </div>

                            <div class="tm-main uk-width-medium-1-3" style="margin-top: 0px;">
                              <h3 class="uk-module-title" style="margin-bottom: 5px;padding-bottom: 5px;">Informacion Extra</h3>
                              <?php getExtraByCandID($cd->rrhh_cand_id); ?>
                            </div>
                            
                            <div class="tm-main uk-width-medium-10-10" style="margin-bottom: 5px;padding-bottom: 5px;">
                              <h3 class="uk-module-title" style="margin-bottom: 0;">Biografia</h3>
                              <p style="margin-top:0px;line-height: 16px;font-size: 15px;">
                                <?php echo $cd->bio; ?>
                              </p>
                            </div>
                          </div>
                        <?php else: ?>
                        <p>El Candidato Seleccionado tiene su Perfil Desactivado</p>
                        <?php endif ?>
                        <?php endforeach ?>
                    
                    </div>
                  </div>
                </div>
              </article>
            </main>
          </div>

        </div>
      </div>
    </div>
   
    <?php $this->load->view('elements/footer');?>

  </div>
  <?php $this->load->view('assets/scripts');?>

  <div id="modal-c" class="uk-modal" aria-hidden="false">
    <div class="uk-modal-dialog uk-panel-box">
      <a class="uk-modal-close uk-close"></a>
      <h3 class="uk-panel-title">Enviar Comentario</h3>
      <form role="form" id="contact_form" class="uk-form uk-form-stacked">
        <div class="uk-grid uk-grid-width-medium-1-1 uk-grid-width-1-1">
          <div class="uk-form-icon">
            <i class="uk-icon-user"></i>
            <input type="text" class="uk-width-10-10" name="contact-name" placeholder=" Nombre" size="30" required="required">
          </div>
          <div class="uk-form-icon uk-grid-margin">
            <i class="uk-icon-envelope"></i>
            <input type="email" class="uk-width-10-10" name="contact-email" placeholder=" E-Mail" size="30" required="required">
          </div>
          <div class="uk-form-icon uk-grid-margin">
            <i class="uk-icon-bookmark"></i>
            <input type="text" class="uk-width-10-10 optional" name="contact-subject" placeholder=" Asunto" size="60">
          </div>
          <div class="uk-grid-margin">
              <textarea class="uk-width-10-10" name="contact-message" cols="30" rows="3" placeholder="Mensaje" required="required"></textarea>
          </div>
          <div class="uk-grid-margin">
            <button id="Submitbtn" class="uk-width-10-10 uk-button uk-button-primary" type="submit">Enviar Mensaje</button>
          </div>
        </div>
      </form>
    </div>
  </div>
  
</body>
</html>