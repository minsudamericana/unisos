<!DOCTYPE html>
<html lang="<?php echo $language;?>">
<head>
  <?php $this->load->view('assets/headnfo');?>
  <?php $this->load->view('assets/style');?>
</head>
<body id="tm-container">
  <div class="tm-container">
    <?php $this->load->view('elements/component');?>
    
    <?php $this->load->view('elements/navbar');?>
    
    <?php $this->load->view('elements/spotlight-internal');?>

    <?php $this->load->view('elements/breadcrum-gen');?>

    <div id="tm-main" class="tm-block-main uk-block uk-block-default">
      <div class="uk-container uk-container-center">
        <div class="tm-middle uk-grid" data-uk-grid-match data-uk-grid-margin>
          
          <?php if ($this->ion_auth->in_group(3)): ?>
          <?php else: ?>
            <?php $this->load->view('elements/internal-cand-sidebar');?>
          <?php endif ?>
          
          <div class="tm-main uk-width-medium-7-10">
            <main id="tm-content" class="tm-content">
              <article class="uk-article tm-article">
                <div class="tm-article-wrapper">
                  <div class="tm-article-content uk-margin-large-bottom uk-margin-top-remove">
                    <div class="tm-article">
                      
                    <?php if ($this->ion_auth->in_group(3)): ?>
                      <div class="tm-article-content uk-margin-large-bottom uk-margin-top-remove">
                        <div class="tm-article">
                          <p>La plataforma de RRHH es solo para las Empresas Registradas.</p>  
                        </div>
                      </div>
                    <?php else: ?>
                      <div class="uk-grid">
                        <?php foreach ($candidates->result() as $cd): ?>
                          <div class="uk-width-medium-1-3">
                            <?php if ($cd->certificated_state == 1): ?>
                              <div class="uk-alert-success uk-alert" style="margin-bottom:0px;">Verificada por Unisos</div>
                            <?php else: ?>
                              <div class="uk-alert-warning uk-warning" style="margin-bottom:0px; padding-left: 15px;">Sin Verificar</div>
                            <?php endif ?>
                            <div class="uk-panel uk-panel-box">
                                <?php if ($cd->pic != null): ?>
                                  <img id="imageProfile" class="pic" src="<?php echo base_url();?>assets/uploads/files/rrhh/<?php echo $cd->pic;?>" alt="">
                                <?php else: ?>
                                  <?php $imageURL = base_url() . 'assets/img/avatar.jpg';?>
                                  <img id="imageProfile" class="pic" src="<?php echo base_url();?>assets/img/avatar.jpg" alt="">
                                <?php endif ?>
                                <h3 class="uk-panel-title" style="margin-bottom:0px; padding-bottom:0px;">
                                  <?php echo $cd->firstname; ?> <?php echo $cd->lastname; ?> <br>
                                </h3>
                                <h3 class="uk-panel-title" style="margin-bottom: 5px;padding-bottom:0px;line-height: 0;margin-top: -5px;"><small><?php echo $cd->profesion; ?></small></h3>
                                <p style="margin-top:0px;line-height: 16px;font-size: 15px;">
                                  <?php echo $cd->short_desc; ?> <br>
                                  <?php calcAge($cd->birthdate); ?> años <br>
                                  $ <?php echo number_format($cd->salary, 0, 0, '.'); ?>
                                </p>
                                <a class="uk-button-color uk-button-mini uk-button" href="#" target="_self">Guardar Perfil</a>
                                <a class="uk-button-color uk-button-mini uk-button" href="<?php echo base_url();?>rrhh/directory/candidate/<?php echo $cd->rrhh_cand_id;?>" target="_self">Ver Perfil</a>
                            </div>
                          </div>
                        <?php endforeach ?>
                      </div>
                    <?php endif ?>
                    
                    </div>
                  </div>
                </div>
              </article>
            </main>
          </div>

        </div>
      </div>
    </div>
   
    <?php $this->load->view('elements/footer');?>
  </div>
  <?php $this->load->view('assets/scripts');?>

</body>
</html>