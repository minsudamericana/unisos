<!DOCTYPE html>
<html lang="<?php echo $language;?>">
<head>
  <?php $this->load->view('assets/headnfo');?>
  <?php $this->load->view('assets/style');?>
</head>
<body id="tm-container">
  <div class="tm-container">
    <?php $this->load->view('elements/component');?>
    
    <?php $this->load->view('elements/navbar');?>
    
    <?php $this->load->view('elements/spotlight-internal');?>

    <?php $this->load->view('elements/breadcrum-gen');?>

    <div id="tm-main" class="tm-block-main uk-block uk-block-default">
      <div class="uk-container uk-container-center">
        <div class="tm-middle uk-grid" data-uk-grid-match data-uk-grid-margin>
          
          <div class="tm-main uk-width-medium-7-10">
            <main id="tm-content" class="tm-content">
              <article class="uk-article tm-article">
                <div class="tm-article-wrapper">
                  <div class="tm-article-content uk-margin-large-bottom uk-margin-top-remove">
                    <div class="tm-article">
                      
                      <p>
                        Para verificar la cuenta, por favor envie un E-Mail a cuentas@unisos.com.ar y en un plazo de 48 hs. sera contactado por nuestro personal, muchas gracias.
                        <br>
                        <small>
                          <i>
                            Para verificar la cuenta, usted debera tener al menos un 90% de los formularios llenos.
                          </i>
                        </small>
                      </p>

                      <ul class="uk-tab uk-tab-grid uk-tab-top" data-uk-tab="{connect:'#edit_my_cand', animation: 'fade'}">
                        <li class="uk-active"><a href="#">Informacion General</a></li>
                        <li><a href="#">Fotografia</a></li>
                        <li><a href="#">Educacion</a></li>
                        <li><a href="#">Experiencia</a></li>
                        <li><a href="#">Informacion Extra</a></li>
                      </ul>

                      <ul id="edit_my_cand" class="uk-switcher uk-margin uk-tab-content" style="padding: 13px!important;">
                        <li>
                          
                          <?php foreach ($candInfo->result() as $cdNfo): ?>
                            <div class="tm-article">
                              <form class="uk-form uk-form-stacked" id="editCandGen" name="editCandGen" action="<?php echo base_url();?>Rrhh/saveCandidateValues" method="post" accept-charset="utf-8" enctype="multipart/form-data">
                                <input type="hidden" name="candID" id="candID" value="<?php echo $cdNfo->rrhh_cand_id; ?>">
                                <?php $candID = $cdNfo->rrhh_cand_id;?>
                                <legend>Datos Basicos</legend>
                                <div class="uk-form-row">
                                    <label for="firstname" class="uk-form-label">Nombre</label>
                                    <input type="text"  name="firstname" id="firstname" placeholder="Nombre" class="uk-width-1-1" value="<?php echo $cdNfo->firstname; ?>">
                                </div>
                                <div class="uk-form-row">
                                    <label for="lastname" class="uk-form-label">Apellido</label>
                                    <input type="text"  name="lastname" id="lastname" placeholder="Apellido" class="uk-width-1-1" value="<?php echo $cdNfo->lastname; ?>">
                                </div>
                                <div class="uk-form-row">
                                    <label for="birthdate" class="uk-form-label">Fecha de Nacimiento</label>
                                    <input type="text"  name="birthdate" id="birthdate" placeholder="Fecha de Nacimiento" class="uk-width-1-1 datepicker" value="<?php echo $cdNfo->birthdate; ?>">
                                    <small>*Formato de Fecha: dd/mm/aaaa</small>
                                </div>
                                <div class="uk-form-row">
                                    <label for="doc" class="uk-form-label">Documento</label>
                                    <input type="text"  name="doc" id="doc" placeholder="Numero de Documento" class="uk-width-1-1" value="<?php echo $cdNfo->doc; ?>">
                                </div>
                                <div class="uk-form-row">
                                    <label for="doc_type" class="uk-form-label">Tipo de Documento</label>
                                    <select name="docType" id="docType">
                                      <?php if ($cdNfo->doc_type == null): ?>
                                        <option value="" selected>Seleccione</option>
                                        <option value="1">D.N.I.</option>
                                        <option value="2">Pasaporte</option>
                                        <option value="3">D.U.</option>
                                      <?php elseif($cdNfo->doc_type == 1): ?>
                                        <option value="">Seleccione</option>
                                        <option value="1" selected>D.N.I.</option>
                                        <option value="2">Pasaporte</option>
                                        <option value="3">D.U.</option>
                                      <?php elseif($cdNfo->doc_type == 2): ?>
                                        <option value="">Seleccione</option>
                                        <option value="1">D.N.I.</option>
                                        <option value="2" selected>Pasaporte</option>
                                        <option value="3">D.U.</option>
                                      <?php elseif($cdNfo->doc_type == 3): ?>
                                        <option value="">Seleccione</option>
                                        <option value="1">D.N.I.</option>
                                        <option value="2">Pasaporte</option>
                                        <option value="3" selected>D.U.</option>
                                      <?php endif ?>
                                    </select>
                                </div>
                                <div class="uk-form-row">
                                    <label for="sex" class="uk-form-label">Genero</label>
                                    <select name="sex" id="sex">
                                      <?php if ($cdNfo->sex == null): ?>
                                        <option value="" selected>Seleccione</option>
                                        <option value="1">Masculino</option>
                                        <option value="2">Femenino</option>
                                      <?php elseif($cdNfo->sex == 1): ?>
                                        <option value="">Seleccione</option>
                                        <option value="1" selected>Masculino</option>
                                        <option value="2">Femenino</option>
                                      <?php elseif($cdNfo->sex == 2): ?>
                                        <option value="">Seleccione</option>
                                        <option value="1">Masculino</option>
                                        <option value="2" selected>Femenino</option>
                                      <?php endif ?>
                                    </select>
                                </div>

                                <div class="uk-form-row">
                                    <label for="sex" class="uk-form-label">Categoria de Profesion</label>
                                    <select name="rhcat" id="rhcat" class="uk-width-4-10">
                                      <option value="">Categorias</option>
                                      <?php foreach ($categories->result() as $rhcat): ?>
                                        <option value="<?php echo $rhcat->rrhh_cat_id; ?>"><?php echo $rhcat->title; ?></option>  
                                      <?php endforeach ?>
                                    </select>
                                </div>

                                <div class="uk-form-row">
                                    <label for="short_desc" class="uk-form-label">Descripcion Corta</label>
                                    <input type="text"  name="short_desc" id="short_desc" placeholder="Descripcion Corta" class="uk-width-1-1" value="<?php echo $cdNfo->short_desc; ?>">
                                </div>
                                <div class="uk-form-row">
                                    <label for="bio" class="uk-form-label">Biografia</label>
                                    <textarea name="bio" class="bodyFichaCand" id="bio" cols="30" rows="10" placeholder="Biografia" style="width:100%;"><?php echo $cdNfo->bio; ?></textarea>
                                </div>
                                <div class="uk-form-row">
                                    <label for="profesion" class="uk-form-label">Profesion</label>
                                    <input type="text"  name="profesion" id="profesion" placeholder="Profesion" class="uk-width-1-1" value="<?php echo $cdNfo->profesion; ?>">
                                </div>
                                <div class="uk-form-row">
                                    <label for="phone" class="uk-form-label">Telefono</label>
                                    <input type="text"  name="phone" id="phone" placeholder="Telefono" class="uk-width-1-1" value="<?php echo $cdNfo->phone; ?>">
                                </div>
                                <div class="uk-form-row">
                                    <label for="mail" class="uk-form-label">E-Mail</label>
                                    <input type="text"  name="mail" id="mail" placeholder="E-Mail" class="uk-width-1-1" value="<?php echo $cdNfo->mail; ?>">
                                </div>
                                <div class="uk-form-row">
                                    <label for="salary" class="uk-form-label">Salario</label>
                                    <input type="text"  name="salary" id="salary" placeholder="Salario Requerido" class="uk-width-1-1" value="<?php echo $cdNfo->salary; ?>">
                                </div>
                                <div class="uk-form-row">
                                    <label for="status" class="uk-form-label">Estado de Cuenta</label>
                                    <select name="status" id="status">
                                      <?php if ($cdNfo->status == 0): ?>
                                      <option value="0" selected>Desactivada</option>
                                      <option value="1">Activada</option>
                                      <?php else: ?>
                                      <option value="0">Desactivada</option>
                                      <option value="1" selected>Activada</option>
                                      <?php endif ?>
                                    </select>
                                </div>
                                <br>
                                <input type="submit" id="Submitbtn" class="uk-button uk-button-primary" value="Actualizar Cambios">
                              </form>
                            </div>
                          <?php endforeach ?>
                          
                        </li>
                        <li>
                          <?php foreach ($candInfo->result() as $cnP): ?>
                          
                          <form class="uk-form uk-form-stacked" id="editCandLogo" name="editCandLogo" action="<?php echo base_url();?>rrhh/saveCandPic" method="post" accept-charset="utf-8" enctype="multipart/form-data">
                            <input type="hidden" name="candID" id="candID" value="<?php echo $cnP->rrhh_cand_id; ?>">
                            <legend>Fotografia</legend>
                            <div class="uk-form-row">
                                <div class="uk-width-6-10" style="float:left;">
                                  <label for="pic" class="uk-form-label">Fotografia*</label>
                                  <input type="file" name="pic" id="pic">
                                </div>
                                <div class="uk-width-3-10" style="float:left;">
                                  <img src="<?php echo base_url() . 'assets/uploads/files/rrhh/' . $cnP->pic; ?>">
                                  <div class="uk-link" id="deleteProfilePic">Borrar Imagen</div>
                                </div>
                            </div>
                            <small>* Foto primer plano de rostro.</small>
                            <br>
                            <input type="submit" id="Submitbtn" class="uk-button uk-button-primary" value="Actualizar Cambios">
                          </form>
                          <?php endforeach ?>
                        </li>
                        <li>
                          <table class="uk-table uk-table-hover uk-table-striped" style="font-size: 12px; line-height: 15px;">
                            <caption>Estudios Cursados</caption>
                            <thead>
                              <tr>
                                <th>Titulo</th>
                                <th>Institucion</th>
                                <th>Tipo</th>
                                <th>Calificacion</th>
                                <th>Materias Completadas</th>
                                <th>Materias Totales</th>
                                <th>Ingreso</th>
                                <th>Egreso</th>
                                <th>Accion</th>
                              </tr>
                            </thead>
                            <tbody>
                              <?php foreach ($getCandStudies->result() as $stds): ?>
                                <tr>
                                  <td><?php echo $stds->title; ?></td>
                                  <td><?php echo $stds->entity; ?></td>
                                  <td><?php echo $stds->type; ?></td>
                                  <td><?php echo $stds->calification; ?></td>
                                  <td><?php echo $stds->courses_comp; ?></td>
                                  <td><?php echo $stds->courses_total; ?></td>
                                  <td><?php echo $stds->start; ?></td>
                                  <td><?php echo $stds->end; ?></td>
                                  <td>
                                    <a class="uk-button-color uk-button-mini uk-button" href="#editStudy-<?php echo $stds->rrhh_edu_id;?>" data-uk-modal target="_self">Editar</a>
                                    <a class="uk-button-color uk-button-mini uk-button" href="<?php echo base_url();?>rrhh/deleteEdu/<?php echo $candID . '-' .  $stds->rrhh_edu_id?>" target="_self">Borrar</a>
                                  </td>
                                </tr>
                              <?php endforeach ?>
                            </tbody>
                          </table>
                          <a class="uk-button-default uk-button" href="#addStudy" data-uk-modal>Agregar Estudios</a>
                        </li>
                        <li>
                          <table class="uk-table uk-table-hover uk-table-striped" style="font-size: 12px; line-height: 15px;">
                            <caption>Antecedentes Laborales</caption>
                            <thead>
                              <tr>
                                <th>Cargo</th>
                                <th>Empresa</th>
                                <th>Ingreso</th>
                                <th>Egreso</th>
                                <th>Accion</th>
                              </tr>
                            </thead>
                            <tbody>
                              <?php foreach ($getCandExp->result() as $expr): ?>
                                <tr>
                                  <td><?php echo $expr->position; ?></td>
                                  <td><?php echo $expr->company; ?></td>
                                  <td><?php echo $expr->start; ?></td>
                                  <td><?php echo $expr->end; ?></td>
                                  <td>
                                    <a class="uk-button-color uk-button-mini uk-button" href="#editExp-<?php echo $expr->rrhh_exp_id;?>" data-uk-modal target="_self">Editar</a>
                                    <a class="uk-button-color uk-button-mini uk-button" href="<?php echo base_url();?>rrhh/deleteExp/<?php echo $candID . '-' .  $expr->rrhh_exp_id?>" target="_self">Borrar</a>
                                  </td>
                                </tr>
                              <?php endforeach ?>
                            </tbody>
                          </table>
                          <a class="uk-button-default uk-button" href="#addExperience" data-uk-modal>Agregar Antecedente Laboral</a>
                        </li>
                        <li>
                          <table class="uk-table uk-table-hover uk-table-striped" style="font-size: 12px; line-height: 15px;">
                            <caption>Informacion Extra</caption>
                            <thead>
                              <tr>
                                <th>Titulo</th>
                                <th>Descripcion</th>
                                <th>Accion</th>
                              </tr>
                            </thead>
                            <tbody>
                              <?php foreach ($getCandExtra->result() as $ext): ?>
                                <tr>
                                  <td><?php echo $ext->title; ?></td>
                                  <td><?php echo $ext->desc; ?></td>
                                  <td>
                                    <a class="uk-button-color uk-button-mini uk-button" href="#editExt-<?php echo $ext->rrhh_extra_id;?>" data-uk-modal target="_self">Editar</a>
                                    <a class="uk-button-color uk-button-mini uk-button" href="<?php echo base_url();?>rrhh/deleteExtra/<?php echo $candID . '-' .  $ext->rrhh_extra_id?>" target="_self">Borrar</a>
                                  </td>
                                </tr>
                              <?php endforeach ?>
                            </tbody>
                          </table>
                          <a class="uk-button-default uk-button" href="#addExtra" data-uk-modal>Agregar Informacion Extra</a>
                        </li>
                      </ul>

                    </div>
                  </div>
                </div>
              </article>
            </main>
          </div>
          
          <?php  $this->load->view('elements/internal-sidebar');?>

        </div>
      </div>
    </div>
   
    <?php $this->load->view('elements/footer');?>

  </div>
  <?php $this->load->view('assets/scripts');?>
  <script src="<?php echo base_url();?>assets/js/tinymce/tinymce.min.js" type="text/javascript"></script>
  <script>
    tinymce.init({
    language: 'es',
    selector: '.bodyFichaCand',
    height: 500,
    plugins: [
      "advlist autolink autosave link image lists charmap print preview hr anchor pagebreak spellchecker",
      "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
      "table contextmenu directionality emoticons template textcolor paste fullpage textcolor colorpicker textpattern"
    ],
    toolbar1: "bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | styleselect formatselect fontselect fontsizeselect",
    toolbar2: "cut copy paste | searchreplace | bullist numlist | outdent indent blockquote | undo redo | link unlink anchor image media code | insertdatetime preview | forecolor backcolor",
    toolbar3: "table | hr removeformat | ltr rtl | spellchecker",
    menubar: false,
    toolbar_items_size: 'small',
  });
  </script>
  <script>
    $('#deleteProfilePic').click(function(){
        $.ajax({
            url: "<?php echo base_url();?>Rrhh/removeCandPic"
        })
        .done(function(data) {
          console.log(data);
        });
    });
  </script>

<div id="addStudy" class="uk-modal">
  <div class="uk-modal-dialog uk-panel-box-default">
    <a href="" class="uk-modal-close uk-close"></a>
    <form class="uk-form uk-form-stacked" id="addCandEdu" name="addCandEdu" action="<?php echo base_url();?>rrhh/saveCandidateEducation" method="post" accept-charset="utf-8" enctype="multipart/form-data">
      <input type="hidden" name="cand_id" id="cand_id" value="<?php echo $candID; ?>">
      <legend>Estudios</legend>
      <div class="uk-form-row">
          <div class="uk-form-row">
              <label for="title" class="uk-form-label">Titulo</label>
              <input type="text"  name="title" id="title" placeholder="Titulo" class="uk-width-1-1" value="">
          </div>
          <div class="uk-form-row">
              <label for="entity" class="uk-form-label">Institucion</label>
              <input type="text"  name="entity" id="entity" placeholder="Institucion" class="uk-width-1-1" value="">
          </div>
          <div class="uk-form-row">
              <label for="type" class="uk-form-label">Tipo</label>
              <select name="type" id="type">
                  <option value="" selected>Seleccione tipo de Estudio</option>
                  <option value="Secundario">Secundario</option>
                  <option value="Terciario">Terciario</option>
                  <option value="Universitario">Universitario</option>
                  <option value="Doctorado">Doctorado</option>
                  <option value="Master">Master</option>
                  <option value="Curso">Curso</option>
              </select>
          </div>
          <div class="uk-form-row">
              <label for="calification" class="uk-form-label">Calificacion</label>
              <input type="text"  name="calification" id="calification" placeholder="Calificacion" class="uk-width-1-1" value="">
          </div>
          <div class="uk-form-row">
              <label for="courses_comp" class="uk-form-label">Materias Completadas</label>
              <input type="text"  name="courses_comp" id="courses_comp" placeholder="Materias Completadas" class="uk-width-1-1" value="">
          </div>
          <div class="uk-form-row">
              <label for="courses_total" class="uk-form-label">Materias Totales</label>
              <input type="text"  name="courses_total" id="courses_total" placeholder="Materias Totales" class="uk-width-1-1" value="">
          </div>
          <div class="uk-form-row">
              <label for="start" class="uk-form-label">Ingreso</label>
              <input type="text"  name="start" id="datepicker" placeholder="Ingreso" class="uk-width-1-1 datepicker" value="">
          </div>
          <div class="uk-form-row">
              <label for="end" class="uk-form-label">Egreso</label>
              <input type="text"  name="end" id="datepicker2" placeholder="Egreso" class="uk-width-1-1 datepicker" value="">
          </div>
      </div>
      <input type="submit" id="Submitbtn" class="uk-button uk-button-primary" value="Guardar">
    </form>
  </div>
</div>

<div id="addExperience" class="uk-modal">
  <div class="uk-modal-dialog uk-panel-box-default">
    <a href="" class="uk-modal-close uk-close"></a>
    <form class="uk-form uk-form-stacked" id="addCandExp" name="addCandExp" action="<?php echo base_url();?>rrhh/saveCandidateExp" method="post" accept-charset="utf-8" enctype="multipart/form-data">
      <input type="hidden" name="cand_id" id="cand_id" value="<?php echo $candID; ?>">
      <legend>Antecedente Laboral</legend>
      <div class="uk-form-row">
          <div class="uk-form-row">
              <label for="position" class="uk-form-label">Cargo</label>
              <input type="text"  name="position" id="position" placeholder="Cargo" class="uk-width-1-1" value="">
          </div>
          <div class="uk-form-row">
              <label for="company" class="uk-form-label">Empresa</label>
              <input type="text"  name="company" id="company" placeholder="Empresa" class="uk-width-1-1" value="">
          </div>
          <div class="uk-form-row">
              <label for="start" class="uk-form-label">Ingreso</label>
              <input type="text"  name="start" id="start" placeholder="Ingreso" class="uk-width-1-1 datepicker" value="">
          </div>
          <div class="uk-form-row">
              <label for="end" class="uk-form-label">Egreso</label>
              <input type="text"  name="end" id="start" placeholder="Egreso" class="uk-width-1-1 datepicker" value="">
          </div>
      </div>
      <input type="submit" id="Submitbtn" class="uk-button uk-button-primary" value="Guardar">
    </form>
  </div>
</div>

<div id="addExtra" class="uk-modal">
  <div class="uk-modal-dialog uk-panel-box-default">
    <a href="" class="uk-modal-close uk-close"></a>
    <form class="uk-form uk-form-stacked" id="addCandExtra" name="addCandExtra" action="<?php echo base_url();?>rrhh/saveCandidateExtra" method="post" accept-charset="utf-8" enctype="multipart/form-data">
      <input type="hidden" name="cand_id" id="cand_id" value="<?php echo $candID; ?>">
      <legend>Informacion Extra</legend>
      <div class="uk-form-row">
          <div class="uk-form-row">
              <label for="title" class="uk-form-label">Titulo</label>
              <input type="text"  name="title" id="title" placeholder="Title" class="uk-width-1-1" value="">
          </div>
          <div class="uk-form-row">
              <label for="desc" class="uk-form-label">Descripcion</label>
              <input type="text"  name="desc" id="desc" placeholder="Descripcion" class="uk-width-1-1" value="">
          </div>
      </div>
      <input type="submit" id="Submitbtn" class="uk-button uk-button-primary" value="Guardar">
    </form>
  </div>
</div>

<?php echo base_url();?>rrhh/deleteEdu/<?php echo $candID . '-' .  $stds->rrhh_edu_id?>

<?php foreach ($getCandStudies->result() as $stds): ?>
  <div id="editStudy-<?php echo $stds->rrhh_edu_id;?>" class="uk-modal">
    <div class="uk-modal-dialog uk-panel-box-default">
      <a href="" class="uk-modal-close uk-close"></a>
      <form class="uk-form uk-form-stacked" id="addCandEdu" name="addCandEdu" action="<?php echo base_url();?>rrhh/editCandidateEducation" method="post" accept-charset="utf-8" enctype="multipart/form-data">
        <input type="hidden" name="rrhh_edu_id" id="rrhh_edu_id" value="<?php echo $stds->rrhh_edu_id;?>">
        <input type="hidden" name="cand_id" id="cand_id" value="<?php echo $stds->cand_id;?>">
        <legend>Estudios</legend>
        <div class="uk-form-row">
            <div class="uk-form-row">
                <label for="title" class="uk-form-label">Titulo</label>
                <input type="text"  name="title" id="title" placeholder="Titulo" class="uk-width-1-1" value="<?php echo $stds->title; ?>">
            </div>
            <div class="uk-form-row">
                <label for="entity" class="uk-form-label">Institucion</label>
                <input type="text"  name="entity" id="entity" placeholder="Institucion" class="uk-width-1-1" value="<?php echo $stds->entity; ?>">
            </div>
            <div class="uk-form-row">
                <label for="type" class="uk-form-label">Tipo</label>
                <select name="type" id="type">
                    <option value="" selected>Seleccione tipo de Estudio</option>
                    <option value="Secundario">Secundario</option>
                    <option value="Terciario">Terciario</option>
                    <option value="Universitario">Universitario</option>
                    <option value="Doctorado">Doctorado</option>
                    <option value="Master">Master</option>
                    <option value="Curso">Curso</option>
                </select>
            </div>
            <div class="uk-form-row">
                <label for="calification" class="uk-form-label">Calificacion</label>
                <input type="text"  name="calification" id="calification" placeholder="Calificacion" class="uk-width-1-1" value="<?php echo $stds->calification; ?>">
            </div>
            <div class="uk-form-row">
                <label for="courses_comp" class="uk-form-label">Materias Completadas</label>
                <input type="text"  name="courses_comp" id="courses_comp" placeholder="Materias Completadas" class="uk-width-1-1" value="<?php echo $stds->courses_comp; ?>">
            </div>
            <div class="uk-form-row">
                <label for="courses_total" class="uk-form-label">Materias Totales</label>
                <input type="text"  name="courses_total" id="courses_total" placeholder="Materias Totales" class="uk-width-1-1" value="<?php echo $stds->courses_total; ?>">
            </div>
            <div class="uk-form-row">
                <label for="start" class="uk-form-label">Ingreso</label>
                <input type="text"  name="start" id="start" placeholder="Ingreso" class="uk-width-1-1 datepicker" value="<?php echo $stds->start; ?>">
            </div>
            <div class="uk-form-row">
                <label for="end" class="uk-form-label">Egreso</label>
                <input type="text"  name="end" id="end" placeholder="Egreso" class="uk-width-1-1 datepicker" value="<?php echo $stds->end; ?>">
            </div>
        </div>
        <input type="submit" id="Submitbtn" class="uk-button uk-button-primary" value="Guardar">
      </form>
    </div>
  </div>
<?php endforeach ?>

<?php foreach ($getCandExp->result() as $exprc): ?>
  <div id="editExp-<?php echo $exprc->rrhh_exp_id;?>" class="uk-modal">
    <div class="uk-modal-dialog uk-panel-box-default">
      <a href="" class="uk-modal-close uk-close"></a>
      <form class="uk-form uk-form-stacked" id="addCandEdu" name="addCandEdu" action="<?php echo base_url();?>rrhh/editCandidateExp" method="post" accept-charset="utf-8" enctype="multipart/form-data">
        <input type="hidden" name="rrhh_exp_id" id="rrhh_exp_id" value="<?php echo $exprc->rrhh_exp_id;?>">
        <input type="hidden" name="cand_id" id="cand_id" value="<?php echo $exprc->cand_id;?>">
        <legend>Antecedente Laboral</legend>
        <div class="uk-form-row">
            <div class="uk-form-row">
                <label for="position" class="uk-form-label">Posicion</label>
                <input type="text"  name="position" id="position" placeholder="Institucion" class="uk-width-1-1" value="<?php echo $exprc->position; ?>">
            </div>
            <div class="uk-form-row">
                <label for="company" class="uk-form-label">Empresa</label>
                <input type="text"  name="company" id="company" placeholder="Calificacion" class="uk-width-1-1" value="<?php echo $exprc->company; ?>">
            </div>
            <div class="uk-form-row">
                <label for="start" class="uk-form-label">Ingreso</label>
                <input type="text"  name="start" id="start" placeholder="Ingreso" class="uk-width-1-1 datepicker" value="<?php echo $exprc->start; ?>">
            </div>
            <div class="uk-form-row">
                <label for="end" class="uk-form-label">Egreso</label>
                <input type="text"  name="end" id="end" placeholder="Egreso" class="uk-width-1-1 datepicker" value="<?php echo $exprc->end; ?>">
            </div>
        </div>
        <input type="submit" id="Submitbtn" class="uk-button uk-button-primary" value="Guardar">
      </form>
    </div>
  </div>
<?php endforeach ?>

<?php foreach ($getCandExtra->result() as $ext): ?>
  <div id="editExt-<?php echo $ext->rrhh_extra_id;?>" class="uk-modal">
    <div class="uk-modal-dialog uk-panel-box-default">
      <a href="" class="uk-modal-close uk-close"></a>
      <form class="uk-form uk-form-stacked" id="addCandEdu" name="addCandEdu" action="<?php echo base_url();?>rrhh/editCandidateExtra" method="post" accept-charset="utf-8" enctype="multipart/form-data">
        <input type="hidden" name="rrhh_extra_id" id="rrhh_extra_id" value="<?php echo $ext->rrhh_extra_id;?>">
        <input type="hidden" name="cand_id" id="cand_id" value="<?php echo $ext->cand_id;?>">
        <legend>Informacion Extra</legend>
        <div class="uk-form-row">
            <div class="uk-form-row">
                <label for="title" class="uk-form-label">Titulo</label>
                <input type="text"  name="title" id="title" placeholder="Title" class="uk-width-1-1" value="<?php echo $ext->title;?>">
            </div>
            <div class="uk-form-row">
                <label for="desc" class="uk-form-label">Descripcion</label>
                <input type="text"  name="desc" id="desc" placeholder="Descripcion" class="uk-width-1-1" value="<?php echo $ext->desc;?>">
            </div>
        </div>
        <input type="submit" id="Submitbtn" class="uk-button uk-button-primary" value="Guardar">
      </form>
    </div>
  </div>
<?php endforeach ?>

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
  $( function() {
    $('.datepicker').datepicker();
  } );
</script>

</body>
</html>

