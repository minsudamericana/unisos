<?php 
  $userID = getMyID();
  $indID = getIndustrialUserByID($userID);
?>
<!DOCTYPE html>
<html lang="<?php echo $language;?>">
<head>
  <?php $this->load->view('assets/headnfo');?>
  <?php $this->load->view('assets/style');?>
</head>
<body id="tm-container">
  <div class="tm-container">
    <?php $this->load->view('elements/component');?>
    
    <?php $this->load->view('elements/navbar');?>
    
    <?php $this->load->view('elements/spotlight-internal');?>

    <?php $this->load->view('elements/breadcrum-gen');?>

    <div id="tm-main" class="tm-block-main uk-block uk-block-default">
      <div class="uk-container uk-container-center">
        <div class="tm-middle uk-grid" data-uk-grid-match data-uk-grid-margin>
          
          <?php  $this->load->view('elements/internal-sidebar');?>
          
          <div class="tm-main uk-width-medium-7-10">
            <main id="tm-content" class="tm-content">
              <article class="uk-article tm-article">
                <div class="tm-article-wrapper">
                  <div class="tm-article-content uk-margin-large-bottom uk-margin-top-remove">
                    <div class="tm-article">
                      
                      <div class="uk-grid">
                          <div class="uk-width-medium-1-1">
                            <h2>Candidatos Seleccionados</h2>
                          </div>
                          <?php getMyCandidates($indID);?>
                      </div>
                    
                    </div>
                  </div>
                </div>
              </article>
            </main>
          </div>

        </div>
      </div>
    </div>
   
    <?php $this->load->view('elements/footer');?>
  </div>
  <?php $this->load->view('assets/scripts');?>
</body>
</html>