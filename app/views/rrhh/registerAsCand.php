<!DOCTYPE html>
<html lang="<?php echo $language;?>">
<head>
  <?php $this->load->view('assets/headnfo');?>
  <?php $this->load->view('assets/style');?>
</head>
<body id="tm-container">
  <div class="tm-container">
    <?php $this->load->view('elements/component');?>
    
    <?php $this->load->view('elements/navbar');?>
    
    <?php $this->load->view('elements/spotlight-internal');?>

    <?php $this->load->view('elements/breadcrum-gen');?>

    <div id="tm-main" class="tm-block-main uk-block uk-block-default">
      <div class="uk-container uk-container-center">
        <div class="tm-middle uk-grid" data-uk-grid-match data-uk-grid-margin>
          
          <div class="tm-main uk-width-medium-7-10">
            <main id="tm-content" class="tm-content">
              <article class="uk-article tm-article">
                <div class="tm-article-wrapper">
                  <div class="tm-article-content uk-margin-large-bottom uk-margin-top-remove">
                    <div class="tm-article">
                      <h3 class="uk-panel-title">Registrarme Como Candidato</h3>
                      <form id="registForm" action="<?php echo base_url();?>Rrhh/regCand" method="post" accept-charset="utf-8" class="uk-form">
                        <div class="uk-form-row">
                          <input class="uk-width-1-1" value="" id="regname" name="name" placeholder="Nombre" type="text">
                        </div>
                        <div class="uk-form-row">
                          <input class="uk-width-1-1" value="" id="reglastname" name="lastname" placeholder="Apellido" type="text">
                        </div>
                        <div class="uk-form-row">
                          <input class="uk-width-1-1" value="" id="regphone" name="phone" placeholder="Telefono" type="text">
                        </div>
                        <div class="uk-form-row">
                          <input class="uk-width-1-1" value="" id="regmail" name="mail" placeholder="E-Mail" type="text">
                        </div>
                        <div class="uk-form-row">
                          <input class="uk-width-1-1" value="" id="regpass" name="pass" placeholder="Contraseña" type="text">
                        </div>
                        <div class="uk-form-row">
                          <input class="uk-width-1-1" value="" id="regrepeatpass" name="rpass" placeholder="Repetir Contraseña" type="text">
                        </div>
                        <input type="hidden" name="paisID" id="paisID" value="null">
                        <input type="hidden" name="provinciaID" id="provinciaID" value="null">
                        <input type="hidden" name="partidoID" id="partidoID" value="null">
                        <input type="hidden" name="localidadID" id="localidadID" value="null">
                        <input type="hidden" name="barrioID" id="barrioID" value="null">
                        <input type="hidden" name="subbarrioID" id="subbarrioID" value="null">
                        <div class="uk-form-row">
                          <button class="uk-button uk-button-primary" name="Submit" type="submit" value="Log in">Registrarme</button>
                          <div id="loadingDiv" style="width:100%;float:left;display:none;">
                              <img src="<?php echo base_url();?>assets/img/AjaxLoader.gif"> Registrando....
                          </div>
                          <span class="alert alert-success alert-mail" role="alert" id="contactOk" style="display:none;">Se registro con Exito</span>
                          <span class="alert alert-danger alert-mail" role="alert" id="contactError" style="display:none;">Se produjo un error</span>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </article>
            </main>
          </div>
          
          <?php  $this->load->view('elements/internal-sidebar');?>

        </div>
      </div>
    </div>
   
    <?php $this->load->view('elements/footer');?>

  </div>
  <?php $this->load->view('assets/scripts');?>
</body>
</html>