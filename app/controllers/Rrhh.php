<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rrhh extends CI_Controller {
	
	public function __construct(){
		parent::__construct();	
		$this->load->model('Commercial_model');
		$this->load->model('Internal_model');	
		$this->load->model('Location_Model');	
		$this->load->model('Rrhh_model');
	}

	public function viewRrhhHome()
	{
		// Navbar Configuration
		$data['navbarConf'] = 'commercial';
		// Navbar Configuration End
		// Load Renders for Navbar
		$data['menuCat'] = $this->Commercial_model->getCategoryALL();
		$data['menuBrands'] = $this->Commercial_model->getBrandsALL();
		$data['menuInt'] = $this->Internal_model->getInternalALL();
		// Load Renders for Navbar End

		// Render Title and Tags
		$data['title'] = getSiteConfiguration()['site_name'] . ' | RRHH';
		$data['charset'] = getSiteConfiguration()['site_charset'];
		$data['description'] = getSiteConfiguration()['site_desc'];
		$data['keywords'] = getSiteConfiguration()['site_keywords'];
		$data['language'] = getSiteConfiguration()['site_lang'];
		$data['appleicon'] = getSiteConfiguration()['site_appleicon'];
		$data['favicon'] = getSiteConfiguration()['site_favicon'];
		$data['author'] = getSiteConfiguration()['site_author'];

		// Render Blogs
		$data['postLst'] = $this->Internal_model->getAllPostReview();
		// Render Blogs End

		// Render Visualizations
		$data['titleSpot'] = 'Mercado de Candidatos';
		// Render Visualizations End

		// Location Script
		$data['pais'] = $this->Location_Model->getPais();
		// Location Script End

		// Load View
		$this->load->view('rrhh/home', $data);
	}

	public function uploadCV()
	{
		// Navbar Configuration
		$data['navbarConf'] = 'commercial';
		// Navbar Configuration End
		// Load Renders for Navbar
		$data['menuCat'] = $this->Commercial_model->getCategoryALL();
		$data['menuBrands'] = $this->Commercial_model->getBrandsALL();
		$data['menuInt'] = $this->Internal_model->getInternalALL();
		// Load Renders for Navbar End

		// Render Blogs
		$data['postLst'] = $this->Internal_model->getAllPostReview();
		// Render Blogs End

		// Render Title and Tags
		$data['title'] = getSiteConfiguration()['site_name'] . ' | RRHH';
		$data['charset'] = getSiteConfiguration()['site_charset'];
		$data['description'] = getSiteConfiguration()['site_desc'];
		$data['keywords'] = getSiteConfiguration()['site_keywords'];
		$data['language'] = getSiteConfiguration()['site_lang'];
		$data['appleicon'] = getSiteConfiguration()['site_appleicon'];
		$data['favicon'] = getSiteConfiguration()['site_favicon'];
		$data['author'] = getSiteConfiguration()['site_author'];

		// Render Visualizations
		$data['titleSpot'] = 'Cargar CV';
		// Render Visualizations End

		// Location Script
		$data['pais'] = $this->Location_Model->getPais();
		// Location Script End

		// Render Directory Categories
		$data['categories'] = $this->Rrhh_model->getCategoryALL();
		// Render Directory Categories End

		// Load View
		$this->load->view('rrhh/registerAsCand', $data);
	}

	public function viewDirectory()
	{
		// Navbar Configuration
		$data['navbarConf'] = 'commercial';
		// Navbar Configuration End
		// Load Renders for Navbar
		$data['menuCat'] = $this->Commercial_model->getCategoryALL();
		$data['menuBrands'] = $this->Commercial_model->getBrandsALL();
		$data['menuInt'] = $this->Internal_model->getInternalALL();
		// Load Renders for Navbar End

		// Render Blogs
		$data['postLst'] = $this->Internal_model->getAllPostReview();
		// Render Blogs End

		// Render Title and Tags
		$data['title'] = getSiteConfiguration()['site_name'] . ' | RRHH';
		$data['charset'] = getSiteConfiguration()['site_charset'];
		$data['description'] = getSiteConfiguration()['site_desc'];
		$data['keywords'] = getSiteConfiguration()['site_keywords'];
		$data['language'] = getSiteConfiguration()['site_lang'];
		$data['appleicon'] = getSiteConfiguration()['site_appleicon'];
		$data['favicon'] = getSiteConfiguration()['site_favicon'];
		$data['author'] = getSiteConfiguration()['site_author'];

		// Render Visualizations
		$data['titleSpot'] = 'Directorio de Candidatos';
		// Render Visualizations End

		// Location Script
		$data['pais'] = $this->Location_Model->getPais();
		// Location Script End

		// Render Directory Categories
		$data['categories'] = $this->Rrhh_model->getCategoryALL();
		// Render Directory Categories End

		// Load View
		$this->load->view('rrhh/directory', $data);
	}

	public function regCand() {
		$regname = $_POST['name'];
		$reglastname = $_POST['lastname'];
		$regphone = $_POST['phone'];
		$regmail = $_POST['mail'];
		$regpass = $_POST['pass'];
		$paisID = $_POST['paisID'];
		$provinciaID = $_POST['provinciaID'];
		$partidoID = $_POST['partidoID'];
		$localidadID = $_POST['localidadID'];
		$barrioID = $_POST['barrioID'];
		$subbarrioID = $_POST['subbarrioID'];

		$additional_data = array(
			'first_name' => $regname,
			'last_name' => $reglastname,
			'username' => $regmail,
			'pais' => $paisID,
			'provincia' => $provinciaID,
			'partido' => $partidoID,
			'localidad' => $localidadID,
			'barrio' => $barrioID,
			'subbarrio' => $subbarrioID
		);

		$group = array('3');
		
		// Load Renders for Navbar
		$data['menuCat'] = $this->Commercial_model->getCategoryALL();
		$data['menuBrands'] = $this->Commercial_model->getBrandsALL();
		$data['menuInt'] = $this->Internal_model->getInternalALL();
		// Load Renders for Navbar End

		// Render Blogs
		$data['postLst'] = $this->Internal_model->getAllPostReview();
		// Render Blogs End

		// Get Basic Info For Page Meta
		$data['title'] = getSiteConfiguration()['site_name'] . ' | ' . 'Bienvenido';
		$data['charset'] = getSiteConfiguration()['site_charset'];
		$data['description'] = getSiteConfiguration()['site_desc'];
		$data['keywords'] = getSiteConfiguration()['site_keywords'];
		$data['language'] = getSiteConfiguration()['site_lang'];
		$data['appleicon'] = getSiteConfiguration()['site_appleicon'];
		$data['favicon'] = getSiteConfiguration()['site_favicon'];
		$data['author'] = getSiteConfiguration()['site_author'];

		// Location Script
		$data['pais'] = $this->Location_Model->getPais();
		// Location Script End

		// Render Visualizations
		$data['titleSpot'] = 'Bienvenido';
		// Render Visualizations End

		$this->ion_auth->register($regmail, $regpass, $regmail, $additional_data, $group);

		$userID = getLastUserRegister();

		createCand($userID);

		$this->load->view('rrhh/welcome', $data);
	}

	public function editCand($candID)
	{
		$userID = getMyID();
		$myCandID = getCandUserByID($userID);

		if ($candID == $myCandID) {		
			// Navbar Configuration
			$data['navbarConf'] = 'commercial';
			// Navbar Configuration End
			// Load Renders for Navbar
			$data['menuCat'] = $this->Commercial_model->getCategoryALL();
			$data['menuBrands'] = $this->Commercial_model->getBrandsALL();
			$data['menuInt'] = $this->Internal_model->getInternalALL();
			// Load Renders for Navbar End

			// Render Blogs
			$data['postLst'] = $this->Internal_model->getAllPostReview();
			// Render Blogs End

			// Render Title and Tags
			$data['title'] = getSiteConfiguration()['site_name'] . ' | Editar mi Perfil';
			$data['charset'] = getSiteConfiguration()['site_charset'];
			$data['description'] = getSiteConfiguration()['site_desc'];
			$data['keywords'] = getSiteConfiguration()['site_keywords'];
			$data['language'] = getSiteConfiguration()['site_lang'];
			$data['appleicon'] = getSiteConfiguration()['site_appleicon'];
			$data['favicon'] = getSiteConfiguration()['site_favicon'];
			$data['author'] = getSiteConfiguration()['site_author'];

			// Render Visualizations
			$data['titleSpot'] = 'Editar Perfil';
			// Render Visualizations End

			// Location Script
			$data['pais'] = $this->Location_Model->getPais();
			// Location Script End

			// Render Directory Categories
			$data['categories'] = $this->Rrhh_model->getCategoryALL();
			// Render Directory Categories End

			// Render Candidate Info
			$data['candInfo'] = $this->Rrhh_model->getCandidatesByCandID($candID);
			// Render Candidate Info End

			// Render Candidate Extra Information
			$data['getCandStudies'] = $this->Rrhh_model->getCandStudiesByCandID($candID);
			$data['getCandExp'] = $this->Rrhh_model->getCandExpByCandID($candID);
			$data['getCandExtra'] = $this->Rrhh_model->getCandExtraByCandID($candID);
			// Render Candidate Extra Information End

			// Load View
			$this->load->view('rrhh/editcand', $data);
		}else{
			// Error Forbiden Access
			redirect('Home','refresh');
		}
	}

	public function getCategory($slug)
	{
		// Navbar Configuration
		$data['navbarConf'] = 'commercial';
		// Navbar Configuration End
		// Load Renders for Navbar
		$data['menuCat'] = $this->Commercial_model->getCategoryALL();
		$data['menuBrands'] = $this->Commercial_model->getBrandsALL();
		$data['menuInt'] = $this->Internal_model->getInternalALL();
		// Load Renders for Navbar End

		// Render Blogs
		$data['postLst'] = $this->Internal_model->getAllPostReview();
		// Render Blogs End

		// Retrive Nfo from Internal
		$data['category'] = $this->Rrhh_model->getCategoryBySlug($slug);

		// Render Title and Tags
		if ($data['category']->num_rows() > 0) {
			
			foreach ($data['category']->result() as $cat) {
				// Save Category ID
				$catID = $cat->rrhh_cat_id;
				// Get Basic Info For Page Meta
				$data['title'] = getSiteConfiguration()['site_name'] . ' | ' . $cat->title;
			}
			$data['charset'] = getSiteConfiguration()['site_charset'];
			$data['description'] = getSiteConfiguration()['site_desc'];
			$data['keywords'] = getSiteConfiguration()['site_keywords'];
			$data['language'] = getSiteConfiguration()['site_lang'];
			$data['appleicon'] = getSiteConfiguration()['site_appleicon'];
			$data['favicon'] = getSiteConfiguration()['site_favicon'];
			$data['author'] = getSiteConfiguration()['site_author'];

			// Location Script
			$data['pais'] = $this->Location_Model->getPais();
			// Location Script End

			// Render Visualizations
			$data['titleSpot'] = $cat->title;
			// Render Visualizations End

			// Slug for Candidate Category
			$data['slugCat'] = $slug;

			// Get Candidates per Category
			$data['candidates'] = $this->Rrhh_model->getCandidatesByCatID($catID);
			// Get Candidates per Category End

			// Render Directory Categories
			$data['categories'] = $this->Rrhh_model->getCategoryALL();
			// Render Directory Categories End

			// Load View
			$this->load->view('rrhh/category', $data);
		}else{
			// Error 404 Page
			redirect('Error/NotFound', 'refresh');
		}
	}

	public function getCandidate($id)
	{
		// Navbar Configuration
		$data['navbarConf'] = 'commercial';
		// Navbar Configuration End
		// Load Renders for Navbar
		$data['menuCat'] = $this->Commercial_model->getCategoryALL();
		$data['menuBrands'] = $this->Commercial_model->getBrandsALL();
		$data['menuInt'] = $this->Internal_model->getInternalALL();
		// Load Renders for Navbar End

		// Render Blogs
		$data['postLst'] = $this->Internal_model->getAllPostReview();
		// Render Blogs End

		// Retrive Nfo from Internal
		$data['candidate'] = $this->Rrhh_model->getCandidatesByCandID($id);

		// Render Title and Tags
		if ($data['candidate']->num_rows() > 0) {
			
			foreach ($data['candidate']->result() as $cand) {
				// Get Basic Info For Page Meta
				$data['title'] = getSiteConfiguration()['site_name'] . ' | ' . $cand->firstname . ' ' . $cand->lastname;
			}
			$data['charset'] = getSiteConfiguration()['site_charset'];
			$data['description'] = getSiteConfiguration()['site_desc'];
			$data['keywords'] = getSiteConfiguration()['site_keywords'];
			$data['language'] = getSiteConfiguration()['site_lang'];
			$data['appleicon'] = getSiteConfiguration()['site_appleicon'];
			$data['favicon'] = getSiteConfiguration()['site_favicon'];
			$data['author'] = getSiteConfiguration()['site_author'];

			// Location Script
			$data['pais'] = $this->Location_Model->getPais();
			// Location Script End

			// Render Visualizations
			$data['titleSpot'] = $cand->firstname . ' ' . $cand->lastname;
			// Render Visualizations End

			// Render Directory Categories
			$data['categories'] = $this->Rrhh_model->getCategoryALL();
			// Render Directory Categories End

			// Load View
			$this->load->view('rrhh/candidate', $data);
		}else{
			// Error 404 Page
			redirect('Error/NotFound', 'refresh');
		}
	}

	public function mycandidates()
	{
		// Navbar Configuration
		$data['navbarConf'] = 'commercial';
		// Navbar Configuration End
		// Load Renders for Navbar
		$data['menuCat'] = $this->Commercial_model->getCategoryALL();
		$data['menuBrands'] = $this->Commercial_model->getBrandsALL();
		$data['menuInt'] = $this->Internal_model->getInternalALL();
		// Load Renders for Navbar End

		// Render Blogs
		$data['postLst'] = $this->Internal_model->getAllPostReview();
		// Render Blogs End
		
		// Retrive Nfo from Internal


		// Get Basic Info For Page Meta
		$data['title'] = getSiteConfiguration()['site_name'] . ' | Mis Candidatos';
		$data['charset'] = getSiteConfiguration()['site_charset'];
		$data['description'] = getSiteConfiguration()['site_desc'];
		$data['keywords'] = getSiteConfiguration()['site_keywords'];
		$data['language'] = getSiteConfiguration()['site_lang'];
		$data['appleicon'] = getSiteConfiguration()['site_appleicon'];
		$data['favicon'] = getSiteConfiguration()['site_favicon'];
		$data['author'] = getSiteConfiguration()['site_author'];

		// Location Script
		$data['pais'] = $this->Location_Model->getPais();
		// Location Script End

		// Render Visualizations
		$data['titleSpot'] = 'Mis Candidatos';
		// Render Visualizations End

		// Load View
		$this->load->view('rrhh/candidatesList', $data);
	}

	// Save values for Candidate Settings
	public function saveCandidateValues()
	{

		$userID = getMyID();
		$myCandID = getCandUserByID($userID);
		$candID = $_POST['candID'];
		if ($candID == $myCandID) {
			if(isset($_POST['firstname'])){
				$nombre = $_POST['firstname'];
			}else{
			  	$nombre = null;
			}
			if(isset($_POST['lastname'])){
				$apellido = $_POST['lastname'];
			}else{
				$apellido = null;
			}
			if(isset($_POST['birthdate'])){
				$fechaNac = $_POST['birthdate'];
			}else{
				$fechaNac = null;
			}
			if(isset($_POST['doc'])){
				$documento = $_POST['doc'];
			}else{
				$documento = null;
			}
			if(isset($_POST['docType'])){
				$tipoDocumento = $_POST['docType'];
			}else{
				$tipoDocumento = null;
			}
			if(isset($_POST['sex'])){
				$genero = $_POST['sex'];
			}else{
				$genero = null;
			}
			if(isset($_POST['short_desc'])){
				$desCorta = $_POST['short_desc'];
			}else{
				$desCorta = null;
			}
			if(isset($_POST['bio'])){
				$biografia = $_POST['bio'];
			}else{
				$biografia = null;
			}
			if(isset($_POST['profesion'])){
				$profesion = $_POST['profesion'];
			}else{
				$profesion = null;
			}
			if(isset($_POST['phone'])){
				$telefono = $_POST['phone'];
			}else{
				$telefono = null;
			}
			if(isset($_POST['mail'])){
				$mail = $_POST['mail'];
			}else{
				$mail = null;
			}
			if(isset($_POST['salary'])){
				$salario = $_POST['salary'];
			}else{
				$salario = null;
			}
			if(isset($_POST['rhcat'])){
				$rhcat = $_POST['rhcat'];
			}else{
				$rhcat = null;
			}
			if(isset($_POST['status'])){
				$estado = $_POST['status'];
			}else{
				$estado = null;
			}

			$data = array(
				'firstname' => $nombre,
				'lastname' => $apellido,
				'birthdate' => $fechaNac,
				'doc' => $documento,
				'doc_type' => $tipoDocumento,
				'sex' => $genero,
				'short_desc' => $desCorta,
				'bio' => $biografia,
				'profesion' => $profesion,
				'phone' => $telefono,
				'mail' => $mail,
				'cat_id' => $rhcat,
				'salary' => $salario,
				'status' => $estado,
	        );
			$this->db->where('rrhh_cand_id', $candID);
			$this->db->update('rrhh_cand', $data);

			redirect('Rrhh/editCand/' . $candID,'refresh');
		}else{
			// Error Forbiden Access
			redirect('Home','refresh');
		}
	}

	public function saveCandPic()
	{
		$userID = getMyID();
		$myCandidateID = getCandUserByID($userID);
		$candID = $_POST['candID'];

		if ($candID == $myCandidateID) {
			$imgName = do_uploadCand('pic', getMyID() . '_' ,  'files/rrhh/');			
			$uploadPath = base_url() . 'assets/uploads/files/rrhh/';
			$imgName = str_replace(' ','_',$imgName);
			$data = array(
	           'pic' => $imgName
	        );
			$this->db->where('rrhh_cand_id', $candID);
			$this->db->update('rrhh_cand', $data);
			redirect('rrhh/editCand/' . $candID,'refresh');
		}else{
			// Error Forbiden Access
			redirect('Home','refresh');
		}
	}

	public function removeCandPic() {
		remove_fileCand('pic');		
	}

	public function saveCandidateEducation()
	{
		$candID = $_POST['cand_id'];
		$title = $_POST['title'];
		$entity = $_POST['entity'];
		$calification = $_POST['calification'];
		$type = $_POST['type'];
		$coursesCompleted = $_POST['courses_comp'];
		$coursesTotal = $_POST['courses_total'];
		$start = $_POST['start'];
		$end = $_POST['end'];
		$data = array(
			'cand_id' => $candID,
			'title' => $title,
			'entity' => $entity,
			'calification' => $calification,
			'type' => $type,
			'courses_comp' => $coursesCompleted,
			'courses_total' => $coursesTotal,
			'start' => $start,
			'end' => $end,
        );
		$this->db->insert('rrhh_edu', $data); 
		redirect('Rrhh/editCand/' . $candID,'refresh');
	}

	public function editCandidateEducation()
	{
		$candID = $_POST['cand_id'];
		$studyID = $_POST['rrhh_edu_id'];
		$title = $_POST['title'];
		$entity = $_POST['entity'];
		$calification = $_POST['calification'];
		$type = $_POST['type'];
		$coursesCompleted = $_POST['courses_comp'];
		$coursesTotal = $_POST['courses_total'];
		$start = $_POST['start'];
		$end = $_POST['end'];
		$data = array(
			'cand_id' => $candID,
			'title' => $title,
			'entity' => $entity,
			'calification' => $calification,
			'type' => $type,
			'courses_comp' => $coursesCompleted,
			'courses_total' => $coursesTotal,
			'start' => $start,
			'end' => $end
        );

		$this->db->where('rrhh_edu_id', $studyID);
		$this->db->update('rrhh_edu', $data); 

		redirect('Rrhh/editCand/' . $candID,'refresh');
	}

	public function deleteCandidateEducation($candID, $educationID)
	{
		$this->db->where('cand_id', $candID);
		$this->db->where('rrhh_edu_id', $educationID);
		$this->db->delete('rrhh_edu'); 
		redirect('Rrhh/editCand/' . $candID,'refresh');
	}

	public function saveCandidateExp()
	{
		$candID = $_POST['cand_id'];
		$position = $_POST['position'];
		$company = $_POST['company'];
		$start = $_POST['start'];
		$end = $_POST['end'];
		$data = array(
			'cand_id' => $candID,
			'position' => $position,
			'company' => $company,
			'start' => $start,
			'end' => $end,
        );
		$this->db->insert('rrhh_exp', $data); 
		redirect('Rrhh/editCand/' . $candID,'refresh');
	}

	public function editCandidateExp()
	{
		$candID = $_POST['cand_id'];
		$expID = $_POST['rrhh_exp_id'];
		$title = $_POST['title'];
		$position = $_POST['position'];
		$company = $_POST['company'];
		$start = $_POST['start'];
		$end = $_POST['end'];
		$data = array(
			'cand_id' => $candID,
			'position' => $position,
			'company' => $company,
			'start' => $start,
			'end' => $end,
        );

		$this->db->where('rrhh_exp_id', $expID);
		$this->db->update('rrhh_exp', $data); 

		redirect('Rrhh/editCand/' . $candID,'refresh');
	}

	public function deleteCandidateExp($candID, $expID)
	{
		$this->db->where('cand_id', $candID);
		$this->db->where('rrhh_exp_id', $expID);
		$this->db->delete('rrhh_exp'); 
		redirect('Rrhh/editCand/' . $candID,'refresh');
	}

	public function saveCandidateExtra()
	{
		$candID = $_POST['cand_id'];
		$title = $_POST['title'];
		$desc = $_POST['desc'];
		$data = array(
			'title' => $title,
			'desc' => $desc,
        );
		$this->db->insert('rrhh_extra', $data); 
		redirect('Rrhh/editCand/' . $candID,'refresh');
	}

	public function editCandidateExtra()
	{
		$candID = $_POST['cand_id'];
		$extraID = $_POST['rrhh_extra_id'];
		$title = $_POST['title'];
		$desc = $_POST['desc'];
		$data = array(
			'title' => $title,
			'desc' => $desc
        );

		$this->db->where('rrhh_extra_id', $extraID);
		$this->db->update('rrhh_extra', $data); 

		redirect('Rrhh/editCand/' . $candID,'refresh');
	}

	public function deleteCandidateExtra($candID, $educationID)
	{
		$this->db->where('cand_id', $candID);
		$this->db->where('rrhh_extra_id', $educationID);
		$this->db->delete('rrhh_extra'); 
		redirect('Rrhh/editCand/' . $candID,'refresh');
	}

}