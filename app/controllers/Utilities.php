<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Utilities extends CI_Controller {
	
	public function __construct(){
		parent::__construct();	
		$this->load->model('Commercial_model');
		$this->load->model('Internal_model');
		$this->load->model('Location_Model');			
	}

	public function postMailtoList()
	{

		$mailChimpAPI = '4d7ee5bf72b49bede2c6345b8538576a-us14';
		$mailChimpListID = '1cc04a17ff';

	    $this->load->helper('mailchimp');
	    $email = $this->input->post('email');
	    $fname = $this->input->post('fname');

	    $api = new MCAPI($mailChimpAPI);
	    $email_type = 'html';
	    $merge_vars = Array( 
	        'EMAIL' => $email,
	        'FNAME' => $fname, 
	    );
	    if($api->listSubscribe($mailChimpListID, $email, $merge_vars , $email_type) === true) {
	        echo 'Tu Mail se agrego Correctamente.';
	        //redirect('Home',  $data);
	    }else{
	        echo '<b>Error:</b>&nbsp; ' . $api->errorMessage;
	        //redirect('Home', $data);
	    }

	}

}