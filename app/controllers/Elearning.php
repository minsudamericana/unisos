<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Elearning extends CI_Controller {
	
	public function __construct(){
		parent::__construct();	
		$this->load->model('Elearning_model');	
		$this->load->model('Internal_model');	
		$this->load->model('Location_Model');
	}

	public function Home()
	{
		// Navbar Configuration
		$data['navbarConf'] = 'commercial';
		// Navbar Configuration End

		// Load Renders for Navbar
		$data['menuInt'] = $this->Internal_model->getInternalALL();
		// Load Renders for Navbar End

		// Render Blogs
		$data['postLst'] = $this->Internal_model->getAllPostReview();
		// Render Blogs End

		$data['title'] = getSiteConfiguration()['site_name'] . ' | ' . 'Plataforma de E-Learning';
		$data['charset'] = getSiteConfiguration()['site_charset'];
		$data['description'] = getSiteConfiguration()['site_desc'];
		$data['keywords'] = getSiteConfiguration()['site_keywords'];
		$data['language'] = getSiteConfiguration()['site_lang'];
		$data['appleicon'] = getSiteConfiguration()['site_appleicon'];
		$data['favicon'] = getSiteConfiguration()['site_favicon'];
		$data['author'] = getSiteConfiguration()['site_author'];

		$data['titleSpot'] = 'E-Learning';

		// Load View
	 	$this->load->view('elearning/home', $data);
	}

	public function courses()
	{
			// Render Blogs
			$data['postLst'] = $this->Internal_model->getAllPostReview();
			// Render Blogs End

			$data['cursosList'] = $this->Elearning_model->getCoursesList();

			$data['navbarConf'] = 'commercial';
			$data['menuInt'] = $this->Internal_model->getInternalALL();

			$data['title'] = getSiteConfiguration()['site_name'] . ' | ' . 'Lista de Cursos';
			$data['charset'] = getSiteConfiguration()['site_charset'];
			$data['description'] = getSiteConfiguration()['site_desc'];
			$data['keywords'] = getSiteConfiguration()['site_keywords'];
			$data['language'] = getSiteConfiguration()['site_lang'];
			$data['appleicon'] = getSiteConfiguration()['site_appleicon'];
			$data['favicon'] = getSiteConfiguration()['site_favicon'];
			$data['author'] = getSiteConfiguration()['site_author'];

			$data['titleSpot'] = 'Cursos';

			// Load View
		 	$this->load->view('elearning/courses_list', $data);
	}

	public function course($cursoID, $slug)
	{

		if ($this->ion_auth->logged_in()) {

			// Render Blogs
			$data['postLst'] = $this->Internal_model->getAllPostReview();
			// Render Blogs End

			// Get My ID
			$data['myID'] = getMyID();

			// Curse ID
			$data['curseID'] = $cursoID;

			// Render Variable
			$data['cursoNfo'] = $this->Elearning_model->getCourseByID($cursoID, $slug);

			if ($data['cursoNfo']->num_rows() > 0) {

				// Course NFO Loop
				foreach ($data['cursoNfo']->result() as $courseNFo) {
					$courseTitle = $courseNFo->name;
					$curseDesc = $courseNFo->desc;
					$pic = $courseNFo->pic;
				}
				
				$data['title'] = getSiteConfiguration()['site_name'] . ' | ' . $courseTitle;

				// Render Data From Course
				$data['cursoClassNfo'] = $this->Elearning_model->getCourseClassByCourseID($cursoID);
				$data['cursoMaterial'] = $this->Elearning_model->getCourseMaterialByCourseID($cursoID);
				$data['cursoTest'] = $this->Elearning_model->getCourseTestByCourseID($cursoID);
				$data['videoMaterial'] = $this->Elearning_model->getCourseVideoByCourseID($cursoID);
				
				// Course NFO Loop End

				$data['navbarConf'] = 'commercial';
				$data['menuInt'] = $this->Internal_model->getInternalALL();

				$data['charset'] = getSiteConfiguration()['site_charset'];
				$data['description'] = getSiteConfiguration()['site_desc'];
				$data['keywords'] = getSiteConfiguration()['site_keywords'];
				$data['language'] = getSiteConfiguration()['site_lang'];
				$data['appleicon'] = getSiteConfiguration()['site_appleicon'];
				$data['favicon'] = getSiteConfiguration()['site_favicon'];
				$data['author'] = getSiteConfiguration()['site_author'];

				$data['titleSpot'] = $courseTitle;
				$data['curseDesc'] = $curseDesc;
				$data['pic'] = $pic;


				// Load View
			 	$this->load->view('elearning/course_planning', $data);
			
			}else{
			
				// Error 404 Page
				redirect('Error/NotFound', 'refresh');
			}
		
		}else{
			// Error Forbiden Access
			redirect('User/mustLog','refresh');
		}

	}

	public function courseClass($cursoID, $slug)
	{

			// Render Blogs
			$data['postLst'] = $this->Internal_model->getAllPostReview();
			// Render Blogs End

			// Get My ID
			$data['myID'] = getMyID();

			// Curse ID
			$data['curseID'] = $cursoID;

			// Render Variable
			$data['cursoNfo'] = $this->Elearning_model->getCourseByID($cursoID, $slug);

			// Render Class Variable
			$data['classNfo'] = $this->Elearning_model->getCourseClassNFOByClassID($cursoID, $slug);

			if ($data['classNfo']->num_rows() > 0) {


				$rolledInCourse = checkIfRolled($data['myID'], $cursoID);

				if ($rolledInCourse) {

					foreach ($data['classNfo']->result() as $clnfo) {
						$courseTitle = $clnfo->title;
						$curseDesc = $clnfo->desc;
					}

					$data['cursoClassNfo'] = $this->Elearning_model->getCourseClassByCourseID($cursoID, $slug);
					$data['cursoMaterial'] = $this->Elearning_model->getCourseMaterialByCourseID();
					$data['cursoTest'] = $this->Elearning_model->getCourseTestByCourseID();

					$data['navbarConf'] = 'commercial';
					$data['menuInt'] = $this->Internal_model->getInternalALL();

					$data['title'] = getSiteConfiguration()['site_name'] . ' | ' . $courseTitle;
					$data['charset'] = getSiteConfiguration()['site_charset'];
					$data['description'] = getSiteConfiguration()['site_desc'];
					$data['keywords'] = getSiteConfiguration()['site_keywords'];
					$data['language'] = getSiteConfiguration()['site_lang'];
					$data['appleicon'] = getSiteConfiguration()['site_appleicon'];
					$data['favicon'] = getSiteConfiguration()['site_favicon'];
					$data['author'] = getSiteConfiguration()['site_author'];

					$data['titleSpot'] = $courseTitle;
					$data['curseDesc'] = $curseDesc;

					// Load View
				 	$this->load->view('elearning/course_class', $data);

			 	}else{
					// Not Enrolled
					redirect('elearning/notenroled', 'refresh');
				}
			 	

		 	}else{
				// Error 404 Page
				redirect('Error/NotFound', 'refresh');
			}
	}

	public function exam()
	{

			// Render Blogs
			$data['postLst'] = $this->Internal_model->getAllPostReview();
			// Render Blogs End

			$data['cursoClassNfo'] = $this->Elearning_model->getCourseClassByCourseID();
			$data['cursoMaterial'] = $this->Elearning_model->getCourseMaterialByCourseID();
			$data['cursoTest'] = $this->Elearning_model->getCourseTestByCourseID();

			$data['navbarConf'] = 'commercial';
			$data['menuInt'] = $this->Internal_model->getInternalALL();

			$data['charset'] = getSiteConfiguration()['site_charset'];
			$data['description'] = getSiteConfiguration()['site_desc'];
			$data['keywords'] = getSiteConfiguration()['site_keywords'];
			$data['language'] = getSiteConfiguration()['site_lang'];
			$data['appleicon'] = getSiteConfiguration()['site_appleicon'];
			$data['favicon'] = getSiteConfiguration()['site_favicon'];
			$data['author'] = getSiteConfiguration()['site_author'];

			$data['titleSpot'] = '$courseTitle';
			$data['curseDesc'] = '$curseDesc';

			// Load View
		 	$this->load->view('elearning/course_exam', $data);
	}

	public function notEnrolled()
	{
			// Render Blogs
			$data['postLst'] = $this->Internal_model->getAllPostReview();
			// Render Blogs End

			$data['navbarConf'] = 'commercial';
			$data['menuInt'] = $this->Internal_model->getInternalALL();

			$data['title'] = getSiteConfiguration()['site_name'] . ' | ' . 'No Estas Registrado en el Curso';

			$data['charset'] = getSiteConfiguration()['site_charset'];
			$data['description'] = getSiteConfiguration()['site_desc'];
			$data['keywords'] = getSiteConfiguration()['site_keywords'];
			$data['language'] = getSiteConfiguration()['site_lang'];
			$data['appleicon'] = getSiteConfiguration()['site_appleicon'];
			$data['favicon'] = getSiteConfiguration()['site_favicon'];
			$data['author'] = getSiteConfiguration()['site_author'];

			$data['titleSpot'] = 'No estas Registrado en este Curso';
			$data['curseDesc'] = 'Para ver el contenido. Por favor Registrate en el curso.';

			// Load View
		 	$this->load->view('elearning/noterolled', $data);
	}

	public function enrollme()
	{
			$courseID = $_POST['courseID'];
			$userID = $_POST['userID'];

			$data = array(
			   'curso_id' => $courseID,
			   'usuario_id' => $userID
			);

			$this->db->insert('curso_alumn', $data); 

			// Enroll Success
			redirect('elearning/enrollSuccess', 'refresh');

	}

	public function enrollSuccess()
	{
			// Render Blogs
			$data['postLst'] = $this->Internal_model->getAllPostReview();
			// Render Blogs End

			$data['navbarConf'] = 'commercial';
			$data['menuInt'] = $this->Internal_model->getInternalALL();

			$data['title'] = getSiteConfiguration()['site_name'] . ' | ' . 'Mis Cursadas';

			$data['charset'] = getSiteConfiguration()['site_charset'];
			$data['description'] = getSiteConfiguration()['site_desc'];
			$data['keywords'] = getSiteConfiguration()['site_keywords'];
			$data['language'] = getSiteConfiguration()['site_lang'];
			$data['appleicon'] = getSiteConfiguration()['site_appleicon'];
			$data['favicon'] = getSiteConfiguration()['site_favicon'];
			$data['author'] = getSiteConfiguration()['site_author'];

			$data['titleSpot'] = 'Mis Cursadas';

			// Load View
		 	$this->load->view('elearning/enrollSuccess', $data);
	}

	public function myCourses()
	{

		if ($this->ion_auth->logged_in()) {


			// Render Blogs
			$data['postLst'] = $this->Internal_model->getAllPostReview();
			// Render Blogs End

			$data['navbarConf'] = 'commercial';
			$data['menuInt'] = $this->Internal_model->getInternalALL();

			$data['title'] = getSiteConfiguration()['site_name'] . ' | ' . 'Mis Cursadas';

			$data['charset'] = getSiteConfiguration()['site_charset'];
			$data['description'] = getSiteConfiguration()['site_desc'];
			$data['keywords'] = getSiteConfiguration()['site_keywords'];
			$data['language'] = getSiteConfiguration()['site_lang'];
			$data['appleicon'] = getSiteConfiguration()['site_appleicon'];
			$data['favicon'] = getSiteConfiguration()['site_favicon'];
			$data['author'] = getSiteConfiguration()['site_author'];

			$data['titleSpot'] = 'Mis Cursadas';
			

			// Info From my Enrolled Courses
			// Get My ID
			$myID = getMyID();
			$data['cursoMaterial'] = $this->Elearning_model->getCoursesByAlumID($myID);

			// Build Loop With ID from Courses
			$cursesArray = array();
			foreach ($data['cursoMaterial']->result() as $crseUsr) {
				$cursoID = $crseUsr->curso_id;

				// Insert ID in Array
				array_push($cursesArray, $cursoID);
			}

			$couresNfo = array();
			// Build Loop to Get Information From Courses with Course_ID
			foreach ($cursesArray as $key => $value) {
				$info = $this->Elearning_model->getCourseNfoByCourseID($value);

				// Insert CoursesNFo in Array
				array_push($couresNfo, $info);
			}

			$data['coursesHistorial'] = $couresNfo;

			// Counter of Courses Limiter
			$data['coursesLimiter'] = count($couresNfo);
			// Load View
		 	$this->load->view('elearning/myCourses', $data);
		
		}else{
			// Error Forbiden Access
			redirect('User/mustLog','refresh');
		}
	}

	public function myCalifications()
	{

		if ($this->ion_auth->logged_in()) {


			// Render Blogs
			$data['postLst'] = $this->Internal_model->getAllPostReview();
			// Render Blogs End

			$data['navbarConf'] = 'commercial';
			$data['menuInt'] = $this->Internal_model->getInternalALL();

			$data['title'] = getSiteConfiguration()['site_name'] . ' | ' . 'Mis Cursadas';

			$data['charset'] = getSiteConfiguration()['site_charset'];
			$data['description'] = getSiteConfiguration()['site_desc'];
			$data['keywords'] = getSiteConfiguration()['site_keywords'];
			$data['language'] = getSiteConfiguration()['site_lang'];
			$data['appleicon'] = getSiteConfiguration()['site_appleicon'];
			$data['favicon'] = getSiteConfiguration()['site_favicon'];
			$data['author'] = getSiteConfiguration()['site_author'];

			$data['titleSpot'] = 'Mis Cursadas';
			

			// Info From my Enrolled Courses
			// Get My ID
			$myID = getMyID();
			$data['cursoMaterial'] = $this->Elearning_model->getCoursesByAlumID($myID);

			// Build Loop With ID from Courses
			$cursesArray = array();
			foreach ($data['cursoMaterial']->result() as $crseUsr) {
				$cursoID = $crseUsr->curso_id;

				// Insert ID in Array
				array_push($cursesArray, $cursoID);
			}

			$couresNfo = array();
			// Build Loop to Get Information From Courses with Course_ID
			foreach ($cursesArray as $key => $value) {
				$info = $this->Elearning_model->getCourseNfoByCourseID($value);

				// Insert CoursesNFo in Array
				array_push($couresNfo, $info);
			}

			$data['coursesHistorial'] = $couresNfo;

			// Counter of Courses Limiter
			$data['coursesLimiter'] = count($couresNfo);
			// Load View
		 	$this->load->view('elearning/myCalifications', $data);
		
		}else{
			// Error Forbiden Access
			redirect('User/mustLog','refresh');
		}
	}

	public function examCurrent($examID, $courseID)
	{

		if ($this->ion_auth->logged_in()) {

			// Render Blogs
			$data['postLst'] = $this->Internal_model->getAllPostReview();
			// Render Blogs End

			$data['navbarConf'] = 'commercial';
			$data['menuInt'] = $this->Internal_model->getInternalALL();

			$data['title'] = getSiteConfiguration()['site_name'] . ' | ' . 'Examen';

			$data['charset'] = getSiteConfiguration()['site_charset'];
			$data['description'] = getSiteConfiguration()['site_desc'];
			$data['keywords'] = getSiteConfiguration()['site_keywords'];
			$data['language'] = getSiteConfiguration()['site_lang'];
			$data['appleicon'] = getSiteConfiguration()['site_appleicon'];
			$data['favicon'] = getSiteConfiguration()['site_favicon'];
			$data['author'] = getSiteConfiguration()['site_author'];

			$data['titleSpot'] = 'Examen: ';

			$data['myID'] = getMyID();
			$data['examID'] = $examID;
			
			$data['cursoExams'] = $this->Elearning_model->getExamsByCourse($courseID);

			$data['cursoExamNfo'] = $this->Elearning_model->getCursoExam($examID);

			$data['cursoExamQuest'] = $this->Elearning_model->getExamQuest($examID);

			$this->load->view('elearning/examForm', $data);

		}else{
			// Error Forbiden Access
			redirect('User/mustLog','refresh');
		}
	}

	public function saveQuestionInExam()
	{
		$respVal = $_POST['respVal'];
		$questionID = $_POST['questionID'];
		$userID = $_POST['userID'];
		$cursoID = $_POST['cursoID'];
		$examID = $_POST['examID'];
		$questNum = $_POST['questNum'];

		$data = array(
		   'curso_exam_quest_id' => $questionID,
		   'curso_exam_resep_val' => $respVal,
		   'curso_exam_id' => $examID,
		   'user_id' => $userID,
		   'curso_id' => $cursoID,
		   'questNum' => $questNum

		);

		$this->db->insert('curso_exam_resp', $data); 
	}

	public function addArticleByProp($prop_id) {
		$articleTitle = $_POST['artTitle'];
		$articleText = $_POST['artText'];
		
		$data = array(
			'title' => $articleTitle,
			'text' => $articleText,
			'prop_id' => $prop_id
		);
		$this->db->insert('article', $data); 
	}


}