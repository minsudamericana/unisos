<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Software extends CI_Controller {
	
	public function __construct(){
		parent::__construct();	
		$this->load->model('Elearning_model');	
		$this->load->model('Internal_model');	
		$this->load->model('Location_Model');
		$this->load->model('Commercial_model');
	}

	public function Home()
	{
		// Navbar Configuration
		$data['navbarConf'] = 'commercial';
		// Navbar Configuration End

		// Load Renders for Navbar
		$data['menuInt'] = $this->Internal_model->getInternalALL();
		// Load Renders for Navbar End

		// Render Blogs
		$data['postLst'] = $this->Internal_model->getAllPostReview();
		// Render Blogs End

		$data['title'] = getSiteConfiguration()['site_name'] . ' | ' . 'Software';
		$data['charset'] = getSiteConfiguration()['site_charset'];
		$data['description'] = getSiteConfiguration()['site_desc'];
		$data['keywords'] = getSiteConfiguration()['site_keywords'];
		$data['language'] = getSiteConfiguration()['site_lang'];
		$data['appleicon'] = getSiteConfiguration()['site_appleicon'];
		$data['favicon'] = getSiteConfiguration()['site_favicon'];
		$data['author'] = getSiteConfiguration()['site_author'];

		$data['titleSpot'] = 'Software y Gestores de Contenido';

		// Load View
	 	$this->load->view('software/home', $data);
	}

	public function getIsoft()
	{
		// Navbar Configuration
		$data['navbarConf'] = 'internal';
		// Navbar Configuration End
		// Load Renders for Navbar
		$data['menuCat'] = $this->Commercial_model->getCategoryALL();
		$data['menuBrands'] = $this->Commercial_model->getBrandsALL();
		$data['menuInt'] = $this->Internal_model->getInternalALL();
		// Load Renders for Navbar End
		
		// Render Blogs
		$data['postLst'] = $this->Internal_model->getAllPostReview();
		// Render Blogs End

		// Render for Package Detail
		// Render Title and Tags
		$data['title'] = getSiteConfiguration()['site_name'] . ' | ' . 'iSoft';
		$data['charset'] = getSiteConfiguration()['site_charset'];
		$data['description'] = getSiteConfiguration()['site_desc'];
		$data['keywords'] = getSiteConfiguration()['site_keywords'];
		$data['language'] = getSiteConfiguration()['site_lang'];
		$data['appleicon'] = getSiteConfiguration()['site_appleicon'];
		$data['favicon'] = getSiteConfiguration()['site_favicon'];
		$data['author'] = getSiteConfiguration()['site_author'];

		// Location Script
		$data['pais'] = $this->Location_Model->getPais();
		// Location Script End

		// Render Visualizations
		$data['titleSpot'] = 'iSoft';
		// Render Visualizations End

		// Load View
		$this->load->view('software/isoft', $data);
	}

}