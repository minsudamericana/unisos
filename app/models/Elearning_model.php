<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Elearning_model extends CI_Model
{
	// Main Courses //
	public function getCoursesList()
	{	
		$this->db->where('active', 1);
		$result = $this->db->get('cursos');
		return $result;
	}

	public function getCourseByID($cursoID, $slug)
	{
		$this->db->where('curso_id', $cursoID);
		$this->db->where('slug', $slug);
		$result = $this->db->get('cursos');
		return $result;
	}

	public function getCourseClassByCourseID($cursoID)
	{
		$this->db->where('curso_id', $cursoID);
		$result = $this->db->get('curso_class');
		return $result;
	}

	public function getCourseClassNFOByClassID($cursoID, $slug)
	{
		$this->db->where('curso_id', $cursoID);
		$this->db->where('slug', $slug);
		$result = $this->db->get('curso_class');
		return $result;
	}

	public function showCourseByCourseID($cursoID, $slug)
	{
		$this->db->where('curso_id', $cursoID);
		$this->db->where('slug', $slug);
		$result = $this->db->get('curso_class');
		return $result;
	}

	public function getCourseMaterialByCourseID($cursoID)
	{
		$this->db->where('curso_id', $cursoID);
		$result = $this->db->get('curso_class_material');
		return $result;
	}

	public function getCourseVideoByCourseID($cursoID)
	{
		$this->db->where('curso_id', $cursoID);
		$result = $this->db->get('curso_class_video');
		return $result;
	}

	public function getCourseTestByCourseID($cursoID)
	{
		$this->db->where('curso_id', $cursoID);
		$result = $this->db->get('curso_exam');
		return $result;
	}


	public function getCoursesByAlumID($alumnID)
	{
		$this->db->where('usuario_id', $alumnID);
		$result = $this->db->get('curso_alumn');
		return $result;
	}

	public function getCourseNfoByCourseID($cursoID)
	{
		$this->db->where('curso_id', $cursoID);
		$result = $this->db->get('cursos');
		return $result;
	}


	// Exams Integration with Databases
	public function getExamsByCourse($cursoID)
	{
		$this->db->where('curso_id', $cursoID);
		$result = $this->db->get('curso_exam');
		return $result;
	}

	public function getCursoExam($examID)
	{
		$this->db->where('curso_exam_id', $examID);
		$result = $this->db->get('curso_exam');
		return $result;
	}

	public function getExamQuest($examID)
	{
		$this->db->where('curso_exam_id', $examID);
		$result = $this->db->get('curso_exam_quest');
		return $result;
	}

	public function getExamQuestResp($cursoID)
	{
		$this->db->where('curso_id', $cursoID);
		$result = $this->db->get('cursos');
		return $result;
	}

	public function getExamCal($cursoID)
	{
		$this->db->where('curso_id', $cursoID);
		$result = $this->db->get('cursos');
		return $result;
	}
	// Exams Integration with Databases End
	// Main Courses End //

}